<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //////////////////Customer info//////////////////
        if(isset($_POST)){
            show_all($_POST,$conn);
        }
    }

    function show_all($Update_Info,$conn){
        $sql = "SELECT `booking_index`.`Booking_ID`,`booking_index`.`CIN_Date`,`booking_index`.`COUT_Date`,`staff`.`Staff_Name`,`payment`.`Payment_Type`,`payment`.`Payment_Method_Num`,`payment`.`Amount`,`booking_index`.`Total_Price`,`payment`.`Payment_Remark`,`payment`.`Payment_Datetime` FROM `payment`,`booking_index`,`staff`";
        $WHERE = " WHERE `booking_index`.`Booking_ID`=`payment`.`Booking_ID` AND `payment`.`Staff_ID`=`staff`.`Staff_ID`";
        $Date = "";
        if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
            if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                if(empty($Update_Info['End']))
                    $Date = " (`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "')";
                else if(empty($Update_Info['Start']))
                    $Date = " (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "')";
                else{
                    if($Update_Info['Start'] > $Update_Info['End'])
                        $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['Start'] . "'))";
                    else
                        $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "'))";
                }
            }
        }
        $Sorting = " ORDER BY `payment`.`Payment_Datetime` DESC";        
        $sql .= $WHERE;
        if($Date != '')
            $sql .= " AND " . $Date;
        $sql = $sql . $Sorting;
        // echo $sql;die;
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                if($key == 'Payment_Remark')
                    $value = str_replace(chr(13).chr(10), "<br />",nl2br($value));
                $info_array[$count][$key] = $value;
            }
            $count++;
        }
        $Room_Array = array();
        $BID_Array = array();
        for($i=0;$i<sizeof($info_array);$i++){
            if(!in_array($info_array[$i]['Booking_ID'],$BID_Array))
                $BID_Array[]=$info_array[$i]['Booking_ID'];
        }
        for($i=0;$i<sizeof($BID_Array);$i++){
            $sql = "SELECT `Room_Num` FROM `booking_detail` WHERE `Booking_ID`='".$BID_Array[$i]."'";
            $result = mysqli_query($conn,$sql);
            while($row=$result->fetch_assoc()){
                $Room_Array[$BID_Array[$i]][] = $row['Room_Num'];
            }
        }
        foreach($Room_Array as $key => $value_array){
            $Rooms = '';
            for($i=0;$i<sizeof($info_array);$i++){
                if($info_array[$i]['Booking_ID']==$key){
                    for($j=0;$j<sizeof($value_array);$j++){
                        $Rooms .= $value_array[$j];
                        if($j!=sizeof($value_array)-1)
                        $Rooms .= ", ";
                    }
                    $info_array[$i]['Rooms'] = $Rooms;
                    $Rooms = '';
                }
            }
        }

        // echo "<pre>";
        // print_r($info_array);
        // echo "</pre>";
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }
?>