<?php
    header('Content-Type: text/html; charset=UTF-8');
?>
<html>
    <head>
        <script type="text/javascript" src="../functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="../lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../lightpick.css">
        
        <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            .function_btn{
                width:130px;
                height:50px;
                border-radius:15px;
            }
            .info_td{
                padding-left: 75px;
                font-size:26px;
                border: 0;
            }
            input[type=text]{
                border: solid 3px #DADADA;
                padding-left: 10px;
                border-radius:15px;
            }
            .modified_select:-moz-focusring {
                color: transparent;
                text-shadow: 0 0 0 #000;
            }
            .modified_select{
                border: solid 3px #DADADA;
                -webkit-appearance: none;
                -moz-appearance: none;
                background: url("/hotel_management_system/images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
                background-size: 18.51px 16.03px;
                background-origin: content-box;
                padding-left: 10px;
                padding-right: 10px;
                background-repeat: no-repeat;
                border-radius:15px;
            }
            .separator {
                display: flex;
                align-items: center;
                text-align: left;
                font-size: 36px;
                width: 1495px;
            }
            .separator::after {
                content: '';
                flex: 1;
                border-bottom: 1px solid #000;
                margin-left: 45px;
            }
            .info_table{
                width: 1495px;
                height: 225px;
                border-radius:15px;
                margin: 25px
            }
            .info_table:hover{
                border: 0;
                background-color: #DADADA;
            }
            .blue_dot {
                height: 25px;
                width: 25px;
                background-color: #32C5FF;
                color:WHITE;
                border-radius: 50%;
                display: inline-block;
            }
            .red_dot {
                height: 25px;
                width: 25px;
                background-color: #F94D4D;
                color:WHITE;
                border-radius: 50%;
                display: inline-block;
            }
            .grey_dot {
                height: 25px;
                width: 25px;
                background-color: #C9C9C9;
                color:WHITE;
                border-radius: 50%;
                display: inline-block;
            }
            .divide_pd{
                text-align: left;
                font-size: 35px;
                padding-left: 90px;
                padding-bottom: 30px;
            }
            .payment_table{
                border: 1px solid #979797;
                border-collapse: collapse;
                width: 90%;
                font-size: 23px;
                margin-bottom: 30px;
            }
            .payment_table tr:nth-child(odd){
                background: #CCC
            }
            .payment_table tr:first-child td{
                background-color: #6236FF;
                color: WHITE;
                text-align: center;
            }
            .payment_table tr{
                border: 1px solid #979797;
                height: 74px;
            }
            .payment_table td{
                border: 1px solid #979797;
                text-align: center
                /* padding-left: 15px; */
            }
            .dropbtn {
            }
            .dropdown {
                position: relative;
                display: inline-block;
            }
            .dropdown-content {
                text-align: left;
                display: none;
                position: absolute;
                background-color: WHITE;
                width: 430px;
                box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
                z-index: 1;
            }
            .dropdown-content td{
                color: black;
                font-size: 26px;
                padding-left: 30px;
                text-decoration: none;
                height: 80px;
                vertical-align: center;
            }
            .dropdown-content p:hover {background-color: #ddd;}
            .dropdown:hover .dropdown-content {display: block;}
            .dropdown:hover .dropbtn {background-color: #3e8e41;}
        </style>
    </head>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center>
                <p id='result'></p>
                <table style='width: 85%' border='0'>
                    <tr>
                        <td style='font-size:20px;width:33%;text-align:right'>
                            <input type='text' id='Start' value='' style='width:270px;height:50px;font-size:20px' placeholder='點擊選擇起始日'>
                        </td>
                        <td style='text-align:left;font-size:20px;width:33%'>
                            <input type='text' id='End' value='' style='width:270px;height:50px;font-size:20px' placeholder='點擊選擇結束日'>
                            <!-- <button class='function_btn' id='Rest' style='width:110px;height:50px;font-size:24px;background-color:#0091FF'>重設</button> -->
                        </td>
                        <td style='text-align:right;font-size:24px'>
                            <button class='function_btn' id='Print_List' style='width:160px;height:50px;font-size:20px;background-color:WHITE;margin-right:30px;color:BLACK' disabled>列印訂單</button>
                        </td>
                    </tr>
                    <tr>
                        <td style='width:30%;text-align:center'>
                            <div class="dropdown" style='width:100%;'>
                                <button class='function_btn dropbtn' style='width:90%;height:85px;font-size:24px;background-color:#0091FF;font-size:40px;color:WHITE' disabled>收款：<span id='Paid'></span></button>
                                <div class="dropdown-content">
                                    <table style='width:90%'>
                                        <tr><td id="cash_payment"></td></tr>
                                        <tr><td id="credit_card_payment"></td></tr>
                                        <tr><td id="transfer_payment"></td></tr>
                                        <tr><td id="else_payment"></td></tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                        <td style='width:30%;text-align:center'>
                            <button class='function_btn' style='width:90%;height:85px;font-size:24px;background-color:#0091FF;font-size:40px;color:WHITE' disabled>應收：<span id='Total_Price'></span></button>
                        </td>
                        <td style='width:30%;text-align:center'>
                            <button class='function_btn' style='width:90%;height:85px;font-size:24px;background-color:#0091FF;font-size:40px;color:WHITE' disabled>待收：<span id='Unpaid'></span></button>
                        </td>
                    </tr>
                </table>
                <br>
                <div id='container'>
                </div>
            </center>
        </div>
        <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
    </body>
</html>
<script>
    var picker = new Lightpick({
        field: document.getElementById('Start'),
        // startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('End').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var Payment_Type_array = ['訂金','現場收款','月結簽帳','帳務調整','退款','其它'];
                    var Payment_Method_Num_array = ['現金','信用卡','轉帳','其它'];
                    var json_array = data;
                    var pre_date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var date = json_array[i].Payment_Datetime;
                        date = date.split(" ");
                        date = date[0];
                        if(pre_date!=date){
                            if(date != '')
                                pre_date = date;
                            table = "<div class='divide_pd'>"+pre_date.replace(/-/g, "/")+"</div><table id='"+pre_date+"' class='payment_table'><tr><td width='17.5%'>訂單編號</td><td width='22.5%'>入住時間</td><td width='9%'>收款人員</td><td width='9%'>付款類型</td><td width='9%'>付款方式</td><td width='7.5%' style='background-color:#F79B00'>收款</td><td width='7.5%'>應收</td><td width='7.5%'>待收</td><td>備註</td></tr>";
                            $("#container").append(table);
                        }
                    }
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Staying_Dates = json_array[i].CIN_Date.replace(/-/g, "/")+' ~ '+json_array[i].COUT_Date.replace(/-/g, "/");
                        // var Room_Num = json_array[i].Customer_Phone;
                        var Staff_ID = json_array[i].Staff_Name;
                        var Paid = json_array[i].Amount;
                        var Total = json_array[i].Total_Price;
                        var Unpaid = Total-Paid;
                        var Remark = json_array[i].Payment_Remark;
                        var date = json_array[i].Payment_Datetime;
                        var Payment_Type = Payment_Type_array[json_array[i].Payment_Type];
                        var Payment_Method_Num = Payment_Method_Num_array[json_array[i].Payment_Method_Num];
                        date = date.split(" ");
                        date = date[0];
                        // row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Room_Num+"</td><td>"+Staff_ID+"</td><td>"+Paid+"</td><td>"+Total+"</td><td>"+Unpaid+"</td><td>"+Remark+"</td></tr>"
                        row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Staff_ID+"</td><td>"+Payment_Type+"</td><td>"+Payment_Method_Num+"</td><td style='text-align:right'>"+Paid+"</td><td style='text-align:right'>"+Total+"</td><td style='text-align:right'>"+Unpaid+"</td><td>"+Remark+"</td></tr>"
                        $("#"+date).append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    var picker2 = new Lightpick({
        field: document.getElementById('End'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('Start').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                console.log("Start: " + document.getElementById('Start').value + "\nEnd: " + document.getElementById('End').value);
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var Payment_Type_array = ['訂金','現場收款','月結簽帳','帳務調整','退款','其它'];
                    var Payment_Method_Num_array = ['現金','信用卡','轉帳','其它'];
                    var json_array = data;
                    var pre_date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var date = json_array[i].Payment_Datetime;
                        date = date.split(" ");
                        date = date[0];
                        if(pre_date!=date){
                            if(date != '')
                                pre_date = date;
                            table = "<div class='divide_pd'>"+pre_date.replace(/-/g, "/")+"</div><table id='"+pre_date+"' class='payment_table'><tr><td width='17.5%'>訂單編號</td><td width='22.5%'>入住時間</td><td width='9%'>收款人員</td><td width='9%'>付款類型</td><td width='9%'>付款方式</td><td width='7.5%' style='background-color:#F79B00'>收款</td><td width='7.5%'>應收</td><td width='7.5%'>待收</td><td>備註</td></tr>";
                            $("#container").append(table);
                        }
                    }
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Staying_Dates = json_array[i].CIN_Date.replace(/-/g, "/")+' ~ '+json_array[i].COUT_Date.replace(/-/g, "/");
                        // var Room_Num = json_array[i].Customer_Phone;
                        var Staff_ID = json_array[i].Staff_Name;
                        var Paid = json_array[i].Amount;
                        var Total = json_array[i].Total_Price;
                        var Unpaid = Total-Paid;
                        var Remark = json_array[i].Payment_Remark;
                        var date = json_array[i].Payment_Datetime;
                        var Payment_Type = Payment_Type_array[json_array[i].Payment_Type];
                        var Payment_Method_Num = Payment_Method_Num_array[json_array[i].Payment_Method_Num];
                        date = date.split(" ");
                        date = date[0];
                        // row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Room_Num+"</td><td>"+Staff_ID+"</td><td>"+Paid+"</td><td>"+Total+"</td><td>"+Unpaid+"</td><td>"+Remark+"</td></tr>"
                        row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Staff_ID+"</td><td>"+Payment_Type+"</td><td>"+Payment_Method_Num+"</td><td style='text-align:right'>"+Paid+"</td><td style='text-align:right'>"+Total+"</td><td style='text-align:right'>"+Unpaid+"</td><td>"+Remark+"</td></tr>"
                        $("#"+date).append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    $(document).ready(function() {
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Start: $("#Start").val(),
                End: $("#End").val()
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html('');
                var Payment_Type_array = ['訂金','現場收款','月結簽帳','帳務調整','退款','其它'];
                var Payment_Method_Num_array = ['現金','信用卡','轉帳','其它'];
                var json_array = data;
                var pre_date = '';
                var row = '';
                var Total_Paid = 0;
                var Total_MPay = 0;
                
                var cash_payment = 0;
                var credit_card_payment = 0;
                var transfer_payment = 0;
                var else_payment = 0;

                for(i=0;i<json_array.length;i++){
                    var date = json_array[i].Payment_Datetime;
                    date = date.split(" ");
                    date = date[0];
                    if(pre_date!=date){
                        if(date != '')
                            pre_date = date;
                        table = "<div class='divide_pd'>"+pre_date.replace(/-/g, "/")+"</div><table id='"+pre_date+"' class='payment_table'><tr><td width='17.5%'>訂單編號</td><td width='22.5%'>入住時間</td><td>房號</td><td width='9%'>收款人員</td><td width='9%'>付款類型</td><td width='9%'>付款方式</td><td width='7.5%' style='background-color:#F79B00'>收款</td><td width='7.5%'>應收</td><td>備註</td></tr>";
                        $("#container").append(table);
                    }
                }
                for(i=0;i<json_array.length;i++){
                    var Booking_ID = json_array[i].Booking_ID;
                    var Staying_Dates = json_array[i].CIN_Date.replace(/-/g, "/")+' ~ '+json_array[i].COUT_Date.replace(/-/g, "/");
                    var Rooms = json_array[i].Rooms;
                    var Staff_ID = json_array[i].Staff_Name;
                    var Paid = json_array[i].Amount;
                    var Total = json_array[i].Total_Price;
                    var Remark = json_array[i].Payment_Remark;
                    var date = json_array[i].Payment_Datetime;
                    var Payment_Type = Payment_Type_array[json_array[i].Payment_Type];
                    var Payment_Method_Num = Payment_Method_Num_array[json_array[i].Payment_Method_Num];
                    date = date.split(" ");
                    date = date[0];
                    // row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Room_Num+"</td><td>"+Staff_ID+"</td><td>"+Paid+"</td><td>"+Total+"</td><td>"+Unpaid+"</td><td>"+Remark+"</td></tr>"
                    row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Rooms+"</td><td>"+Staff_ID+"</td><td>"+Payment_Type+"</td><td>"+Payment_Method_Num+"</td><td style='text-align:right'>"+Paid+"</td><td style='text-align:right'>"+Total+"</td><td>"+Remark+"</td></tr>"
                    $("#"+date).append(row);
                    Total_Paid+=parseInt(Paid);
                    Total_MPay+=parseInt(Total);
                    switch(json_array[i].Payment_Method_Num){
                        case '0':
                            cash_payment += parseInt(Paid);
                        break;

                        case '1':
                            credit_card_payment += parseInt(Paid);
                        break;

                        case '2':
                            transfer_payment += parseInt(Paid);
                        break;

                        case '3':
                            else_payment += parseInt(Paid);
                        break;
                    }
                }
                $("#Paid").html(Total_Paid);
                $("#Total_Price").html(Total_MPay);
                $("#Unpaid").html(Total_MPay-Total_Paid);
                $("#cash_payment").html('現金：'+cash_payment);
                $("#credit_card_payment").html('信用卡：'+credit_card_payment);
                $("#transfer_payment").html('轉帳：'+transfer_payment);
                $("#else_payment").html('其它：'+else_payment);
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
            }
        })
    });
</script>