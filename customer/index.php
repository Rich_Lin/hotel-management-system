<?php
    header('Content-Type: text/html; charset=UTF-8');
?>
<head>
    <script type="text/javascript" src="../functions.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

    <script type="text/javascript" src="../lightpick.js"></script>
    <link rel="stylesheet" type="text/css" href="../lightpick.css">

    <style>
        .blue_dot {
            height: 24px;
            width: 24px;
            background-color: #32C5FF;
            border-radius: 50%;
            display: inline-block;
        }
        .red_dot {
            height: 24px;
            width: 24px;
            background-color: #F94D4D;
            border-radius: 50%;
            display: inline-block;
        }
        .purple_dot {
            height: 24px;
            width: 24px;
            background-color: #A94DF9;
            border-radius: 50%;
            display: inline-block;
        }
        .customer_btn {
            margin: 20px;
            width:360px;
            height:130px;
            border-radius:15px;
            background-color:WHITE;
            border:2.5px solid #DADADA;
        }
        .customer_btn:hover {
            background-color: #DADADA;
        }
        .edit_column{
            display: none
        }
        .display_text{
            display: inline
        }
        .function_btn{
            width:130px;
            height:50px;
            border-radius:15px;
            color:WHITE;
        }
        .ui-widget.ui-widget-content{
            border-radius: 20px;
            border-width: 20px;
            background-color: #DADADA;
            border: 1px solid #DADADA;
        }
        .ui-widget-content {
            border-radius: 20px;
            border-width: 20px;
        }
        .ui-widget-overlay{
            background-color: transparent;
        }
        .ui-dialog-titlebar{
            display: none
        }
        td{
            padding: 0px 0px;
        }
        input[type=text],select,textarea{
            border: solid 3px #DADADA;
            padding-left: 10px;
            border-radius:15px;
        }
        .modified_select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }
        .modified_select{
            border: solid 3px #DADADA;
            -webkit-appearance: none;
            -moz-appearance: none;
            background: url("/hotel_management_system/images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
            background-size: 18.51px 16.03px;
            /* background-image:
                linear-gradient(to right, #ccc, #ccc);
            background-position:
                calc(100% - 2em) 0em; */
            /* background-size:
                1px 2.1em; */
            background-origin: content-box;
            padding-left: 10px;
            padding-right: 10px;
            background-repeat: no-repeat;
            border-radius:15px;
        }
        
    </style>
</head>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div>
    <div class='right'>
        <center>
            <p id='result'></p>
            <table style='width: 90%' border='0'>
                <tr>
                    <td>
                        <input type='text' style='width:250px;height:51px;font-size:20px;text-align:center' id='Fuzzy_Search' placeholder='關鍵字搜尋'>
                    </td>
                    <td style='text-align:center;vertical-align:middle'>
                        <select style='width:188px;height:51px;font-size:20px;margin-left:5px' class='modified_select' id='Grouping'>
                            <option value=''>排列順序</option>
                            <option value='CIN'>入住日期</option>
                            <option value='COUT'>退房日期</option>
                        </select>
                        <select style='width:188px;height:51px;font-size:20px;margin-left:5px' class='modified_select' id='Sorting'>
                            <option value='DESC'>由新到舊</option>
                            <option value='ASC'>由舊到新</option>
                        </select>
                    </td>
                    <td style='font-size:24px;text-align:center;vertical-align:middle'>
                        <input type='text' id='Start' value='' style='width:250px;height:51px;font-size:20px' placeholder='選擇起始日'>
                        <input type='text' id='End' value='' style='width:250px;height:51px;font-size:20px' placeholder='選擇結束日'>
                    </td>
                    <td style='font-size:24px;text-align:center;vertical-align:middle'>
                        <input type='button' class='function_btn' style='width:130px;height:50px;font-size:20px;color:white;background-color:#0091FF' id='Rest' value='重設'>
                    </td>
                    <!-- <td><input type='text' id='Start' value='' placeholder='點擊以選擇入住時間'> ～ <input type='text' id='End' value='' placeholder='點擊以選擇退房時間'>&nbsp;&nbsp;<input type='button' id='Rest' value='重設'></td> -->
                    <!-- <td style='width: 10%'><input type='submit' name='new' value='新增專案'></td> -->
                </tr>
            </table>
            <div id='container'></div>
        </center>
    </div>
    <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
</body>


<div id="dialog" style='background-color:#DADADA'><br>
    <center>
    <input type="hidden" autofocus="true" />
        <table width='100%' style='table-layout: fixed;'>
            <tr style='display:none'>
                <td style='text-align:right;font-size:26px;'>旅客編號：</td>
                <td><input type='text' name='Customer_ID' id='Customer_ID'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;width:25%;height:59px'>旅客姓名：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Name_Text'></span><input type='text' class='edit_column' name='Customer_Name' id='Customer_Name'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;;height:59px'>旅客性別：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Sex_Text'></span>
                    <select class='edit_column modified_select' name='Customer_Sex' id='Customer_Sex'>
                        <option value='0'>女</option>
                        <option value='1'>男</option>
                        <option value='2'>不明</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;;height:59px'>旅客國籍：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Nationality_Text'></span><input type='text' class='edit_column' name='Customer_Nationality' id='Customer_Nationality'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;;height:59px'>身分證號：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_SSID_Text'></span><input type='text' class='edit_column' name='Customer_SSID' id='Customer_SSID'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;;height:59px'>護照號碼：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Passport_Text'></span><input type='text' class='edit_column' name='Customer_Passport' id='Customer_Passport'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;;height:59px'>連絡電話：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Phone_Text'></span><input type='text' class='edit_column' name='Customer_Phone' id='Customer_Phone'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;;height:59px'>E-Mail：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Email_Text'></span><input type='text' class='edit_column' name='Customer_Email' id='Customer_Email'></td>
            </tr>
            <tr>
                <td style='text-align:right;font-size:26px;vertical-align:top'>備註：</td>
                <td style='font-size:26px;' colspan='2'><span class='display_text' id='Customer_Remark_Text'></span><textarea  class='edit_column' name='Customer_Remark' id='Customer_Remark' rows='3' cols='23' style='resize: none;'></textarea></td>
            </tr>
        </table>
        <table style='width:95%;position:absolute; bottom:5px;'>
            <tr>
                <td style='width:33%'>
                    <input type='button' class='function_btn edit_column' style='background-color:#F94D4D;font-size:24px;' name='submit' value='刪除' onClick='Update_Info(this.value)'></div>
                </td>
                <td style='text-align: right;width:33%'>
                    <input type='button' class='function_btn' style='background-color:#F79B00;font-size:24px;' name='submit' value='編輯' onClick='change_situ()'></div>
                </td>
                <td style='text-align: right;width:33%'>
                    <input type='button' class='function_btn' style='background-color:#0091FF;font-size:24px;' name='submit' value='確定' onClick='Update_Info(this.value)'></div>
                </td>
            </tr>
        </table>
    </center>
</div>

<script>
    var gender = ['女','男','不明'];

    var picker = new Lightpick({
        field: document.getElementById('Start'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('End').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Customer_ID = json_array[i].Customer_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Sex = json_array[i].Customer_Sex;
                        var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                        var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                        var color = "";
                        switch(Customer_Sex){
                            case '0':
                                color = "red_dot";
                            break;
                            
                            case '1':
                                color = "blue_dot";
                            break;
                            
                            case '2':
                                color = "purple_dot";
                            break;
                        }
                        var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                        $("#container").append(row);
                        // console.log(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    var picker2 = new Lightpick({
        field: document.getElementById('End'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('Start').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Customer_ID = json_array[i].Customer_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Sex = json_array[i].Customer_Sex;
                        var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                        var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                        var color = "";
                        switch(Customer_Sex){
                            case '0':
                                color = "red_dot";
                            break;
                            
                            case '1':
                                color = "blue_dot";
                            break;
                            
                            case '2':
                                color = "purple_dot";
                            break;
                        }
                        var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                        $("#container").append(row);
                        // console.log(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    $(document).ready(function() {
        $("#dialog").dialog({
            height: 660,
            width: 590,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Sorting").change(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Customer_ID = json_array[i].Customer_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Sex = json_array[i].Customer_Sex;
                        var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                        var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                        var color = "";
                        switch(Customer_Sex){
                            case '0':
                                color = "red_dot";
                            break;
                            
                            case '1':
                                color = "blue_dot";
                            break;
                            
                            case '2':
                                color = "purple_dot";
                            break;
                        }
                        var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                        $("#container").append(row);
                        // console.log(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Grouping").change(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Customer_ID = json_array[i].Customer_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Sex = json_array[i].Customer_Sex;
                        var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                        var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                        var color = "";
                        switch(Customer_Sex){
                            case '0':
                                color = "red_dot";
                            break;
                            
                            case '1':
                                color = "blue_dot";
                            break;
                            
                            case '2':
                                color = "purple_dot";
                            break;
                        }
                        var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                        $("#container").append(row);
                        // console.log(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Fuzzy_Search").keyup(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Customer_ID = json_array[i].Customer_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Sex = json_array[i].Customer_Sex;
                        var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                        var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                        var color = "";
                        switch(Customer_Sex){
                            case '0':
                                color = "red_dot";
                            break;
                            
                            case '1':
                                color = "blue_dot";
                            break;
                            
                            case '2':
                                color = "purple_dot";
                            break;
                        }
                        var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                        $("#container").append(row);
                        // console.log(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Rest").click(function(){
            $("#Grouping").prop('selectedIndex',0);
            $("#Sorting").prop('selectedIndex',0);
            $("#Fuzzy_Search").val('');
            $("#Start").val('');
            $("#End").val('');
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Customer_ID = json_array[i].Customer_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Sex = json_array[i].Customer_Sex;
                        var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                        var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                        var color = "";
                        switch(Customer_Sex){
                            case '0':
                                color = "red_dot";
                            break;
                            
                            case '1':
                                color = "blue_dot";
                            break;
                            
                            case '2':
                                color = "purple_dot";
                            break;
                        }
                        var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                        $("#container").append(row);
                        // console.log(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        })

        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_all",
                Fuzzy_Search: $("#Fuzzy_Search").val(),
                Sorting: $("#Sorting").val(),
                Grouping: $("#Grouping").val(),
                Start: $("#Start").val(),
                End: $("#End").val()
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html('');
                var json_array = data;
                for(i=0;i<json_array.length;i++){
                    var Customer_ID = json_array[i].Customer_ID;
                    var Customer_Name = json_array[i].Customer_Name;
                    var Customer_Sex = json_array[i].Customer_Sex;
                    var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                    var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                    var color = "";
                    switch(Customer_Sex){
                        case '0':
                            color = "red_dot";
                        break;
                        
                        case '1':
                            color = "blue_dot";
                        break;
                        
                        case '2':
                            color = "purple_dot";
                        break;
                    }
                    var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                    $("#container").append(row);
                    // console.log(row);
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
            }
        })
    });

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        for(i=0;i<document.getElementsByClassName("display_text").length;i++){
            document.getElementsByClassName("display_text")[i].style.display = "inline";
        }
        for(i=0;i<document.getElementsByClassName("edit_column").length;i++){
            document.getElementsByClassName("edit_column")[i].style.display = "none";
        }
        $('#dialog').dialog( "close" );
    }

    function show_dialog(Customer_ID){
        document.getElementById("Customer_ID").value = Customer_ID;
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_customer_info",
                Customer_ID: $("#Customer_ID").val()
            },
            success: function(data) {
                $("#result").html('');
                var json_array = data;
                $("#Customer_ID").val(json_array.Customer_ID);
                $("#Customer_Name").val(json_array.Customer_Name);
                $("#Customer_Sex").val(json_array.Customer_Sex);
                $("#Customer_Nationality").val(json_array.Customer_Nationality);
                $("#Customer_SSID").val(json_array.Customer_SSID);
                $("#Customer_Passport").val(json_array.Customer_Passport);
                $("#Customer_Phone").val(json_array.Customer_Phone);
                $("#Customer_Email").val(json_array.Customer_Email);
                $("#Customer_Remark").val(json_array.Customer_Remark);
                //////////////////////////////////////////////////////text
                $("#Customer_Name_Text").html(json_array.Customer_Name);
                $("#Customer_Sex_Text").html(gender[json_array.Customer_Sex]);
                $("#Customer_Nationality_Text").html(json_array.Customer_Nationality);
                $("#Customer_SSID_Text").html(json_array.Customer_SSID);
                $("#Customer_Passport_Text").html(json_array.Customer_Passport);
                $("#Customer_Phone_Text").html(json_array.Customer_Phone);
                $("#Customer_Email_Text").html(json_array.Customer_Email);
                $("#Customer_Remark_Text").html(json_array.Customer_Remark.replace(/\r?\n|\r/g, "<br>"));
                // console.log(data);
                $( "#dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                // console.log(jqXHR.responseText);

            }
        })
    }

    function change_situ(){
        for(i=0;i<document.getElementsByClassName("display_text").length;i++){
            document.getElementsByClassName("display_text")[i].style.display = "none";
        }
        for(i=0;i<document.getElementsByClassName("edit_column").length;i++){
            document.getElementsByClassName("edit_column")[i].style.display = "inline";
        }
    }

    function Update_Info(target_value){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "Update_Info",
                Customer_ID: $("#Customer_ID").val(),
                Customer_Name: $("#Customer_Name").val(),
                Customer_Sex: $("#Customer_Sex").val(),
                Customer_Nationality: $("#Customer_Nationality").val(),
                Customer_SSID: $("#Customer_SSID").val(),
                Customer_Passport: $("#Customer_Passport").val(),
                Customer_Phone: $("#Customer_Phone").val(),
                Customer_Email: $("#Customer_Email").val(),
                Customer_Remark: $("#Customer_Remark").val(),
                submit: target_value,
                Fuzzy_Search: $("#Fuzzy_Search").val(),
                Sorting: $("#Sorting").val(),
                Grouping: $("#Grouping").val(),
                Start: $("#Start").val(),
                End: $("#End").val()
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html('');
                var json_array = data;
                for(i=0;i<json_array.length;i++){
                    var Customer_ID = json_array[i].Customer_ID;
                    var Customer_Name = json_array[i].Customer_Name;
                    var Customer_Sex = json_array[i].Customer_Sex;
                    var CIN_Date = json_array[i].CIN_Date.split(" ")[0];
                    var COUT_Date = json_array[i].COUT_Date.split(" ")[0];
                    var color = "";
                    switch(Customer_Sex){
                        case '0':
                            color = "red_dot";
                        break;
                        
                        case '1':
                            color = "blue_dot";
                        break;
                        
                        case '2':
                            color = "purple_dot";
                        break;
                    }
                    var row = "<button class='customer_btn' value='" + Customer_ID + "' onClick='show_dialog(this.value)'><div style='text-align:right;margin-right:10px'><span class='" + color + "'></span></div><span style='text-align:center;font-size: 26px'>" + Customer_Name + "</span><br><span style='text-align:center;font-size: 22px'>" + CIN_Date + "～ " + COUT_Date + "</span></button>";
                    $("#container").append(row);
                    // console.log(row);
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：aaaaaaaaaa' + jqXHR.responseText + '</font>');
            }
        })

        close_dialog();
    }

</script>