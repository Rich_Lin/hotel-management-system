<?php
header("Access-Control-Allow-Origin: *");      
// header("Access-Control-Allow-Headers:
//  {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
header('Content-Type: application/json; charset=UTF-8');
include_once "../mysql_connect.inc.php";
// if ($_SERVER['REQUEST_METHOD'] == "POST") {
//////////////////Customer info//////////////////
    if(isset($_POST)){
        if($_POST['Feature'] == "show_all"){
            show_all('show_all',$_POST,$conn);
        }
        else if($_POST['Feature'] == "show_customer_info"){
            $info_array = array();
            @$Customer_ID = $_POST["Customer_ID"];
            if ($Customer_ID){
                $sql = "SELECT * FROM `customer` WHERE `Customer_ID` = '".$Customer_ID."'";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc())
                    foreach($row as $key => $value){
                        if($key == 'Customer_Remark')
                            $value = str_replace(chr(13).chr(10), "<br />",$value);
                        $info_array[$key] = $value;
                    }
        
                echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
            } else {
                echo json_encode(array(
                    'errorMsg' => '資料未輸入完全！'
                ),JSON_UNESCAPED_UNICODE);
            }
        }
        else if($_POST['Feature'] == "Update_Info"){
            if($_POST['submit'] == '確定'){
                $Customer_Remark = str_replace("<br />", '',nl2br($_POST["Customer_Remark"]));
                $sql = "UPDATE `customer` SET `Customer_Name`='".$_POST['Customer_Name']."',`Customer_Sex`='".$_POST['Customer_Sex']."',`Customer_Phone`='".$_POST['Customer_Phone']."',`Customer_Email`='".$_POST['Customer_Email']."',`Customer_Nationality`='".$_POST['Customer_Nationality']."',`Customer_SSID`='".$_POST['Customer_SSID']."',`Customer_Passport`='".$_POST['Customer_Passport']."',`Customer_Remark`='".$Customer_Remark."' WHERE `Customer_ID`='".$_POST['Customer_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
            }
            else if($_POST['submit'] == '刪除'){
                $sql = "UPDATE `customer` SET `Enable`=0 WHERE `Customer_ID`='".$_POST['Customer_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }

            }
            show_all('Update_Info',$_POST,$conn);
        }
    }
    function show_all($From,$Update_Info,$conn){
        // echo $From."<pre>";
        // print_r($Update_Info);
        // echo "</pre>";
        $flag=0;
        $sql = "SELECT * FROM `customer`,`booking_index` WHERE `customer`.`Customer_ID`=`booking_index`.`Customer_ID` AND `customer`.`Enable`=1 AND `customer`.`Customer_ID`!=0 AND `customer`.`Customer_ID`!=1";
        $group_by = " GROUP BY `customer`.`Customer_ID`";
        $Fuzzy_Search = "";
        $Sorting = " ORDER BY `booking_index`.`COUT_Date`";
        $Date = "";
        if(!empty($Update_Info['Fuzzy_Search'])){
            $Fuzzy_Search = " (`customer`.`Customer_Name` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_Phone` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_Email` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_SSID` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_Passport` LIKE '%".$Update_Info['Fuzzy_Search']."%') ";
        }
        if(isset($Update_Info["Grouping"])){
            switch($Update_Info["Grouping"]){
                case "CIN":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `booking_index`.`CIN_Date`";
                break;

                case "COUT":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`COUT_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`COUT_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`booking_index`.`COUT_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`COUT_Date` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`booking_index`.`COUT_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`COUT_Date` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `booking_index`.`COUT_Date`";
                break;

                default:
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
            }
        }
        if(isset($Update_Info["Sorting"])){
            switch($Update_Info["Sorting"]){
                case "DESC":
                    $Sorting .= " DESC";
                break;

                case "ASC":
                    $Sorting .= " ASC"; 
                break;
            }
        }

        if($Fuzzy_Search != '')
            $sql = $sql . " AND " . $Fuzzy_Search;
        if($Date != '')
            $sql .= " AND " . $Date;
        $sql = $sql . $group_by . $Sorting;
        // echo $sql;die;
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                $info_array[$count][$key] = $value;
            }
            $count++;
        }
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }
?>