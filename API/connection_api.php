<?php
    header('Content-Type: application/json; charset=UTF-8');
	$NusoapWSDL="http://invoice.cetustek.com.tw/InvoiceEntitySync/InvoiceAPI?wsdl";
    include_once "../mysql_connect.inc.php";
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(!empty($_POST)){
            switch($_POST['Request']){
                case 'Check_In':
                    switch($_POST['Feature']){
                        case 'Search':
                            Search_Booking($_POST['Booking_Info'],$conn);
                        break;
                        
                        // case 'list':
                        // break;
                        
                        case 'Finish':
                            Check_In($_POST['Booking_ID'],$_POST['Room_Num'],$_POST['All'],$conn);
                        break;
                    }
                break;
                
                case 'Check_Out':
                break;
                
                case 'Search_Room':
                    switch($_POST['Feature']){
                        case 'Search':
                            Select_Room($_POST['CIN_Date'],$_POST['COUT_Date'],$_POST['Person_Count'],$conn);
                        break;
                        
                        case 'Search_Customer':
                            Search_Customer($_POST['Info'],$conn);
                        break;
                        
                        case 'Create_Customer':
                            Create_New_Customer($_POST['Info'],$conn);
                        break;
                        
                        case 'Take':
                            Take_Room($_POST['CIN_Date'],$_POST['COUT_Date'],$_POST['Room_Type'],$_POST['Total_Price'],$_POST['People_Count'],$conn);
                        break;
                        
                        case 'Cancel':
                            Cancel_Booking($_POST['Booking_ID'],$conn);
                        break;
                    }
                break;

                case 'Invoice_Info':
                    switch($_POST['Feature']){
                        case 'Get_Company_Info':
                            Get_Company_Info($conn);
                        break;
                        
                        case 'Get_Invoice_Variable':
                            Get_Invoice_Variable($_POST['Month'],$conn);
                        break;
                        
                        // case 'Get_New_Invoice_List':
                        //     Get_New_Invoice_List($_POST['Month'],$conn);
                        // break;

                        case 'Upload_Invoice':
                            Upload_Invoice($_POST['Info'],$conn);
                        break;
                    }
                break;
            }
        }
    }

    function Create_New_Customer($Info,$conn){
        $Info = json_decode($Info,true);
        $argument = '';
        if(!empty($Info['Customer_SSID']) || !empty($Info['Customer_Passport'])){
            if(!empty($Info['Customer_SSID']) && !empty($Info['Customer_Passport']))
                $argument = "`Customer_SSID`='".$Info['Customer_SSID']."' OR `Customer_Passport`='".$Info['Customer_Passport']."'";
            else if(!empty($Info['Customer_SSID']))
                $argument = "`Customer_SSID`='".$Info['Customer_SSID']."'";
            else
                $argument = "`Customer_Passport`='".$Info['Customer_Passport']."'";
        }
        $sql = "SELECT * FROM `customer` WHERE " . $argument;
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        $sql = "INSERT INTO `customer`(`Customer_Name`, `Customer_Sex`, `Customer_Phone`, `Customer_Email`, `Customer_SSID`, `Customer_Passport`, `Customer_Nationality`) VALUES ('".$Info['Customer_Name']."','".$Info['Customer_Sex']."','".$Info['Customer_Phone']."','".$Info['Customer_Email']."','".$Info['Customer_SSID']."','".$Info['Customer_Passport']."','".$Info['Customer_Nationality']."')";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        $sql = "SELECT * FROM `customer` WHERE " . $argument;
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        $sql = "UPDATE `booking_index` SET `Customer_ID`='".$row['Customer_ID']."' WHERE `Booking_ID`='".$Info['Booking_ID']."'";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        echo json_encode(array('Success' => true, 'Customer_ID' => $row['Customer_ID']),JSON_UNESCAPED_UNICODE);
    }

    function Search_Customer($Info,$conn){
        $Info = json_decode($Info,true);
        $sql = "SELECT `Customer_ID`,`Customer_Name` FROM `customer` WHERE `Customer_SSID`='".$Info['Search_String']."' OR `Customer_Passport`='".$Info['Search_String']."'";
        $result = mysqli_query($conn,$sql);
        if(mysqli_num_rows($result)>0){
            $row = $result->fetch_assoc();
            $sql = "UPDATE `booking_index` SET `Customer_ID`='".$row['Customer_ID']."' WHERE `Booking_ID`='".$Info['Booking_ID']."'";
            if(!mysqli_query($conn,$sql)){
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
            echo json_encode(array('Success' => true,'Customer_ID' => $row['Customer_ID'],'Customer_Name' => $row['Customer_Name']),JSON_UNESCAPED_UNICODE);
        }
        else
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
    }

    function Search_Booking($info,$conn){
        $info_array = array();
        date_default_timezone_set('Asia/Taipei');
        $Today_Date = date('Y/m/d', time());
        // $or_Statement = "`booking_index`.`Booking_ID` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Phone` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Email` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_SSID` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Passport` = '".$_POST['Booking_Info']."'";
        $or_Statement = "`booking_index`.`Booking_ID` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_SSID` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Passport` = '".$_POST['Booking_Info']."'";
        $sql = "SELECT * FROM `booking_index`,`customer`,`room_type`,`booking_detail` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_index`.`Customer_ID`=`customer`.`Customer_ID` AND `room_type`.`Room_Type`=`booking_detail`.`Room_Type` AND (".$or_Statement.") AND `booking_detail`.`Room_Num`!='' AND `booking_detail`.`Room_Status`='0' AND `booking_detail`.`Actual_CIN_Datetime`='0000-00-00 00:00:00' AND DATE(`booking_index`.`CIN_Date`)<='$Today_Date'";
        $result = mysqli_query($conn,$sql);
        $i=0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                $info_array[$i][$key] = $value;
            }
            $i++;
        }
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }

    function Check_In($Booking_ID,$Room_Num,$All,$conn){
        $sql = "SELECT `Timezone_Set` FROM `hotel_info`";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        $timezone_set = $row['Timezone_Set'];
        date_default_timezone_set($timezone_set);
        $Now_Datetime = date('Y/m/d H:i:s', time());
        if(!isset($Booking_ID) || empty($Booking_ID)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        if(!empty($Room_Num)){
            $sql = "UPDATE `booking_detail` SET `Room_Status`='1',`Actual_CIN_Datetime`='".$Now_Datetime."' WHERE `Booking_ID`='".$Booking_ID."' AND `Room_Num`='".$Room_Num."'";
            if(!mysqli_query($conn,$sql)){
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }

            // $sql = "SELECT * FROM `booking_detail` WHERE `Booking_ID`='".$Booking_ID."' AND `Room_Status`!='0'";
            // $result = mysqli_query($conn,$sql);
            // $room_count = mysqli_num_rows($result);
            // $sql = "SELECT `Room_Count` FROM `booking_index` WHERE `Booking_ID`='".$Booking_ID."'";
            // $result = mysqli_query($conn,$sql);
            // $row = $result->fetch_assoc();
            // if($row['Room_Count']==$room_count)
            //     $sql = "UPDATE `booking_index` SET `Overall_Status`=2 WHERE `Booking_ID`='".$Booking_ID."'";
            // else
            //     $sql = "UPDATE `booking_index` SET `Overall_Status`=1 WHERE `Booking_ID`='".$Booking_ID."'";
            // if(!mysqli_query($conn,$sql)){
            //     echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            //     die;
            // }
            echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
        }
        else if($All){
            // $sql = "UPDATE `booking_index` SET `Overall_Status`=2 WHERE `Booking_ID`='".$Booking_ID."'";
            // if(!mysqli_query($conn,$sql)){
            //     echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            //     die;
            // }
            $sql = "UPDATE `booking_detail` SET `Room_Status`=1 WHERE `Booking_ID`='".$Booking_ID."'";
            if(!mysqli_query($conn,$sql)){
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
            echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
        }
    }

    function Check_Out($info,$conn){
        // $sql = "UPDATE `booking_detail` SET `Room_Status` = 2, `Remark`='".$result."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
    }

    function Select_Customer($info,$conn){
        $or_Statement = "`Customer_Email` ='".$_POST['Booking_Info']."' OR `Customer_SSID` ='".$_POST['Booking_Info']."' OR `Customer_Passport` = '".$_POST['Booking_Info']."'";
        $sql = "SELECT * FROM `customer` WHERE ".$or_Statement;
    }

    function Select_Room($CIN_Date, $COUT_Date, $Person_Count,$conn){
        $rooms = array();
        $sql = "SELECT * FROM `room_type`,`room_status` WHERE `room_type`.`Room_Type`=`room_status`.`Room_Type` AND `room_type`.`Tenant`>='".$Person_Count."' ORDER BY `room_type`.`Tenant` ASC";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $rooms[$row['Room_Type']]['Room_Type'] = $row['Room_Type'];
            $rooms[$row['Room_Type']]['Tenant'] = $row['Tenant'];
            $rooms[$row['Room_Type']]['Description'] = str_replace("\\n", chr(13).chr(10),nl2br($row['Description']));
            if(!isset($rooms[$row['Room_Type']]['Room_Count']) || empty($rooms[$row['Room_Type']]['Room_Count']))
                $rooms[$row['Room_Type']]['Room_Count'] = 0;
            $rooms[$row['Room_Type']]['Room_Count']++;
            $rooms[$row['Room_Type']]['WeekDay_Price'] = $row['WeekDay_Price'];
            $rooms[$row['Room_Type']]['Weekend_Price'] = $row['Weekend_Price'];
            $rooms[$row['Room_Type']]['WeekDay_Days'] = $row['WeekDay_Days'];
            $rooms[$row['Room_Type']]['Weekend_Days'] = $row['Weekend_Days'];
            
        }
        $sql = "SELECT `booking_detail`.`Room_Type`, COUNT(*) FROM `booking_index`,`booking_detail` WHERE (('$CIN_Date' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR ('$COUT_Date' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR (`booking_index`.`CIN_Date` BETWEEN '$CIN_Date' AND '$COUT_Date') OR (`booking_index`.`COUT_Date` BETWEEN '$CIN_Date' AND '$COUT_Date')) AND `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND (`booking_detail`.`Room_Status`!=5 AND `booking_detail`.`Room_Status`!=8) GROUP BY `booking_detail`.`Room_Type`";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            if(isset($rooms[$row['Room_Type']])){
                $rooms[$row['Room_Type']]['Room_Count']-=$row['COUNT(*)'];
                if($rooms[$row['Room_Type']]['Room_Count']<=0)
                    unset($rooms[$row['Room_Type']]);
            }
        }

        //$Total_Price;

        echo json_encode($rooms,JSON_UNESCAPED_UNICODE);
    }

    function Take_Room($CIN_Date, $COUT_Date, $Room_Type, $Total_Price, $People_Count,$conn){
        date_default_timezone_set('Asia/Taipei');
        $Booking_Date = date('Y/m/d', time());
        $Day_Count = date_diff(date_create($COUT_Date),date_create($CIN_Date))->format("%a");
        $sql = "SELECT COUNT(*) FROM `booking_index` WHERE `Booking_Date`='".$Booking_Date."'";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        $Booking_ID = strtoupper('B' . dechex(date('Y', time())) . dechex(date('m', time())) . dechex(date('d', time())) . dechex(date('h', time())) . dechex(date('i', time())) . dechex(date('s', time())) . str_pad($row['COUNT(*)']+1, 4, "0", STR_PAD_LEFT));
        $sql = "INSERT INTO `booking_index` (`Booking_ID`, `Booking_Date`, `Discount_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `Total_Price`,`People_Count`) VALUES ('".$Booking_ID."','".$Booking_Date."','2','".$CIN_Date."','".$COUT_Date."','".$Day_Count."','".$Total_Price."','".$People_Count."')";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        $room_array = array();
        $sql = "SELECT * FROM `room_status` WHERE `Room_Type`='".$Room_Type."'";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc())
            $room_array[] = $row['Room_Num'];

        $sql = "SELECT * FROM `booking_detail` WHERE `Room_Type`='".$Room_Type."'";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc())
            if(in_array($row['Room_Num'],$room_array))
                unset($room_array[$row['Room_Num']]);
        sort($room_array);
        $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Room_Num`, `Price`) VALUES ('".$Booking_ID."','".$Room_Type."','".$room_array[0]."','".$Total_Price."')";
        // echo $sql;die;
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        echo json_encode(array('Success' => true,'Booking_ID' => $Booking_ID,'Room_Num' => $room_array[0]),JSON_UNESCAPED_UNICODE);
    }

    function Cancel_Booking($Booking_ID,$conn){
        if(!isset($Booking_ID) || empty($Booking_ID)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        $sql = "DELETE FROM `booking_detail` WHERE `Booking_ID`='".$Booking_ID."'";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        $sql = "DELETE FROM `booking_index` WHERE `Booking_ID`='".$Booking_ID."'";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
    }

    function Get_Company_Info($conn){
        $sql = "SELECT * FROM `hotel_info`";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        echo json_encode($row,JSON_UNESCAPED_UNICODE);
    }

    function Get_Invoice_Variable($month,$conn){
        $sql = "SELECT * FROM `invoice_list`";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        if((int)$month >= (int)$row['Invoice_Start_Month'] && (int)$month <= (int)$row['Invoice_End_Month']){
            $Invoice_Aphabetic_Letter = $row['Invoice_Aphabetic_Letter'];
            $Invoice_Start = str_pad((int)$row['Invoice_Start'] + (int)$row['Invoice_Counter'],'8','0',STR_PAD_LEFT);
            $Invoice_Type = $row['Invoice_Type'];
            $Invoice_Counter = $row['Invoice_Counter'] + 1;

            $sql = "UPDATE `invoice_list` SET `Invoice_Counter`='".$Invoice_Counter."'";
            if(!mysqli_query($conn,$sql)){
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
            echo json_encode(array('Invoice_Aphabetic_Letter' => $Invoice_Aphabetic_Letter,'Invoice_Number' => $Invoice_Start,'Invoice_Type' => $Invoice_Type),JSON_UNESCAPED_UNICODE);
        }
        // else
        //     Get_New_Invoice_List((int)$month,$conn);
    }

    function Get_New_Invoice_List($month,$conn){
        $sql = "SELECT * FROM `hotel_info`";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        date_default_timezone_set($row['Timezone_Set']);
        $year = date('Y', time())-1911;
        if((int)$month==0)
            $month = date('m', time());
        if((int)$month%2==1)
            $month .= "-" . str_pad((int)$month+1,'2','0',STR_PAD_LEFT);
        else
            $month = str_pad((int)$month-1,'2','0',STR_PAD_LEFT) . "-" . $month;

        $param = array('year'=>$year,'month'=>$month,'officeid'=>$row['officeid'],'posno'=>$row['posno'],'posid'=>$row['posid']);
        $result = $client->__soapCall('ApplyInvoiceNumber', array($param));
        $applyid = $result->return;
        $param = array('applyid'=>$applyid,'officeid'=>$row['officeid'],'posno'=>$row['posno'],'posid'=>$row['posid']);
        $result = $client->__soapCall('TakeInvoiceNumber', array($param));
        $invoiceRange = explode(";",$result->return);
        
        $Invoice_Start_Month = date('m', time());
        if((int)$Invoice_Start_Month%2==1){
            $Invoice_End_Month = (int)$Invoice_Start_Month+1;
        }
        else{
            $Invoice_End_Month = $Invoice_Start_Month;
            $Invoice_Start_Month = (int)$Invoice_Start_Month-1;
        }
        $Invoice_Aphabetic_Letter = $invoiceRange[0];
        $Invoice_Start = $invoiceRange[1];
        $Invoice_End = $invoiceRange[2];
        $Invoice_Type = $invoiceRange[3];
        $Invoice_Counter = 0;
        $sql = "DELETE FROM `invoice_list` WHERE 1";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        $sql = "UPDATE `invoice_list` SET `Invoice_Aphabetic_Letter`='".$Invoice_Aphabetic_Letter."',`Invoice_Start_Month`='".$Invoice_Start_Month."',`Invoice_End_Month`='".$Invoice_End_Month."',`Invoice_Start`='".$Invoice_Start."',`Invoice_End`='".$Invoice_End."',`Invoice_Type`='".$Invoice_Type."',`Invoice_Counter`='".$Invoice_Counter."' WHERE 1";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        Get_Invoice_Variable($month,$conn);
    }

    function Upload_Invoice($Info,$conn){
        $Info = json_decode($Info,true);
        $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Staff_ID`, `Payment_Datetime`, `Invoive_Number`, `Random_Number`, `Upload_XML`, `BuyerID`, `Invoice_Status`) VALUES ('".$Info['Booking_ID']."','1','".$Info['Payment_Method_Num']."','".$Info['Amount']."','0','".$Info['Payment_Datetime']."','".$Info['Invoive_Number']."','".$Info['Random_Number']."','".$Info['Upload_XML']."','".$Info['BuyerID']."','".$Info['Invoice_Status']."')";
        if(!mysqli_query($conn,$sql)){
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }
        echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
    }
?>