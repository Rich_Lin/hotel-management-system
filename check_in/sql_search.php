<?php
header('Content-Type: application/json; charset=UTF-8');
include_once "../mysql_connect.inc.php";
// error_reporting(0);
if($_SERVER['REQUEST_METHOD'] == "POST") {
    //////////////////Customer_info//////////////////
        if($_POST["Category"]=='Customer_Info'){
            $info_array = array();
            if (isset($_POST["Customer_SSID"]) && !empty($_POST["Customer_SSID"])){
                $_POST["Customer_SSID"] = addslashes($_POST["Customer_SSID"]);
                $sql = "SELECT * FROM `customer` WHERE `Customer_SSID` = '".$_POST["Customer_SSID"]."'";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc())
                    foreach($row as $key => $value)
                        $info_array[$key] = $value;

                echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
            }
            else if(isset($_POST["Customer_Passport"]) && !empty($_POST["Customer_Passport"])){
                $_POST["Customer_Passport"] = addslashes($_POST["Customer_Passport"]);
                $sql = "SELECT * FROM `customer` WHERE `Customer_Passport` = '".$_POST["Customer_Passport"]."'";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc())
                    foreach($row as $key => $value)
                        $info_array[$key] = $value;

                echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
            }
            else {
                echo json_encode(array(
                    'errorMsg' => '資料未輸入完全！'
                ),JSON_UNESCAPED_UNICODE);
            }
        }
    //////////////////room_info//////////////////
        else if($_POST["Category"]=='Room_Info'){
            if($_POST["Check_In"] != '' && $_POST["Check_Out"] != ''){
                $CIN_Date = $_POST["Check_In"];
                $COUT_Date = $_POST["Check_Out"];
                $Tenant = array();
                $rooms = array();
                $sql = "SELECT `room_type`.`Room_Type` , `room_type`.`Tenant`, COUNT(`room_status`.`Room_Num`) FROM `room_type`, `room_status` WHERE `room_type`.`Room_Type`=`room_status`.`Room_Type` GROUP BY `room_type`.`Room_Type`";
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc()){
                    $rooms[$row['Room_Type']] = $row['COUNT(`room_status`.`Room_Num`)'];
                    $Tenant[$row['Room_Type']] = $row['Tenant'];
                }
                
                $sql = "SELECT `booking_detail`.`Room_Type`, COUNT(*) FROM `booking_index`,`booking_detail` WHERE (('$CIN_Date' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR ('$COUT_Date' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR (`booking_index`.`CIN_Date` BETWEEN '$CIN_Date' AND '$COUT_Date') OR (`booking_index`.`COUT_Date` BETWEEN '$CIN_Date' AND '$COUT_Date')) AND `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND (`booking_detail`.`Room_Status`!=5 AND `booking_detail`.`Room_Status`!=8) GROUP BY `booking_detail`.`Room_Type`";
                // echo $sql;
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc()){
                    foreach($rooms as $key => $value){
                        if($key == $row['Room_Type'])
                            $rooms[$key]-=$row['COUNT(*)'];
                    }
                }
                // echo "<pre>";
                // print_r($rooms);
                // print_r($Tenant);
                // echo "</pre>";die;
                echo "<table width='90%'>";
                foreach($rooms as $key => $value){
                    echo "
                        <tr>
                            <td>".$key."</td>
                            <td>
                                <input type='number' name='room[".$key."]' style='width:75px' value='0' min='0' max='".$value."' onchange='cal_needed(this.value*".$Tenant[$key].",\"".$key."\")' onKeyUp='cal_needed(this.value*".$Tenant[$key].",\"".$key."\")'>間
                                <input type='hidden' name='covered' id='".$key."' value='0'>
                            </<td>
                        </tr>";
                }
            }
            else echo "<p>請先選擇入住時間</p>";
        }
    //////////////////Get_Project//////////////////
        else if($_POST['Category']=='Get_Project'){
            if(!isset($_POST['Rooms'])){
                echo json_encode(array('0' => array('ID' => '', 'Name' => '選擇價格專案')),JSON_UNESCAPED_UNICODE);
                die;
            }

            $sql = "SELECT `Discount_ID`,`Discount_Name` FROM `discount` WHERE";
            for($i=0;$i<sizeof($_POST['Rooms']);$i++){
                if($i!=0)
                    $sql .= " AND";
                $sql .= " `Fits` LIKE '%".$_POST['Rooms'][$i]."%'";
            }
            $sql .= " AND `Enable`=1";
            $sql .= " AND ('".$_POST['Start']."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`)";
            $sql .= " AND ('".$_POST['End']."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`)";
            $result = mysqli_query($conn,$sql);
            $info_array = array();
            $i = 0;
            $info_array[$i]['ID'] = '';
            $info_array[$i]['Name'] = '選擇價格專案';
            $i = 1;
            while($row = $result->fetch_assoc()){
                $info_array[$i]['ID'] = $row['Discount_ID'];
                $info_array[$i]['Name'] = $row['Discount_Name'];
                $i++;
            }
            echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
        }
    //////////////////Discount_info//////////////////
        else if($_POST["Category"]=='Discount_Info'){
            if($_POST["Check_In"] != '' && $_POST["Check_Out"] != '' && $_POST["Room_Type"] != ''){
                $CIN_Date = $_POST["Check_In"];
                $COUT_Date = $_POST["Check_Out"];
                $Room_Type = $_POST["Room_Type"];
                $Discount_array = array();
                $Date = " (('".$CIN_Date."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`) OR ('".$COUT_Date."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`) OR (`Discount_Start_Date` BETWEEN '".$CIN_Date."' AND '".$COUT_Date."') OR (`Discount_End_Date` BETWEEN '".$CIN_Date."' AND '".$COUT_Date."'))";
                $sql = "SELECT * FROM `discount` WHERE `Enable`=1 AND `Fits` LIKE '%".$Room_Type."%' AND" . $Date;
                // echo "AAAAA: " . $sql;
                $result = mysqli_query($conn,$sql);
                while($row = $result->fetch_assoc()){
                    $Discount_array[] = $row;
                }
                // echo "<pre>";
                // print_r($Discount_array);
                // echo "</pre>";die;

                echo json_encode($Discount_array,JSON_UNESCAPED_UNICODE);
            }
        }
}
else {
    echo json_encode(array(
        'errorMsg' => '請求無效，只允許 POST 方式訪問！'
    ));
}
?>