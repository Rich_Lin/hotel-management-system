<?php
    include_once "../mysql_connect.inc.php";
    error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        $Paid = 0;
        $Total_Price = 0;

        if(isset($_POST['submit_button'])){
            date_default_timezone_set('Asia/Taipei');
            $Booking_Date = date('Y/m/d', time());
            $CIN_Date = $_POST['Check_In'] . " " . $_POST['Check_In_Time'] . ":00";
            $COUT_Date = $_POST['Check_Out'] . " " . $_POST['Check_Out_Time'] . ":00";
            $Day_Count = $_POST['Day_Count'];
            $Number_Of_People = $_POST['Number_Of_People'];

            $room = $_POST['room'];

            $Discount_ID = $_POST['Discount_ID'];
            $Remark = "";
            if(isset($_POST["Remark"]))
                $Remark = addslashes(str_replace("<br />", '',nl2br($_POST["Remark"])));

            $sql = "SELECT `Discount_Name` FROM `discount` WHERE `Discount_ID`='".$Discount_ID."'";
            $result = mysqli_query($conn,$sql);
            $row = $result->fetch_array();
            $Discount_Name = $row['Discount_Name'];
            $Customer_ID = "0";
            if(!isset($_POST['hold_room'])){
                if($_POST['Customer_ID']==''){
                    $sql = "SELECT `Customer_ID` FROM `customer` WHERE `Customer_Email`='".$_POST['Customer_Email']."' AND `Customer_Phone`='".$_POST['Customer_Phone']."'";
                    $result = mysqli_query($conn,$sql);
                    $row = mysqli_num_rows($result);
                    if($row == 0){
                        $sql = "INSERT INTO `customer`(`Customer_Name`, `Customer_Sex`, `Customer_Phone`, `Customer_Email`, `Customer_SSID`, `Customer_Passport`, `Customer_Nationality`) VALUES ('".$_POST['Customer_Name']."','".$_POST['Customer_Sex']."','".$_POST['Customer_Phone']."','".$_POST['Customer_Email']."','".$_POST['Customer_SSID']."','".$_POST['Customer_Passport']."','".$_POST['Customer_Nationality']."')";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                        $sql = "SELECT `Customer_ID` FROM `customer` WHERE `Customer_Email`='".$_POST['Customer_Email']."' AND `Customer_Phone`='".$_POST['Customer_Phone']."' ORDER BY `Customer_ID` DESC";
                        $result = mysqli_query($conn,$sql);
                    }
                    $row = $result->fetch_array();
                    $Customer_ID = $row['Customer_ID'];
                }
                else{
                    // $sql = "UPDATE `customer` SET `Customer_Name`='".$_POST['Customer_Name']."',`Customer_Sex`='".$_POST['Customer_Sex']."',`Customer_Phone`='".$_POST['Customer_Phone']."',`Customer_Email`='".$_POST['Customer_Email']."',`Customer_Nationality`='".$_POST['Customer_Nationality']."',`Customer_SSID`='".$_POST['Customer_SSID']."',`Customer_Passport`='".$_POST['Customer_Passport']."' WHERE `Customer_ID` = '".$_POST['Customer_ID']."'";
                    // if(!mysqli_query($conn,$sql)){
                    //     echo "This2 SQL: " . $sql . "<br>";
                    //     die;
                    // }
                    $Customer_ID = $_POST['Customer_ID'];
                }
            }

            $sql = "SELECT * FROM `discount` WHERE `Discount_ID`='".$Discount_ID."'";
            $result = mysqli_query($conn,$sql);
            $Discount_row = $result->fetch_assoc();

            $temp_date = date($CIN_Date);
            $room_array = array();
            $x=0;
            foreach($room as $key => $value){
                if($value>0){
                    $Total_Price = 0;
                    $sql = "SELECT * FROM `room_type` WHERE `Room_Type`='".$key."'";
                    $result = mysqli_query($conn,$sql);
                    $Room_row = $result->fetch_assoc();
                    for($i=0;$i<$Day_Count;$i++){
                        if(($temp_date <= $Discount_row['Discount_End_Date'] && $temp_date >= $Discount_row['Discount_Start_Date'])){
                            switch($Discount_row['Discount_Type']){
                                case '0':
                                    if(strpos($Room_row['WeekDay_Days'],date_format(date_create($temp_date),'w')) !== false)
                                        $Total_Price+=ceil($Room_row['WeekDay_Price']*($Discount_row['Cal_Method']/10));
                                    else
                                        $Total_Price+=ceil($Room_row['Weekend_Price']*($Discount_row['Cal_Method']/10));
                                break;

                                case '1':
                                    $Total_Price+=$Discount_row['Change_Price'];
                                break;
                            }
                        }
                        else{
                            if(strpos($Room_row['WeekDay_Days'],date_format(date_create($temp_date),'w')) !== false)
                                $Total_Price+=$Room_row['WeekDay_Price'];
                            else
                                $Total_Price+=$Room_row['Weekend_Price'];
                        }
                        $temp_date = date_format(date_create($temp_date)->modify('+1 day'), 'Y-m-d');
                    }
                    $room_array[$x]['Room_Type'] = $key;
                    $room_array[$x]['Room_Count'] = $value;
                    $room_array[$x]['Price'] = $Total_Price;
                    $x++;
                }
            }
            $Total_Price = 0;
            for($x=0;$x<sizeof($room_array);$x++){
                $Total_Price += $room_array[$x]['Room_Count'] * $room_array[$x]['Price'];
            }
    
            $sql = "SELECT COUNT(*) FROM `booking_index` WHERE `Booking_Date`='".$Booking_Date."'";
            $result = mysqli_query($conn,$sql);
            $row = $result->fetch_assoc();
            $Booking_ID = strtoupper('B' . dechex(date('Y', time())) . dechex(date('m', time())) . dechex(date('d', time())) . dechex(date('h', time())) . dechex(date('i', time())) . dechex(date('s', time())) . str_pad($row['COUNT(*)']+1, 3, "0", STR_PAD_LEFT));
            $sql = "INSERT INTO `booking_index` (`Booking_ID`, `Booking_Date`, `Customer_ID`, `Discount_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `Total_Price`, `Index_Remark`) VALUES ('".$Booking_ID."','".$Booking_Date."','".$Customer_ID."','".$Discount_ID."','".$CIN_Date."','".$COUT_Date."','".$Day_Count."','".$Total_Price."','".$Remark."')";
            if(isset($_POST['hold_room']) && $_POST['hold_room']=='1')
                $sql = "INSERT INTO `booking_index` (`Booking_ID`, `Booking_Date`, `Customer_ID`, `Discount_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `Total_Price`, `Index_Remark`, `Overall_Status`) VALUES ('".$Booking_ID."','".$Booking_Date."','".$Customer_ID."','".$Discount_ID."','".$CIN_Date."','".$COUT_Date."','".$Day_Count."','".$Total_Price."','".$Remark."','7')";
            if(!mysqli_query($conn,$sql)){
                echo "<br>This SQL: " . $sql . "<br>";
                die;
            }
            if($_POST['submit_button']=='auto'){
                for($i=0;$i<sizeof($room_array);$i++){
                    $sql = "SELECT * FROM `room_status` WHERE `Room_Type`='".$room_array[$i]['Room_Type']."'";
                    $result = mysqli_query($conn,$sql);
                    while($row = $result->fetch_assoc())
                    $room_array[$i]['empty_room'][] = $row['Room_Num'];
                }
                
                for($i=0;$i<sizeof($room_array);$i++){
                    $sql = "SELECT * FROM `booking_index`,`booking_detail` WHERE `booking_detail`.`Room_Type`='".$room_array[$i]['Room_Type']."' AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND (('".$CIN_Date."' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR ('".$COUT_Date."' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR (`booking_index`.`CIN_Date` BETWEEN '".$CIN_Date."' AND '".$COUT_Date."') OR (`booking_index`.`COUT_Date` BETWEEN '".$CIN_Date."' AND '".$COUT_Date."')) AND `booking_detail`.`Room_Status`!=5 AND `booking_detail`.`Room_Status`!=8";
                    $result = mysqli_query($conn,$sql);
                    while($row = $result->fetch_assoc())
                        if(in_array($row['Room_Num'],$room_array[$i]['empty_room']))
                            unset($room_array[$i]['empty_room'][array_search($row['Room_Num'],$room_array[$i]['empty_room'])]);
                    sort($room_array[$i]['empty_room']);
                }
                
                for($i=0;$i<sizeof($room_array);$i++){
                    for($j=0;$j<$room_array[$i]['Room_Count'];$j++){
                        $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Room_Num`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_array[$i]['Room_Type']."','".$room_array[$i]['empty_room'][$j]."','".$room_array[$i]['Price']."','0')";
                        if(isset($_POST['hold_room']) && $_POST['hold_room']=='1')
                            $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Room_Num`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_array[$i]['Room_Type']."','".$room_array[$i]['empty_room'][$j]."','".$room_array[$i]['Price']."','7')";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                    }
                }
            }
            else if($_POST['submit_button']=='manual'){
                for($i=0;$i<sizeof($room_array);$i++){
                    for($j=0;$j<$room_array[$i]['Room_Count'];$j++){
                        $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_array[$i]['Room_Type']."','".$room_array[$i]['Price']."','0')";
                        if(isset($_POST['hold_room']) && $_POST['hold_room']=='1')
                            $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_array[$i]['Room_Type']."','".$room_array[$i]['Price']."','7')";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                    }
                }
            }
        }
        else if(isset($_POST['Finish'])){
            $Changed_Price = $_POST['Total_Price']-$_POST['coupon'];
            $sql = "UPDATE `booking_index` SET `Total_Price`='".$Changed_Price."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
            $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$_POST['Booking_ID']."', '4', '3', '".$_POST['coupon']."', '".$_POST['Staff_ID']."', '".$_POST['payment_date']."')";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
            for($i=0;$i<2;$i++){
                if($_POST['payment'][$i]!=0){
                    $Remark = addslashes(str_replace("<br />", '',nl2br($_POST['Remark'][$i])));
                    $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Staff_ID`, `Payment_Datetime`, `Payment_Remark`) VALUES ('".$_POST['Booking_ID']."', '".$_POST['Payment_Type']."', '".$_POST['Payment_Method'][$i]."', '".$_POST['payment'][$i]."', '".$_POST['Staff_ID']."', '".$_POST['payment_date']."','".$Remark."')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                }
            }
            echo '<meta http-equiv=REFRESH CONTENT=0;url=../index.php>';
        }
        else if(isset($_POST['Next'])){
            $sql = "SELECT `Room_Count` FROM `booking_index` WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
            $result = mysqli_query($conn,$sql);
            $row = $result->fetch_assoc();
            for($i=0;$i<2;$i++){
                if($_POST['payment'][$i]!=0){
                    $Changed_Price -= $_POST['payment'][$i];
                    $Remark = addslashes(str_replace("<br />", '',nl2br($_POST['Remark'][$i])));
                    $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Payment_Method_Name`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$_POST['Booking_ID']."','".$_POST['Payment_Type']."','".$_POST['Payment_Method'][$i]."','".$_POST['Payment_Type_Name']."','".$_POST['payment'][$i]."','".$Remark."','".$_POST['Staff_ID']."','".$_POST['payment_date'][$i]."')";
                    // echo $i+1 . " SQL: " . $sql . "<br>";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                }
            }
            if($Changed_Price>0){
                if($Changed_Price == $_POST['Total_Price'])
                    $sql = "UPDATE `booking_index` SET `Payment_Status`='0' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                else
                    $sql = "UPDATE `booking_index` SET `Payment_Status`='1' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
            }
            else if($Changed_Price==0){
                $sql = "UPDATE `booking_index` SET `Payment_Status`='2' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
            }
            else{
                $sql = "UPDATE `booking_index` SET `Payment_Status`='".$Changed_Price."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
            }
            echo '<meta http-equiv=REFRESH CONTENT=0;url=../index.php>';
        }
        else{
            echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
        }
    }
?>
<html>
    <head>
        <title>客戶入住</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <link rel="stylesheet" type="text/css" href="lightpick.css">

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            td {
                border: none;
            }
        </style>

    </head>    

    <script type="text/javascript" src="../functions.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="lightpick.js"></script>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center><div>
                <p id='result'></p>
                <div><form action='paying.php' method='POST' id='reservation'>
                    <input type='hidden' id='Booking_ID' name='Booking_ID' value='<?php echo $Booking_ID;?>'>
                    <input type='hidden' id='Total_Price' name='Total_Price' value='<?php echo $Total_Price;?>'>
                    <input type='hidden' id='Total_Price' name='Room_Count' value='<?php echo $Room_Count;?>'>
                    <table border='0' id='myTable' cellspacing='0' width='35%'>
                        <tr>
                            <td width='33%' style='font-size:1.5vw'>確認付款資訊：</td>
                            <td style='text-align:left;font-size:2vw' width='33%'>待收金額：</td>
                            <td id='Unpaid' width='33%' style='font-size:2vw'><?php echo $Total_Price-$Paid;?></td>
                        </tr><tr>
                            <td></td>
                            <td style='text-align:left;font-size:1.25vw'><p style='display:inline'>已收金額：</p><p id='Paid' style='display:inline'><?php echo $Paid;?></p></td>
                            <td style='font-size:1.25vw'><p style='display:inline'>總額：</p><p id='Total' style='display:inline'><?php echo $Total_Price;?></p></td>
                        </tr>
                        <tr>
                            <td colspan='3' style='font-size:1.25vw'>$<span id='Original'><?php echo $Total_Price;?></span> - <input type='number' min='0' value='0' name='coupon' id='coupon' style='font-size:1.25vw' onchange='cal_Total_Price()' onKeyUp='cal_Total_Price()'> = $<span id='cal'><?php echo $Total_Price;?></span></td>
                        </tr>
                        <tr>
                            <td colspan='3' style='font-size:1.25vw'>使用專案：<?php echo $Discount_Name;?></td>
                        </tr>
                        <tr>
                        <td style='font-size:1.25vw' colspan='3'>收款類型：
                                <select name='Payment_Type' style='font-size:1.25vw' onchange='selection_cahange(this.value)'>
                                    <option value=''>選擇收款類型</option>
                                    <option value='6'>預先授權</option>
                                    <option value='0'>訂金</option>
                                    <option value='1'>現場收款</option>
                                    <option value='2'>月結簽帳</option>
                                    <option value='3'>帳務調整</option>
                                    <option value='4'>退款</option>
                                    <option value='5'>其它</option>
                                </select>
                                <input type='text' name='Payment_Type_Name' id='Payment_Method_Name' style='display: none'>
                                <input type='hidden' name='Staff_ID' value='<?php echo $_COOKIE['Staff_ID']?>'>
                            </td>
                        </tr>
                        <tr>
                            <td style='font-size:1.25vw' colspan='3'>收款日期：<input type='text' id='payment_date1' name='payment_date' style='font-size:1.25vw'></td>
                        </tr>
                        <tr>
                            <td colspan='3'><center>
                                <div id='payment_area'>
                                    <div>
                                        <br><br>
                                        <table border='1'>
                                            <tr>
                                                <td style='font-size:1.25vw'>收款方式：</td>
                                                <td>
                                                    <select name='Payment_Method[]' style='font-size:1.25vw'>
                                                        <option value='0'>現金</option>
                                                        <option value='1'>信用卡</option>
                                                        <option value='2'>轉帳</option>
                                                        <option value='3'>其它</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style='font-size:1.25vw'>收款金額：</td>
                                                <td>
                                                    <input type='number' class='payments' name='payment[]' style='font-size:1.25vw' min='0' value='0'>&nbsp;&nbsp;&nbsp;<label><input type='checkbox' id='check_all' onClick='toogle(this)'>同代收金額</label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style='text-align:right;font-size:1.25vw'>備註：</td>
                                                <td colspan='2'>
                                                    <textarea name='Remark[]' style='font-size:1.25vw'></textarea>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </center></td>
                        </tr>
                        <tr><td colspan='3' style='text-align:center'><input type='button' id='add_payment' style='width:100%;font-size:2vw' value="+　增加" onclick='show_another()'></td></tr>
                        <tr><td colspan='3' style='text-align:center'><input type='submit' name='Finish' style='width:65%;font-size:2vw' value='完成'></td></tr>
                    </table>
                </form></div>
            </div></center>
        </div>
        <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
    </body>
</html>

<script>
    function show_another(){
        var new_payment = "<table border='1' style='margin-top:25px;margin-bottom:25px;'><tr><td style='font-size:1.25vw'>收款方式：</td><td><select name='Payment_Method[]' style='font-size:1.25vw'><option value='0'>現金</option><option value='1'>信用卡</option><option value='2'>轉帳</option><option value='3'>其它</option></select></td></tr><tr><td style='font-size:1.25vw'>收款金額：</td><td><input type='number' class='payments' name='payment[]' style='font-size:1.25vw' min='0' value='0'>&nbsp;&nbsp;&nbsp;<label><input type='checkbox' id='check_all' onClick='toogle(this)'>同代收金額</label></td></tr><tr><td style='text-align:right;font-size:1.25vw'>備註：</td><td colspan='2'><textarea name='Remark[]' style='font-size:1.25vw'></textarea></td></tr></table>";
        document.getElementById("payment_area").innerHTML += new_payment;
        document.getElementById("add_payment").style.display = "none";
    }

    function cal_Total_Price(){
        var x = document.getElementById("Original").innerHTML - document.getElementById("coupon").value;
        document.getElementById("Total").innerHTML = x;
        document.getElementById("cal").innerHTML = x;
    }

    function selection_cahange(value){
        if(value==5){
            document.getElementById("Payment_Method_Name").setAttribute("style", "display: block");
            document.getElementById("Payment_Method_Name").setAttribute("required", "true");
        }
        else{
            document.getElementById("Payment_Method_Name").setAttribute("style", "display: none");
            document.getElementById("Payment_Method_Name").removeAttribute("required");
        }
    }

    function toogle(source){
        if(source.checked){
            for(i=0;i<document.getElementsByClassName("payments").length;i++)
                document.getElementsByClassName("payments")[i].value=document.getElementById("cal").innerHTML;
        }
        else{
            for(i=0;i<document.getElementsByClassName("payments").length;i++)
                document.getElementsByClassName("payments")[i].value="0";
        }
    }
    ///////////////////////////////////////////////////////////////////
    var picker = new Lightpick({
        field: document.getElementById('payment_date1'),
        minDate: moment().startOf('day'),
        startDate: moment().startOf('day'),
        singleDate: true,
        repick: true
            // Day count is disabled at line 719
    });

    var picker = new Lightpick({
        field: document.getElementById('payment_date2'),
        minDate: moment().startOf('day'),
        startDate: moment().startOf('day'),
        singleDate: true,
        repick: true
            // Day count is disabled at line 719
    });
</script>