<?php
    $sex = array('女','男','其他');
?>
<!DOCTYPE html>
<html>
    <head>
        <title>客戶入住</title>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <link rel="stylesheet" type="text/css" href="lightpick.css">
        <script type="text/javascript" src="../functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="lightpick.js"></script>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            *{
                font-size:20px
            }
            textarea{
                border-radius:8px;
                margin:10px;
            }
            input,button,select{
                border-radius:8px;
                height: 35px;
                margin:10px;
            }
            input[type=checkbox], input[type=radio] {
                vertical-align: middle;
                position: relative;
                bottom: 1px;
                zoom: 2;
            }
        </style>
    </head>
    
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center><div>
                <p id='result'></p>
                <div style='width:68%;border: 1px solid GREY;border-radius:15px;'>
                    <form action='paying.php' method='POST' id='reservation'>
                        <input type='hidden' id='Customer_ID' name='Customer_ID' value=''>
                        <div style='width:46%;display:inline-block;vertical-align:top;table-layout: fixed;'><br>
                            <table border='0' cellspacing='0'><tr>
                                <tr>
                                    <td style='text-align:right;width:20%'>選擇日期：</td>
                                    <td colspan='2'>
                                        <div style='float:left;text-align:center;'>
                                            <input type='text' style='width:150px;margin-bottom:2.5px;' id='check_in' name='Check_In' value='' placeholder='YY/MM/DD' required><br>
                                            <input type='time' name='Check_In_Time' style='margin-top:2.5px;' value='10:00'>
                                        </div>
                                        <div style='float:left;margin-top:44px;'> ～ </div>
                                        <div style='float:left;text-align:center;'>
                                            <input type='text' style='width:150px;margin-bottom:2.5px;' id='check_out' name='Check_Out' value='' placeholder='YY/MM/DD' required><br>
                                            <input type='time' name='Check_Out_Time' style='margin-top:2.5px;' value='12:00'>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>入住天數：</td>
                                    <td colspan='2'><input type='text' name='Day_Count' id='Day_Count' value='' readonly>天</td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>入住人數：</td>
                                    <td colspan='2'><input type='number' id='Number_Of_People' name='Number_Of_People' value='' min='1' onchange='counter(this)'>人</td>
                                </tr>
                                <tr>
                                    <td style='text-align:right;vertical-align:top;padding-top:20px'>
                                        房型：
                                    </td>
                                    <td style='padding-top:20px'>
                                        <div id='rooms'>請先選擇入住時間</div>
                                        <p id='needed_text'></p>
                                        <input type='hidden' id='needed' value='0'>
                                    </td>
                                </tr>
                                <tr>
                                    <td style='text-align:right'>價格專案：</td>
                                    <td colspan='2'><select id='Discount_ID' name='Discount_ID' style='width:200px' class='modified_select' required><option value=''>選擇價格專案</option></select>&nbsp;&nbsp;<label><input type='checkbox' name='hold_room' value='1' onChange='hold_room_fnc(this)'>保留房</label></td>
                                </tr>
                            </table>
                        </div>
                        <div style='width:53%;display:inline-block;vertical-align:top;table-layout: fixed;'><br>
                            <table border='0' cellspacing='0'>
                                <tr><td style='text-align:right'>訂房人資訊：</td><td style='text-align: center'><input type='button' id='clear_button' onclick='clear_value()' value='清除顧客資訊'></td></tr>
                                <tr><td style='text-align:right'>顧客姓名：</td><td colspan='2'><input type='text' class='Customer_Info' name='Customer_Name' id='Customer_Name'  value='' required></td></tr>
                                <tr><td style='text-align:right'>顧客電話：</td><td colspan='2'><input type='text' class='Customer_Info' name='Customer_Phone' id='Customer_Phone' onkeyup='check_info(this)' value='' required></td></tr>
                                <tr><td style='text-align:right'>顧客信箱：</td><td colspan='2'><input type='text' class='Customer_Info' name='Customer_Email' id='Customer_Email' onkeyup='ValidateEmail(this.value);check_info(this);' value='' required></td></tr>
                                <tr><td style='text-align:right'>顧客性別：</td><td colspan='2'><select name='Customer_Sex' style='width:100px;' class='Customer_Info modified_select' id='Customer_Sex'>
                                <?php
                                for($i=0;$i<count($sex);$i++)
                                    echo "<option value='".$i."'>".$sex[$i]."</option>";
                                ?>
                                </select>
                                </td></tr>
                                <tr><td  style='text-align:right'>顧客國籍：</td><td colspan='2'><input type='text' class='Customer_Info' name='Customer_Nationality' id='Customer_Nationality' value=''></td></tr>
                                <tr><td  style='text-align:right'>顧客身分證：</td><td colspan='2'><input type='text' class='Customer_Info' name='Customer_SSID' id='Customer_SSID' value=''></td></tr>
                                <tr><td  style='text-align:right'>顧客護照號碼：</td><td colspan='2'><input type='text' class='Customer_Info' name='Customer_Passport' id='Customer_Passport' value=''></td></tr>

                                <tr><td  style='text-align:right'>備註：</td><td colspan='2'><textarea rows='3' cols='40' class='Customer_Info' name='Remark' style='resize: none;'></textarea></td></tr>

                                <tr><td colspan='2' style='text-align:center'><button type='submit' name='submit_button' value='manual'>手動排房</button><button type='submit' name='submit_button' value='auto'>自動排房</button></td></tr>
                            </table>
                        </div>
                    </form>
                </div>
            </div></center>
        </div>    
        <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
    </body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    var total = 0;
    var room_array = new Array();
    var picker = new Lightpick({
        field: document.getElementById('check_in'),
        secondField: document.getElementById('check_out'),
        singleDate: false,
        minDate: moment().startOf('day'),
        repick: true,
        onSelect: function(start, end){
            var str = '';
            var count = 0;
            str += start ? start.format('YYYY/MM/DD') + ' 到 ' : '';
            str += end ? end.format('YYYY/MM/DD') : '...';
            if((end - start)>=0)
                count = Math.floor((end - start)/86400000);
            document.getElementById('Day_Count').value = count;
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Category: 'Room_Info',
                    Check_In: $("#check_in").val(),
                    Check_Out: $("#check_out").val()
                },
                success: function(data) {
                    if (data.Counter) {
                        $("#result").html('<font color="#ff0000">找到房間</font>');
                    } else {
                        $("#result").html('<font color="#ff0000">無房間</font>');
                    }
                },
                error: function(jqXHR) {
                    $("#room_type").val('');
                    $("#Room_Count").val('');
                    $("#rooms").html(jqXHR.responseText);
                }
            })
            //Day count is disabled at line 719
        }
    });
    ///////////////////////////////////////////////////////////////////////////////////////

    function check_info(target){
        if(target.value != ""){
            switch(target.id){
                case 'Customer_Phone':
                    document.getElementById("Customer_Email").required  = false;
                break;

                case 'Customer_Email':
                    document.getElementById("Customer_Phone").required  = false;
                break;
            }
        }
        else{
            switch(target.id){
                case 'Customer_Phone':
                    document.getElementById("Customer_Email").required  = true;
                break;

                case 'Customer_Email':
                    document.getElementById("Customer_Phone").required  = true;
                break;
            }
        }
    }

    function clear_value(){
        document.getElementById("Customer_Name").value = '';
        document.getElementById("Customer_Phone").value = '';
        document.getElementById("Customer_Email").value = '';
        document.getElementById("Customer_Phone").required = true;
        document.getElementById("Customer_Email").required = true;
        document.getElementById("Customer_Nationality").value = '';
        document.getElementById("Customer_Passport").value = '';
        document.getElementById("Customer_ID").value = '';
        document.getElementById("Customer_SSID").value = '';
        $("#Customer_Sex").children().each(function(){$(this).removeAttr("selected");});
    }

    function ValidateEmail(mail) {
        var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (mail.match(mailformat)){
            // alert("此為合法的信箱");
            for(i=0;i<document.getElementsByName("submit_button").length;i++)
                document.getElementsByName("submit_button")[i].disabled = false;
            return (true);
        }
        else if(mail==""){
            for(i=0;i<document.getElementsByName("submit_button").length;i++)
                document.getElementsByName("submit_button")[i].disabled = false;
            return (true);
        }
        // alert("此為不合法的信箱");
        // return (false);
        for(i=0;i<document.getElementsByName("submit_button").length;i++)
            document.getElementsByName("submit_button")[i].disabled = true;
    }

    function counter(target){
        if(target.value == '' || target.value<=0)
            document.getElementById(target.id).value = 1;
            check_needs(target.id);
    }

    function choose_type(target){
        document.getElementById('room_type').value = target.id;
        document.getElementById('Room_Count').setAttribute('max', target.value);
        document.getElementById('Room_Count').value=1;
        check_needs(target.id);
        $.ajax({
            type: "POST",
            url: "sql_search.php",
            dataType: "json",
            data: {
                Category: 'Discount_Info',
                Check_In: $("#check_in").val(),
                Check_Out: $("#check_out").val(),
                Room_Type: $("#room_type").val()
            },
            success: function(data) {
                // $("#result").html('<font color="#ff0000">AAAAAAAAA</font>');
                $("#Discount_ID option").remove();
                $('#Discount_ID').append('<option value="">選擇價格專案</option>');
                for(i=0;i<data.length;i++){
                    var Discount_ID = data[i].Discount_ID;
                    var Discount_Name = data[i].Discount_Name;
                    $('#Discount_ID').append('<option value="' + Discount_ID +'">' + Discount_Name + '</option>');
                }
            },
            error: function(jqXHR) {
                $("#result").html(jqXHR.responseText);
            }
        })
    }

    function hold_room_fnc(target){
        if(target.checked){
            for(i=0;i<document.getElementsByClassName("Customer_Info").length;i++){
                document.getElementsByClassName("Customer_Info")[i].disabled = true;
                document.getElementsByClassName("Customer_Info")[i].required = false;
                if(document.getElementsByClassName("Customer_Info")[i].type=='select-one')
                    document.getElementsByClassName("Customer_Info")[i].selectedIndex = "0";
                else
                    document.getElementsByClassName("Customer_Info")[i].value = '';
            }
        }
        else{
            for(i=0;i<document.getElementsByClassName("Customer_Info").length;i++){
                document.getElementsByClassName("Customer_Info")[i].disabled = false;
                /*
                    if(document.getElementsByClassName("Customer_Info")[i].name=='Customer_SSID' || document.getElementsByClassName("Customer_Info")[i].name=='Customer_Passport')
                        document.getElementsByClassName("Customer_Info")[i].required = true;
                */
            }
        }
    }

    function check_needs(){
        if($("#Number_Of_People").val()!='' || parseInt($("#Number_Of_People").val())>0){
            if($("#Number_Of_People").val()>total)
                $("#needed_text").html("<font color='#ff0000'>(還不足" + (parseInt($("#Number_Of_People").val())-total) + "人的空間)</font>");
            else
                $("#needed_text").html("");
        }
    }

    function cal_needed(value,id){
        $("#"+id).val(value);
        total = 0;
        room_array = [];
        for(i=0;i<document.getElementsByName("covered").length;i++){
            total += parseInt(document.getElementsByName("covered")[i].value);
            if(parseInt(document.getElementsByName("covered")[i].value)>0)
                room_array.push(document.getElementsByName("covered")[i].id);
        }
        check_needs();
        get_project();
    }

    function get_project(){
        $.ajax({
            type: "POST",
            url: "sql_search.php",
            dataType: "json",
            data: {
                Category: 'Get_Project',
                Rooms: room_array,
                Start: $("#check_in").val(),
                End: $("#check_out").val(),
            },
            success: function(data) {
                $("#Discount_ID").empty();
                for(i=0;i<data.length;i++){
                    $("#Discount_ID").append("<option value='"+data[i].ID+"'>"+data[i].Name+"</option>");
                }
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    $(document).ready(function() {
        $("#Customer_SSID").keyup(function() {
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Category: 'Customer_Info',
                    Customer_SSID: $("#Customer_SSID").val()
                },
                success: function(data) {
                    if (data.Customer_ID) {
                        // $("#result").html('<font color="#ff0000">找到使用者</font>');
                        $("#Customer_Name").val(data.Customer_Name);
                        $("#Customer_Sex").children().each(function(){if ($(this).val()==data.Customer_Sex) $(this).attr("selected", "true");});
                        $("#Customer_Phone").val(data.Customer_Phone);
                        $("#Customer_Email").val(data.Customer_Email);
                        $("#Customer_Phone").prop('required',false);
                        $("#Customer_Email").prop('required',false);
                        $("#Customer_Nationality").val(data.Customer_Nationality);
                        $("#Customer_Passport").val(data.Customer_Passport);
                        $("#Customer_ID").val(data.Customer_ID);
                        // $("#Customer_SSID").prop('required',false);
                        // $("#Customer_Passport").prop('required',false);
                    } else {
                        // $("#result").html('<font color="#ff0000">無此使用者</font>');
                        $("#Customer_ID").val('');
                        $("#Customer_Sex").children().each(function(){
                                $(this).removeAttr("selected");
                        });
                        $("#Customer_Phone").prop('required',true);
                        $("#Customer_Email").prop('required',true);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
        });
        $("#Customer_Passport").keyup(function() {
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Category: 'Customer_Info',
                    Customer_Passport: $("#Customer_Passport").val()
                },
                success: function(data) {
                    if (data.Customer_ID) {
                        // $("#result").html('<font color="#ff0000">找到使用者</font>');
                        $("#Customer_Name").val(data.Customer_Name);
                        $("#Customer_Sex").children().each(function(){if ($(this).val()==data.Customer_Sex) $(this).attr("selected", "true");});
                        $("#Customer_Phone").val(data.Customer_Phone);
                        $("#Customer_Email").val(data.Customer_Email);
                        $("#Customer_Phone").prop('required',false);
                        $("#Customer_Email").prop('required',false);
                        $("#Customer_Nationality").val(data.Customer_Nationality);
                        $("#Customer_SSID").val(data.Customer_SSID);
                        $("#Customer_ID").val(data.Customer_ID);
                        // $("#Customer_SSID").prop('required',false);
                        // $("#Customer_Passport").prop('required',false);
                    } else {
                        // $("#result").html('<font color="#ff0000">無此使用者</font>');
                        $("#Customer_ID").val('');
                        $("#Customer_Sex").children().each(function(){
                                $(this).removeAttr("selected");
                        });
                        $("#Customer_Phone").prop('required',true);
                        $("#Customer_Email").prop('required',true);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
        });
    });

</script>