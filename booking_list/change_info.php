<?php
    include_once "../mysql_connect.inc.php";

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST['Action'])){
            $Booking_ID = explode("_",$_POST['Action']);
            $Action = $Booking_ID[1];
            $Booking_ID = $Booking_ID[0];
        }
        else
            echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
    }
?>
<html>
    <head>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="../functions.js"></script>
        
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <script type="text/javascript" src="../lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../lightpick.css">
        <style>
            .tabcontent {
                display: none;
                padding: 6px 12px;
                border: 1px solid #ccc;
                margin-bottom: 50px;
            }
            .active{
            }
            .disabled{
            }
            .tablinks{
                width: 250px;
                height: 80px;
                font-size: 30px;
                color: WHITE;
                background-color: #0091FF;
                border-radius:15px;
            }
            td{
                padding:10px
            }
            #room_status_table tr:nth-child(odd){
                background-color: #f2f2f2;
            }
            #room_status_table th {
                padding:10px;
                text-align: left;
                background-color: #6236FF;
                color: WHITE;
            }
            #payment_status_table tr:nth-child(odd){
                background-color: #f2f2f2;
            }
            #payment_status_table th {
                padding:10px;
                text-align: left;
                background-color: #6236FF;
                color: WHITE;
            }
            .modified_select:-moz-focusring {
                color: transparent;
                text-shadow: 0 0 0 #000;
            }
            .modified_select{
                border: solid 3px #DADADA;
                -webkit-appearance: none;
                -moz-appearance: none;
                background: url("/hotel_management_system/images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
                background-size: 18.51px 16.03px;
                background-origin: content-box;
                padding-left: 10px;
                padding-right: 10px;
                background-repeat: no-repeat;
                border-radius:7.5px;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                /* border-width: 20px; */
                /* background-color: #DADADA;
                border: 1px solid #DADADA; */
            }
            .ui-widget-content {
                /* border-radius: 20px; */
                /* border-width: 20px; */
            }
            .ui-widget-overlay{
                /* background-color: transparent; */
            }
            #modify_btn{
                display: none;
            }
            .info_edit_column{
                display: none
            }
            .Customer_Info_Input{
                display:none;
            }
            .extra_expense_tr{
                display: none;
            }
            #extra_expense_table{
                table-layout: fixed;
                overflow: hidden;
                font-size: 25px;
                border-collapse: collapse;
                width: 100%;
            }
            #extra_expense_table td{
                table-layout: fixed;
                overflow: hidden;
                white-space: nowrap;
                text-overflow: ellipsis;
            }
            #extra_expense_table tr:nth-child(odd){
                background-color: #f2f2f2;
            }
            #extra_expense_table th {
                padding:10px;
                text-align: center;
                background-color: #6236FF;
                color: WHITE;
            }
            input, select{
                font-size: 26px;
                border-radius: 7.5px;
            }
            .modified_select:-moz-focusring {
                color: transparent;
                text-shadow: 0 0 0 #000;
            }
            .modified_select{
                border: solid 3px #DADADA;
                -webkit-appearance: none;
                -moz-appearance: none;
                background: url("/hotel_management_system/images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
                background-size: 18.51px 16.03px;
                background-origin: content-box;
                padding-left: 10px;
                padding-right: 10px;
                background-repeat: no-repeat;
                border-radius:15px;
            }
        </style>
    </head>
    <body onload="includeHTML();display_expense();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <?php
                $Sex_Array = array('女','男','其他');
                $Room_Status = array('未入住','入住中','已退房(待打掃)','已退房(打掃中)','休息中','已退房(打掃完成)','清掃中','保留中','已取消');
                echo "
                    <center>
                        <table width='95%' border='0'>
                            <tr>
                                <td style='width:150px;text-align:center'>
                                    <button style='width:130px;height:50px;font-size:24px' onclick='goback()'>返回</button>
                                </td>
                                <td>
                                    <button class='tablinks"; if($Action=='info') echo " active disabled"; echo "' onclick='openTab(event, \"info\")'>訂單資訊</button>
                                    <button class='tablinks"; if($Action=='payment') echo " active"; echo "' onclick='openTab(event, \"payment\")'>收款結帳</button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <div id='info' class='tabcontent'"; if($Action=='info') echo " style='display: block;'"; echo ">";
                                        $sql = "SELECT * FROM `booking_index`,`customer`,`booking_detail`,`room_type` WHERE `booking_index`.`Booking_ID`='".$Booking_ID."' AND `customer`.`Customer_ID`=`booking_index`.`Customer_ID` AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `room_type`.`Room_Type`=`booking_detail`.`Room_Type`";
                                        // echo $sql;
                                        $result = mysqli_query($conn,$sql);
                                        $row = $result->fetch_assoc();
                                        $max_count = mysqli_num_rows($result) * $row['Tenant'];
                                        echo "<table style='font-size:25px' width='100%'>
                                            <tr>
                                                <td width='50%'>訂單編號：<span style='color:#0091FF' id='Booking_ID'>".$Booking_ID."</span></td>
                                                <td width='50%' style='text-align:right'>
                                                    <input type='button' style='width:130px;height:50px;font-size:20px;margin:12.5px;border-radius:15px' id='edit' value='編輯' onClick='modify_infos()'";if($row['Overall_Status']=='8') echo " disabled"; echo ">
                                                    <input type='button' style='width:180px;height:50px;font-size:20px;margin:12.5px;border-radius:15px' id='print' value='列印訂單'";if($row['Overall_Status']=='8') echo " disabled"; echo ">
                                                    <input type='button' style='width:180px;height:50px;font-size:20px;margin:12.5px;border-radius:15px;background-color:#F79B00;color:WHITE' id='add_expense' value='新增消費'";if($row['Overall_Status']=='8') echo " disabled"; echo ">
                                                    <input type='hidden' id='Overall_Status' value='".$row['Overall_Status']."'>
                                                    <input type='hidden' id='Customer_ID' name='Customer_ID' value='".$row['Customer_ID']."'>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='2' height='10px'><span id='result'></span></td>
                                            </tr>
                                            <tr>
                                                <td>訂房日期：".str_replace('-','/',$row['Booking_Date'])."</td>
                                                <td>旅客姓名：<span class='Customer_Info_Text'>".$row['Customer_Name']."</span>
                                                            <input type='text' class='Customer_Info_Input' style='font-size:26px;border-radius:7.5px;' id='Customer_Name' name='Customer_Name' value='".$row['Customer_Name']."'></td>
                                            </tr>
                                            <tr>
                                                <td>入住日期：<span id='CIN_Date_text' class='display_text'>".str_replace('-','/',$row['CIN_Date'])."</span>
                                                            <input type='text' class='info_edit_column' id='CIN_Date' style='font-size: 26px;border-radius: 7.5px;' name='CIN_Date' value='".str_replace('-','/',$row['CIN_Date'])."'></td>
                                                <td>性別：<span class='Customer_Info_Text'>".$Sex_Array[$row['Customer_Sex']]."</span>
                                                        <select class='Customer_Info_Input modified_select' style='font-size:26px;width:220;height:50;' name='Customer_Sex' id='Customer_Sex'>";
                                                            for($i=0;$i<sizeof($Sex_Array);$i++){
                                                                echo "aaaaaa<br>";
                                                                echo "<option value='".$i."'";
                                                                if($row['Customer_Sex']==$i) echo " selected";
                                                                echo ">".$Sex_Array[$i]."</option>";
                                                            }
                                                        echo "</select></td>
                                            </tr>
                                            <tr>
                                                <td>退房日期：<span class='display_text'>".str_replace('-','/',$row['COUT_Date'])."</span>
                                                            <input type='text' class='info_edit_column' id='COUT_Date' style='font-size: 26px;border-radius: 7.5px;' name='COUT_Date' value='".str_replace('-','/',$row['COUT_Date'])."'></td>
                                                <td>國籍：<span class='Customer_Info_Text'>".$row['Customer_Nationality']."</span>
                                                        <input type='text' class='Customer_Info_Input' style='font-size:26px; border-radius:7.5px;' id='Customer_Nationality' name='Customer_Nationality' value='".$row['Customer_Nationality']."'></td>
                                            </tr>
                                            <tr>
                                                <td>入住天數：<span id='Day_Count_Text'>".$row['Day_Count']."</span><input type='hidden' id='Day_Count' name='Day_Count' value='".$row['Day_Count']."'></td>
                                                <td>身分證：<span class='Customer_Info_Text'>".$row['Customer_SSID']."</span>
                                                            <input type='text' class='Customer_Info_Input' style='font-size:26px; border-radius:7.5px;' id='Customer_SSID' name='Customer_SSID' value='".$row['Customer_SSID']."'></td>
                                            </tr>
                                            <tr>
                                                <td>入住人數：<span class='display_text'>".$row['People_Count']."</span>
                                                            <input type='number' class='info_edit_column' style='font-size: 26px;border-radius: 7.5px;' id='People_Count' name='People_Count' min='0' max='$max_count' value='".$row['People_Count']."'></td>
                                                <td>護照號碼：<span class='Customer_Info_Text'>".$row['Customer_Passport']."</span>
                                                            <input type='text' class='Customer_Info_Input' style='font-size:26px; border-radius:7.5px;' id='Customer_Passport' name='Customer_Passport' value='".$row['Customer_Passport']."'></td>
                                            </tr>
                                            <tr>
                                                <td>總價：<span id='Total_Price_V'>".$row['Total_Price']."</span><input type='hidden' id='Total_Price' name='Total_Price' value='".$row['Total_Price']."'></td>
                                                <td>連絡電話：<span class='Customer_Info_Text'>".$row['Customer_Phone']."</span>
                                                            <input type='text' class='Customer_Info_Input' style='font-size:26px; border-radius:7.5px;' id='Customer_Phone' name='Customer_Phone' value='".$row['Customer_Phone']."'></td>
                                            </tr>
                                            <tr>
                                                <td>備註：<br><span class='display_text'>".str_replace(chr(13).chr(10), "<br />",nl2br($row['Index_Remark']))."</span>
                                                        <textarea style='display:none' id='Original_Index_Remark'>".$row['Index_Remark']."</textarea>
                                                        <textarea class='info_edit_column' style='font-size:26px;border-radius:7.5px;' rows='3' cols='40' id='Index_Remark' name='Index_Remark'>".$row['Index_Remark']."</textarea></td>
                                                <td style='vertical-align:top'>E-Mail：<span class='Customer_Info_Text'>".$row['Customer_Email']."</span>
                                                                                <input type='text' class='Customer_Info_Input' style='font-size:26px; border-radius:7.5px;' id='Customer_Email' name='Customer_Email' value='".$row['Customer_Email']."'></td>
                                            </tr>
                                            <tr>
                                                <td colspan='2' height='30px'></td>
                                            </tr>
                                            <tr>
                                                <td colspan='2'>
                                                    <table id='room_status_table' border='1' style='font-size:25px;border-collapse:collapse;' width='100%'>
                                                        <tr class='title'>
                                                            <th>房號</th>
                                                            <th>實際入住時間</th>
                                                            <th>房單價格</th>
                                                            <th>房間狀態</th>
                                                            <th>房卡張數</th>
                                                            <th>備註</th>
                                                        </tr>";
                                                    $result = mysqli_query($conn,$sql);
                                                    while($rows = $result->fetch_assoc()){
                                                        echo "<tr>";
                                                        if($rows['Room_Status'] == "0" || $rows['Room_Status'] == "7"){
                                                            if(empty($rows['Room_Num'])/* || $rows['Keycard_Counter']==0*/){
                                                                echo "<td><label><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' disabled>".$rows['Room_Num']."</label></td>";
                                                            }
                                                            else{
                                                                echo "<td><input type='hidden' class='Room_Num' name='Room_Num[]' value='".$rows['Room_Num']."'";if($row['Overall_Status']=='8') echo " disabled"; echo "><label><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' value='".$rows['Room_Num']."' onClick='check_in()'>".$rows['Room_Num']."</label></td>";
                                                            }
                                                        }
                                                        else if($rows['Room_Status'] == "8"){
                                                            echo "<td><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' disabled>".$rows['Room_Num']."</label></td>";
                                                        }
                                                        else{
                                                            echo "<td><input type='hidden' class='Room_Num' name='Room_Num[]' value='".$rows['Room_Num']."'";if($row['Overall_Status']=='8') echo " disabled"; echo "><label><input type='checkbox' style='width:20px;height:20px' checked disabled>".$rows['Room_Num']."</label></td>";
                                                        }
                                                        echo "
                                                            <td>".str_replace('-','/',$rows['Actual_CIN_Datetime'])."</td>
                                                            <td>".$rows['Price']."</td>
                                                            <td>".$Room_Status[$rows['Room_Status']]."</td>
                                                            <td>
                                                                <span class='display_text'>".$rows['Keycard_Counter']."</span>
                                                                <input type='number' class='info_edit_column Keycard_Counter' style='font-size: 26px; border-radius: 7.5px;' min='1' max='".$rows['Tenant']."' value='".$rows['Keycard_Counter']."'>
                                                            </td>
                                                            <td>".str_replace(chr(13).chr(10), "<br />",nl2br($rows['Detail_Remark']))."</td>
                                                        </tr>";
                                                    }
                                                    echo "
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr name='extra_expense_tr'>
                                                <td colspan='2'>其它消費：</td>
                                            </tr>
                                            <tr name='extra_expense_tr'>
                                                <td colspan='2'>
                                                    <table id='extra_expense_table' border='1'>
                                                        <tr class='title'>
                                                            <th style='width:20%'>名稱</th>
                                                            <th style='width:10%'>單價</th>
                                                            <th style='width:10%'>數量</th>
                                                            <th style='width:10%'>總額</th>
                                                            <th style='width:40%'>備註</th>
                                                            <th></th>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan='2' height='30px'></td>
                                            </tr>
                                            <tr>
                                                <td colspan='2' style='text-align:right'>
                                                    <div id='default_btn'>
                                                        <input type='button' value='確定' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='goback()'>
                                                        <input type='button' value='辦理入住' id='check_in_btn' class='disabled' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' disabled>
                                                        <input type='button' value='取消訂單' id='cancel_booking_btn' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='cancel_booking()'>
                                                    </div>
                                                    <div id='modify_btn'>
                                                        <input type='button' value='完成' id='完成' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='complete_modify()'>
                                                        <input type='button' value='取消' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='cancel_modify()'>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <div id='payment' class='tabcontent'"; if($Action=='payment') echo " style='display: block;'"; echo ">";
                                        echo "<table style='font-size:25px' width='100%'>
                                                <tr>
                                                    <td width='30%' style='font-size:50px'>總額：<span id='Total_Price_Display'></span>";
                                                            $sql = "SELECT SUM(`Amount`) FROM `payment` WHERE `Booking_ID`='".$Booking_ID."'";
                                                            $result = mysqli_query($conn,$sql);
                                                            $rows = $result->fetch_array();
                                                            if(!isset($rows[0]))
                                                                $rows[0]=0;
                                                            $left = $row['Total_Price']-$rows[0];
                                                            echo "
                                                    <td width='40%'>
                                                        <span style='font-size:25px;margin-right:20px'>已收金額：<span id='already_paid'>".$rows[0]."</span></span>
                                                        <span style='font-size:25px'>待收金額：<span id='Unpaid'>".$left."</span></span>
                                                    </td>
                                                    <td width='30%' style='text-align:right'>";
                                                        // <input type='button' style='width:130px;height:50px;font-size:24px;margin-right:40px;border-radius:15px' value='編輯'>
                                                        echo " <input type='button' style='width:180px;height:50px;font-size:20px;background-color:#F79B00;color:WHITE;border-radius:15px' value='新增收款' onClick='show_payment_dialog()'>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td height='10px'></td>
                                                </tr>
                                                <tr>
                                                    <td>使用專案： ";
                                                    $sql = "SELECT `discount`.`Discount_Name` FROM `booking_index`,`discount` WHERE `booking_index`.`Booking_ID`='".$Booking_ID."' AND `booking_index`.`Discount_ID`=`discount`.`Discount_ID`";
                                                    $result = mysqli_query($conn,$sql);
                                                    $row = $result->fetch_array();
                                                    if(!empty($row))
                                                        echo $row['Discount_Name'];
                                                    else
                                                        echo "無";
                                                    echo "</td>
                                                </tr>
                                                <tr>
                                                    <td>收款明細：</td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3'>
                                                        <table id='payment_status_table' border='1' style='font-size:25px;border-collapse:collapse;' width='100%'>
                                                            <tr class='title'>
                                                                <th>類型</th>
                                                                <th>金額</th>
                                                                <th>方式</th>
                                                                <th>收款時間</th>
                                                                <th>備註</th>
                                                            </tr>";
                                                        $sql = "SELECT * FROM `payment` WHERE `Booking_ID`='".$Booking_ID."'";
                                                        $result = mysqli_query($conn,$sql);
                                                        $Payment_Type = array('訂金','現場收款','月結簽帳','帳務調整','退款','其它', '預先授權');
                                                        $Payment_Method_Num = array('現金','信用卡','轉帳','其它');
                                                        while($row = $result->fetch_assoc()){
                                                            echo "
                                                            <tr>
                                                                <td>".$Payment_Type[$row['Payment_Type']]."</td>
                                                                <td>".$row['Amount']."</td>
                                                                <td>".$Payment_Method_Num[$row['Payment_Method_Num']]."</td>
                                                                <td>".$row['Payment_Datetime']."</td>
                                                                <td>".str_replace(chr(13).chr(10), "",nl2br($row['Payment_Remark']))."</td>
                                                            </tr>";
                                                        }
                                                        echo "
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan='2' height='30px'></td>
                                                </tr>
                                                <tr>
                                                    <td colspan='3' style='text-align:right'>
                                                        <input type='button' value='確定' style='width:130px;height:50px;font-size:20px;margin-left:40px;border-radius:15px;background-color:#0091FF;color:white' onClick='goback()'>
                                                    </td>
                                                </tr>
                                            </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </center>
                ";
            ?>
        </div>
        <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
    </div>
    </body>
    
    <div id="payment_dialog">
        <input type="hidden" autofocus="true" />
        <center><form id='myform' action='service.php'>
            <?php
                $sql = "SELECT * FROM `booking_index` WHERE `Booking_ID`='".$Booking_ID."'";
                $result = mysqli_query($conn,$sql);
                $row = $result->fetch_assoc();
            ?>
            <input type='hidden' id='cal_end_price' name='cal_end_price' value='<?php echo $row['Total_Price'];?>'>
            <input type='hidden' name='Feature' value='pay'>
            <input type='hidden' name='Staff_ID' value='<?php echo $_COOKIE['Staff_ID'];?>'>
            <input type='hidden' name='Booking_ID' value='<?php echo $Booking_ID;?>'>
            <input type='hidden' id='Flag' name='Flag' value='0'>
            <table border='0' id='myTable' width='100%' style='padding: 10px 30px'>
                <tr>
                    <td style='font-size:36px'>新增收款：</td> 
                </tr>
                <tr style='display:none;'>
                    <td style='font-size:26px'>
                        $<span id='Original' class='edit_text'><?php echo $left;?></span> - <input type='number' class='edit_column' style="border-radius:7.5px;font-size:26px;width:225;height:45;" id='coupon' value='0' onchange='cal_Total_Price(this.value)' onKeyUp='cal_Total_Price(this.value)'> = $<span id='coupon_cal'><?php echo $left;?></span>
                    </td>
                </tr>
                <!-- <tr>
                    <td style='font-size:26px'>
                        結帳人員：
                        <select name='Staff_ID' id='Staff_ID' class='modified_select edit_column' style='font-size:26px;width:220;height:50;' required>
                            <option value=''>選擇結帳人員</option>
                            <?php
                            $sql = "SELECT * FROM `staff` WHERE `Staff_Level`>0 ORDER BY `Staff_Level` ASC";
                            $result = mysqli_query($conn,$sql);
                            while($row = $result->fetch_assoc()){
                                echo "<option value='".$row['Staff_ID']."'>".$row['Staff_Name']."</option>";
                            }
                            ?>
                        </select>
                    </td>
                </tr> -->
                <tr>
                    <td style='font-size:26px'>
                        收款日期：<input type='text' style='font-size:26px' id='payment_date' class='edit_column' name='payment_date' placeholder='YYYY/MM/DD' required>
                    </td>
                </tr>
                <tr>
                    <td style='font-size:26px'>收款類型：
                        <select name='Payment_Type' id='Payment_Type' class='modified_select edit_column' style='font-size:26px;width:220;height:50;' onchange='selection_cahange(this.value)' required>
                            <option value=''>選擇收款類型</option>
                            <option value='0'>訂金</option>
                            <option value='1'>現場收款</option>
                            <option value='2'>月結簽帳</option>
                            <option value='3'>帳務調整</option>
                            <option value='4'>退款</option>
                            <option value='5'>其它</option>
                        </select>
                        <input type='text' name='Payment_Type_Name' id='Payment_Method_Name' class='edit_column' style='display: none'>
                    </td>
                </tr>
                <tr>
                    <td colspan='3'><center>
                        <div id='payment_area'>
                            <div>
                                <table border='1'>
                                    <tr>
                                        <td style='font-size:26px;border:0;'>
                                            收款方式：
                                        </td>
                                        <td style='font-size:26px;border:0;border-right: 1px solid;'>
                                            <select class='modified_select edit_column' name='Payment_Method[]' style='font-size:26px;width:220;height:50;'>
                                                <option value='0'>現金</option>
                                                <option value='1'>信用卡</option>
                                                <option value='2'>轉帳</option>
                                                <option value='3'>其它</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='font-size:26px;border:0;'>收款金額：</td>
                                        <td style='font-size:26px;border:0;border-right: 1px solid;'><input type='number' class='payments edit_column' name='Payment[]' style='border-radius:7.5px;font-size:26px;width:220;height:50;' value='0' onKeyUp='check_sign(this)'>&nbsp;&nbsp;&nbsp;<label style='font-size:18px'><input type='checkbox' name='check_all[]' onClick='toogle(this)'>同代收金額</label></td>
                                    </tr>
                                    <tr>
                                        <td style='text-align:right;font-size:26px;border:0;vertical-align:top'>備註：</td>
                                        <td colspan='2' style='font-size:26px;border:0;'><textarea name='Remark[]' class='edit_column' style='border-radius:7.5px;font-size:26px '></textarea></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </center></td>
                </tr>
                <tr><td colspan='3'><center><input type='button' id='add_payment' style='border-radius:15px;width:100%;height:80px;font-size:30px' value="+　增加" onclick='show_another()'></center></td></tr>
                <tr><td colspan='3'><center><input type='submit' name='Finish' id='Finish' style='border-radius:15px;width:560px;height:80px;font-size:32px;background-color:#0091FF;color:WHITE' value='完成'></center></td></tr>
            </table>
            </form>
        </center>
    </div>
    
    <div id="expense_dialog">
        <input type="hidden" autofocus="true" />
        <center><form id='my_expense_form' action='service.php'>
            <input type='hidden' name='Feature' value='add_expense'>
            <input type='hidden' name='Booking_ID' id='expense_form_Booking_ID' value=''>
            <table style='margin-top:50px;'>
                <tr>
                    <td style='font-size:36px'>新增消費：</td>
                </tr>
                <tr>
                    <td style='font-size:26px'>
                        名稱：
                        <select name='Extra_Expense_ID' class='modified_select' style='width:323px;' id='Extra_Expense_ID' onChange='Expense_ID_onchange()' required>
                            <option class='0' value=''>選擇品項</option>
                            <?php
                                $sql = "SELECT * FROM `extra_expense` WHERE `Extra_Expense_Enable`=1";
                                $result = mysqli_query($conn,$sql);
                                while($row = $result->fetch_assoc())
                                    echo "<option class='".$row['Extra_Expense_Price']."' value='".$row['Extra_Expense_ID']."'>".$row['Extra_Expense_Name']."</option>";
                            ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style='font-size:26px'>
                        <div style='display:inline-block;float:left'>單價：<span id='Extra_Expense_Price'>0</span></div>
                        <input type='hidden' name='Expense_Total_Price' id='Expense_Total_Price'>
                        <div style='display:inline-block;float:right'>總價：<span id='Extra_Expense_TotalPrice'>0</span></div>
                    </td>
                </tr>
                <tr>
                    <td style='font-size:26px'>
                        數量：
                        <input type='number' id='Expense_Count' name='Expense_Count' value='1' min='1' onChange='Count_onchange()' required>
                    </td>
                </tr>
                <tr>
                    <td style='font-size:26px'>備註：<input type='text' name='Expense_Remark' maxlength='255'></td>
                </tr>
                <tr>
                    <td><button type='submit' class='function_btn' style='width:435px;height:80px;font-size:32px;background-color:#0091FF'>完成</button></td>
                </tr>
            </table>
            </form>
        </center>
    </div>
</html>

<script>
    function Expense_ID_onchange(){
        var class_value = $("#Extra_Expense_ID :selected").attr('class');
        $("#Extra_Expense_Price").html(class_value);
        $("#Expense_Total_Price").val(class_value*$("#Expense_Count").val());
        $("#Extra_Expense_TotalPrice").html(class_value*$("#Expense_Count").val());
    }

    function Count_onchange(){
        var class_value = $("#Extra_Expense_ID :selected").attr('class');
        $("#Expense_Total_Price").val(class_value*$("#Expense_Count").val());
        $("#Extra_Expense_TotalPrice").html(class_value*$("#Expense_Count").val());
    }

    $(document).ready(function() {
        
        $("#payment_dialog").dialog({
            height: 862,
            width: 970,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
        
        $("#expense_dialog").dialog({
            height: 565,
            width: 702,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#check_in_btn").click(function(){
            var room_num_array = new Array();
            for(i=0;i<document.getElementsByClassName("Check_In_R_N").length;i++){
                if(document.getElementsByClassName("Check_In_R_N")[i].checked){
                    room_num_array.push(document.getElementsByClassName("Check_In_R_N")[i].value);
                }
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "check_in",
                    Booking_ID: $("#Booking_ID").html(),
                    CIN_Date: $("#CIN_Date_text").html(),
                    room_num_array: room_num_array
                },
                success: function(data) {
                    var Room_Status = ['未入住','入住中','已退房(待打掃)','已退房(打掃中)','休息中','已退房(打掃完成)','清掃中','保留中','已取消'];
                    $("#room_status_table tr:not(.title)").remove();
                    json_array = data;
                    for(i=0;i<json_array['Rooms'].length;i++){
                        if(json_array['Rooms'][i].Actual_CIN_Datetime == "0000/00/00 00:00:00" && json_array['Rooms'][i].Room_Status!='8')
                            var row = "<tr><td><label><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' value='" + json_array['Rooms'][i].Room_Num + "' onClick='check_in()'>" + json_array['Rooms'][i].Room_Num + "<input type='hidden' class='Room_Num' name='Room_Num[]' value='" + json_array['Rooms'][i].Room_Num + "'></label></td><td>" + json_array['Rooms'][i].Actual_CIN_Datetime + "</td><td>" + json_array['Rooms'][i].Price + "</td><td>" + Room_Status[json_array['Rooms'][i].Room_Status] + "</td><td>" + json_array['Rooms'][i].Keycard_Counter + "</td><td>" + json_array['Rooms'][i].Detail_Remark + "</td></tr>";
                        else if(json_array['Rooms'][i].Room_Status=='8')
                            var row = "<tr><td><label><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' disabled>" + json_array['Rooms'][i].Room_Num + "<input type='hidden' class='Room_Num' name='Room_Num[]' value='" + json_array['Rooms'][i].Room_Num + "'></label></td><td>" + json_array['Rooms'][i].Actual_CIN_Datetime + "</td><td>" + json_array['Rooms'][i].Price + "</td><td>" + Room_Status[json_array['Rooms'][i].Room_Status] + "</td><td>" + json_array['Rooms'][i].Keycard_Counter + "</td><td>" + json_array['Rooms'][i].Detail_Remark + "</td></tr>";
                        else
                            var row = "<tr><td><label><input type='checkbox' style='width:20px;height:20px' checked disabled>" + json_array['Rooms'][i].Room_Num + "<input type='hidden' class='Room_Num' name='Room_Num[]' value='" + json_array['Rooms'][i].Room_Num + "'></label></td><td>" + json_array['Rooms'][i].Actual_CIN_Datetime + "</td><td>" + json_array['Rooms'][i].Price + "</td><td>" + Room_Status[json_array['Rooms'][i].Room_Status] + "</td><td>" + json_array['Rooms'][i].Keycard_Counter + "</td><td>" + json_array['Rooms'][i].Detail_Remark + "</td></tr>";
                        $("#room_status_table").append(row);
                    }
                    if(data['message']!=''){
                        alert(data['message']);
                    }
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        });

        $("#add_expense").click(function(){
            $("#expense_form_Booking_ID").val($("#Booking_ID").html());
            $( "#expense_dialog" ).dialog( "open" );
        });

        $("#myform").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#myform").serialize(),
                success: function(data) {
                    var Payment_Type = ['訂金','現場收款','月結簽帳','帳務調整','退款','其它','預先授權'];
                    var Payment_Method_Num = ['現金','信用卡','轉帳','其它'];
                    var total = 0;
                    $("#payment_status_table tr:not(.title)").remove();
                    json_array = data;
                    var total=0;
                    for(i=0;i<json_array.length;i++){
                        var row = "<tr><td>"+Payment_Type[json_array[i].Payment_Type]+"</td><td>"+json_array[i].Amount+"</td><td>"+Payment_Method_Num[json_array[i].Payment_Method_Num]+"</td><td>"+json_array[i].Payment_Datetime+"</td><td>"+json_array[i].Payment_Remark.replace(/\r?\n|\r/g, "<br>")+"</td></tr>";
                        total+=parseInt(json_array[i].Amount,10);
                        $("#payment_status_table").append(row);
                    }
                    $("#already_paid").html(total);
                    $("#Unpaid").html($("#Total_Price_Display").html()-$("#already_paid").html());
                    close_dialog();
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        })

        $("#my_expense_form").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#my_expense_form").serialize(),
                success: function(data) {
                    display_expense();
                    close_dialog();
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        })

    });

    function display_expense(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "display_expense",
                Booking_ID: $("#Booking_ID").html()
            },
            success: function(data) {
                var total=0;
                console.log(data);
                if(data.length>0){
                    $("#extra_expense_table tr:not(.title)").remove();
                    json_array = data;
                    console.log(json_array);
                    for(i=0;i<json_array.length;i++){
                        // console.log("total before: " + total);
                        var row = "<tr><td>"+json_array[i].Extra_Expense_Name+"</td><td style='text-align:right'>"+json_array[i].Extra_Expense_Price+"</td><td style='text-align:right'>"+json_array[i].Expense_Count+"</td><td style='text-align:right'>"+json_array[i].Expense_Total_Price+"</td><td>"+json_array[i].Expense_Remark+"</td><td style='text-align:center'><button name='delete_expense' class='function_btn' style='width:90px;height:40;background-color:#E02020' value='"+json_array[i].EO_ID+"' onClick='delete_expense(this.value)'>刪除</button></td></tr>";
                        total+=parseInt(json_array[i].Extra_Expense_Price,10);
                        $("#extra_expense_table").append(row);
                        // console.log("total after: " + total);
                    }
                    // console.log("total end: " + total);
                    // $("#Total_Price_V").html(parseInt($("#Total_Price").val(),10)+total);
                    $("#Total_Price_Display").html($("#Total_Price_V").html());
                    $("#Unpaid").html($("#Total_Price_Display").html()-$("#already_paid").html());
                    var x = document.getElementsByName("extra_expense_tr");
                    for(i=0;i<x.length;i++){
                        x[i].classList.remove("extra_expense_tr");
                    }
                }
                else{
                    var x = document.getElementsByName("extra_expense_tr");
                    for(i=0;i<x.length;i++){
                        x[i].classList.add("extra_expense_tr");
                    }
                    $("#Total_Price_V").html($("#Total_Price").val());
                    $("#Total_Price_Display").html($("#Total_Price_V").html());
                    $("#Unpaid").html($("#Total_Price_Display").html()-$("#already_paid").html());
                }
            },
            error: function(jqXHR) {
                console.log("display_expense error: " + jqXHR.responseText);
            }
        })
    }

    function delete_expense(target_value){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "delete_expense",
                EO_ID: target_value
            },
            success: function(data) {
                display_expense();
            },
            error: function(jqXHR) {
                console.log("display_expense error: " + jqXHR.responseText);
            }
        })
    }
    
    function show_payment_dialog(){
        if(document.getElementById("Overall_Status").value=='8'){
            console.log("AAAAAAAAAAAAAAAA");
            for(i=0;i<document.getElementsByClassName("payments").length;i++){
                document.getElementsByClassName("payments")[i].max='0';
                document.getElementsByClassName("payments")[i].min=document.getElementById("already_paid").innerHTML*(-1);
            }
        }
        else{
            for(i=0;i<document.getElementsByClassName("payments").length;i++){
                document.getElementsByClassName("payments")[i].max='';
                document.getElementsByClassName("payments")[i].min='0';
            }
        }
        $( "#payment_dialog" ).dialog( "open" );
    }

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        for(i=0;i<document.getElementsByClassName("edit_column").length;i++){
            if(document.getElementsByClassName("edit_column")[i].type=='select-one')
                document.getElementsByClassName("edit_column")[i].selectedIndex = "0";
            else if(document.getElementsByClassName("edit_column")[i].type=='number')
                document.getElementsByClassName("edit_column")[i].value = "0";
            else
                document.getElementsByClassName("edit_column")[i].value = "";
        }
        // document.getElementById("coupon_cal").innerHTML = document.getElementById("Original").innerHTML;
        document.getElementById("add_payment").style.display = "block";
        document.getElementById("payment_area").innerHTML = "<div><table border='1'><tr><td style='font-size:26px;border:0;'>收款方式：</td><td style='font-size:26px;border:0;border-right: 1px solid;'><select class='modified_select edit_column' name='Payment_Method[]' style='font-size:26px;width:220;height:50;'><option value='0'>現金</option><option value='1'>信用卡</option><option value='2'>轉帳</option><option value='3'>其它</option></select></td></tr><tr><td style='font-size:26px;border:0;'>收款金額：</td><td style='font-size:26px;border:0;border-right: 1px solid;'><input type='number' class='payments edit_column' name='Payment[]' style='border-radius:7.5px;font-size:26px;width:220;height:50;' value='0' onKeyUp='check_sign(this)'>&nbsp;&nbsp;&nbsp;<label style='font-size:18px'><input type='checkbox' name='check_all[]' onClick='toogle(this)'>同代收金額</label></td></tr><tr><td style='text-align:right;font-size:26px;border:0;vertical-align:top'>備註：</td><td colspan='2' style='font-size:26px;border:0;'><textarea name='Remark[]' class='edit_column' style='border-radius:7.5px;font-size:26px '></textarea></td></tr></table></div>";
        $('#payment_dialog').dialog( "close" );
        $('#expense_dialog').dialog( "close" );
    }

    var picker = new Lightpick({
        field: document.getElementById('payment_date'),
        singleDate: true,
        repick: true,
        onSelect: function(date){

        }
    });

    var picker = new Lightpick({
        field: document.getElementById('CIN_Date'),
        singleDate: true,
        repick: true,
        minDate: moment().startOf('day'),
        onSelect: function(date){
            if(document.getElementById('CIN_Date').value>document.getElementById('COUT_Date').value){
                var temp = document.getElementById('CIN_Date').value;
                document.getElementById('CIN_Date').value = document.getElementById('COUT_Date').value;
                document.getElementById('COUT_Date').value = temp;
            }
            var date1 = new Date(document.getElementById('CIN_Date').value);
            var date2 = new Date(document.getElementById('COUT_Date').value);
            var Difference_In_Days = (date2-date1) / (1000 * 3600 * 24);
            document.getElementById('Day_Count').value = Difference_In_Days;
            document.getElementById('Day_Count_Text').innerHTML = Difference_In_Days;

            var Room_Num = new Array();
            for(i=0;i<document.getElementsByClassName("Room_Num").length;i++){
                Room_Num.push(document.getElementsByClassName("Room_Num")[i].value);
            }

            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "check_clash",
                    CIN_Date: $("#CIN_Date").val(),
                    COUT_Date: $("#COUT_Date").val(),
                    Day_Count: $("#Day_Count").val(),
                    Booking_ID: $("#Booking_ID").html(),
                    Room_Num: Room_Num,
                },
                success: function(data) {
                    console.log(data);
                    if(data.Success == false){
                        $("#result").html("<center><font color='#ff0000'>此時段與其它訂單衝突！請選擇其它時間或新增訂單！</font></center>");
                        $("#完成").attr('disabled',true);

                    }
                    else{
                        $("#result").html("");
                        $("#完成").attr('disabled',false);
                        var Total_Price = data.Total_Price * $("#Room_Count").html();
                        $("#Total_Price").val(Total_Price);
                        $("#Total_Price_V").html(Total_Price);
                    }
                    
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        }
    });

    var picker = new Lightpick({
        field: document.getElementById('COUT_Date'),
        singleDate: true,
        repick: true,
        minDate: moment().startOf('day').add(1, 'day'),
        onSelect: function(date){
            if(document.getElementById('CIN_Date').value>document.getElementById('COUT_Date').value){
                var temp = document.getElementById('CIN_Date').value;
                document.getElementById('CIN_Date').value = document.getElementById('COUT_Date').value;
                document.getElementById('COUT_Date').value = temp;
            }
            var date1 = new Date(document.getElementById('CIN_Date').value);
            var date2 = new Date(document.getElementById('COUT_Date').value);
            var Difference_In_Days = (date2-date1) / (1000 * 3600 * 24);
            document.getElementById('Day_Count').value = Difference_In_Days;
            document.getElementById('Day_Count_Text').innerHTML = Difference_In_Days;

            var Room_Num = new Array();
            for(i=0;i<document.getElementsByClassName("Room_Num").length;i++){
                Room_Num.push(document.getElementsByClassName("Room_Num")[i].value);
            }

            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "check_clash",
                    CIN_Date: $("#CIN_Date").val(),
                    COUT_Date: $("#COUT_Date").val(),
                    Day_Count: $("#Day_Count").val(),
                    Booking_ID: $("#Booking_ID").html(),
                    Room_Num: Room_Num,
                },
                success: function(data) {
                    console.log(data);
                    if(data.Success == false){
                        $("#result").html("<center><font color='#ff0000'>此時段與其它訂單衝突！請選擇其它時間或新增訂單！</font></center>");
                        $("#完成").attr('disabled',true);

                    }
                    else{
                        $("#result").html("");
                        $("#完成").attr('disabled',false);
                        var Total_Price = data.Total_Price*$("#Room_Count").html('');
                        $("#Total_Price").val(Total_Price);
                        $("#Total_Price_V").html('');
                    }
                    
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        }
    });

    function show_another(){
        document.getElementById("Flag").value = "1";
        var new_payment = "<div><br><br><table border='1'><tr><td style='font-size:26px;border:0;'>收款方式：</td><td style='font-size:26px;border:0;border-right: 1px solid;'><select class='modified_select edit_column' name='Payment_Method[]' style='font-size:26px;width:220;height:50;'><option value='0'>現金</option><option value='1'>信用卡</option><option value='2'>轉帳</option><option value='3'>其它</option></select></td></tr><tr><td style='font-size:26px;border:0;'>收款金額：</td><td style='font-size:26px;border:0;border-right: 1px solid;'><input type='number' class='payments edit_column' name='Payment[]' style='border-radius:7.5px;font-size:26px;width:220;height:50;' min='0' value='0' onKeyUp='check_sign(this)'>&nbsp;&nbsp;&nbsp;<label style='font-size:18px'><input type='checkbox' name='check_all[]' onClick='toogle(this)'>同代收金額</label></td></tr><tr><td style='text-align:right;font-size:26px;border:0;vertical-align:top'>備註：</td><td colspan='2' style='font-size:26px;border:0;'><textarea name='Remark[]' class='edit_column' style='border-radius:7.5px;font-size:26px '></textarea></td></tr></table></div>";
        document.getElementById("payment_area").innerHTML += new_payment;
    }

    function toogle(source){
        if(source.checked){
            for(i=0;i<document.getElementsByClassName("payments").length;i++)
                document.getElementsByClassName("payments")[i].value=document.getElementById("coupon_cal").innerHTML;
            console.log(source);
        }
        else{
            for(i=0;i<document.getElementsByClassName("payments").length;i++)
                document.getElementsByClassName("payments")[i].value="0";
        }
        if(document.getElementById("Overall_Status").value=='8')
            document.getElementsByClassName("payments")[i].value *= (-1);
    }

    function cal_Total_Price(coupon){
        if(coupon === '')
            document.getElementById("coupon").value = 0;
        var x = document.getElementById("Original").innerHTML - coupon;
        document.getElementById("cal_end_price").value = x;
        document.getElementById("coupon_cal").innerHTML = x;
    }

    function check_in(){
        for(i=0;i<document.getElementsByClassName("Check_In_R_N").length;i++){
            if(document.getElementsByClassName("Check_In_R_N")[i].checked){
                document.getElementById("check_in_btn").disabled = false;
                break;
            }
            document.getElementById("check_in_btn").disabled = true;
        }
    }

    function openTab(evt, ActionName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(ActionName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    function cancel_booking(){
        if(confirm("確定要取消嗎？")){
            var room_num_array = [];
            for(i=0;i<document.getElementsByClassName("Check_In_R_N").length;i++){
                if(document.getElementsByClassName("Check_In_R_N")[i].checked){
                    room_num_array.push(document.getElementsByClassName("Check_In_R_N")[i].value);
                }
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "cancel_booking",
                    Booking_ID: $("#Booking_ID").html(),
                    Room_Num_Array: room_num_array
                },
                success: function(data) {
                    var Room_Status = ['未入住','入住中','已退房(待打掃)','已退房(打掃中)','休息中','已退房(打掃完成)','清掃中','保留中','已取消'];
                    $("#room_status_table tr:not(.title)").remove();
                    json_array = data;
                    for(i=0;i<json_array['Rooms'].length;i++){
                        if(json_array['Rooms'][i].Actual_CIN_Datetime == "0000/00/00 00:00:00" && json_array['Rooms'][i].Room_Status!='8')
                            var row = "<tr><td><label><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' value='" + json_array['Rooms'][i].Room_Num + "' onClick='check_in()'>" + json_array['Rooms'][i].Room_Num + "<input type='hidden' class='Room_Num' name='Room_Num[]' value='" + json_array['Rooms'][i].Room_Num + "'></label></td><td>" + json_array['Rooms'][i].Actual_CIN_Datetime + "</td><td>" + json_array['Rooms'][i].Price + "</td><td>" + Room_Status[json_array['Rooms'][i].Room_Status] + "</td><td>" + json_array['Rooms'][i].Keycard_Counter + "</td><td>" + json_array['Rooms'][i].Detail_Remark + "</td></tr>";
                        else if(json_array['Rooms'][i].Room_Status=='8')
                            var row = "<tr><td><label><input type='checkbox' style='width:20px;height:20px' class='Check_In_R_N' disabled>" + json_array['Rooms'][i].Room_Num + "<input type='hidden' class='Room_Num' name='Room_Num[]' value='" + json_array['Rooms'][i].Room_Num + "'></label></td><td>" + json_array['Rooms'][i].Actual_CIN_Datetime + "</td><td>" + json_array['Rooms'][i].Price + "</td><td>" + Room_Status[json_array['Rooms'][i].Room_Status] + "</td><td>" + json_array['Rooms'][i].Keycard_Counter + "</td><td>" + json_array['Rooms'][i].Detail_Remark + "</td></tr>";
                        else
                            var row = "<tr><td><label><input type='checkbox' style='width:20px;height:20px' checked disabled>" + json_array['Rooms'][i].Room_Num + "<input type='hidden' class='Room_Num' name='Room_Num[]' value='" + json_array['Rooms'][i].Room_Num + "'></label></td><td>" + json_array['Rooms'][i].Actual_CIN_Datetime + "</td><td>" + json_array['Rooms'][i].Price + "</td><td>" + Room_Status[json_array['Rooms'][i].Room_Status] + "</td><td>" + json_array['Rooms'][i].Keycard_Counter + "</td><td>" + json_array['Rooms'][i].Detail_Remark + "</td></tr>";
                        $("#room_status_table").append(row);
                    }
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        }
    }

    function modify_infos(){
        document.getElementById("default_btn").style.display = "none";
        document.getElementById("modify_btn").style.display = "block";
        for(i=0;i<document.getElementsByClassName("display_text").length;i++){
            document.getElementsByClassName("display_text")[i].style.display = "none";
        }
        for(i=0;i<document.getElementsByClassName("info_edit_column").length;i++){
            document.getElementsByClassName("info_edit_column")[i].style.display = "inline";
        }
        if($("#Customer_Passport").val()=="" && $("#Customer_SSID").val()==""){
            for(i=0;i<document.getElementsByClassName("Customer_Info_Text").length;i++)
                document.getElementsByClassName("Customer_Info_Text")[i].style.display = "none";
            for(i=0;i<document.getElementsByClassName("Customer_Info_Input").length;i++)
                document.getElementsByClassName("Customer_Info_Input")[i].style.display = "inline";
        }
        else{
            for(i=0;i<document.getElementsByClassName("Customer_Info_Text").length;i++)
                document.getElementsByClassName("Customer_Info_Text")[i].style.display = "inline";
            for(i=0;i<document.getElementsByClassName("Customer_Info_Input").length;i++)
                document.getElementsByClassName("Customer_Info_Input")[i].style.display = "none";
        }
    }

    function complete_modify(){
        var Keycard_Counter = new Array();
        for(i=0;i<document.getElementsByClassName("Keycard_Counter").length;i++){
            Keycard_Counter.push(document.getElementsByClassName("Keycard_Counter")[i].value);
        }
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "modify_data",
                CIN_Date: $("#CIN_Date").val(),
                COUT_Date: $("#COUT_Date").val(),
                Day_Count: $("#Day_Count").val(),
                People_Count: $("#People_Count").val(),
                Total_Price: $("#Total_Price").val(),
                Index_Remark: $("#Index_Remark").val(),
                Booking_ID: $("#Booking_ID").html(),
                Keycard_Counter: Keycard_Counter,
                Customer_ID: $("#Customer_ID").val(),
                Customer_Name: $("#Customer_Name").val(),
                Customer_Sex: $("#Customer_Sex").val(),
                Customer_Nationality: $("#Customer_Nationality").val(),
                Customer_SSID: $("#Customer_SSID").val(),
                Customer_Passport: $("#Customer_Passport").val(),
                Customer_Phone: $("#Customer_Phone").val(),
                Customer_Email: $("#Customer_Email").val(),

            },
            success: function(data) {
                document.getElementById("default_btn").style.display = "block";
                document.getElementById("modify_btn").style.display = "none";
                for(i=0;i<document.getElementsByClassName("display_text").length;i++){
                    document.getElementsByClassName("display_text")[i].innerHTML = document.getElementsByClassName("info_edit_column")[i].value;
                    if(document.getElementsByClassName("info_edit_column")[i].name=='Index_Remark'){
                        document.getElementsByClassName("display_text")[i].innerHTML = document.getElementsByClassName("info_edit_column")[i].value.replace(/\r?\n|\r/g, "<br>");
                        document.getElementById("Original_Index_Remark").innerHTML = document.getElementsByClassName("info_edit_column")[i].value;
                    }
                    document.getElementsByClassName("display_text")[i].style.display = "inline";
                }
                for(i=0;i<document.getElementsByClassName("info_edit_column").length;i++){
                    document.getElementsByClassName("info_edit_column")[i].style.display = "none";
                }
                for(i=0;i<document.getElementsByClassName("Customer_Info_Input").length;i++){
                    document.getElementsByClassName("Customer_Info_Text")[i].style.display = "inline";
                    document.getElementsByClassName("Customer_Info_Input")[i].style.display = "none";
                    document.getElementsByClassName("Customer_Info_Text")[i].innerHTML = document.getElementsByClassName("Customer_Info_Input")[i].value;
                }
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        })
    }

    function cancel_modify(){
        var sex = ['女','男','其他'];
        document.getElementById("modify_btn").style.display = "none";
        document.getElementById("default_btn").style.display = "block";
        for(i=0;i<document.getElementsByClassName("info_edit_column").length;i++){
            document.getElementsByClassName("info_edit_column")[i].style.display = "none";

            if(document.getElementsByClassName("info_edit_column")[i].name == 'Index_Remark')
                document.getElementsByClassName("info_edit_column")[i].value = document.getElementById("Original_Index_Remark").value;
            else
                document.getElementsByClassName("info_edit_column")[i].value = document.getElementsByClassName("display_text")[i].innerHTML;
            // console.log("name: " + document.getElementsByClassName("info_edit_column")[i].name + " value: " + document.getElementsByClassName("info_edit_column")[i].value);
        }
        for(i=0;i<document.getElementsByClassName("display_text").length;i++){
            document.getElementsByClassName("display_text")[i].style.display = "inline";
        }
        for(i=0;i<document.getElementsByClassName("Customer_Info_Input").length;i++){
            document.getElementsByClassName("Customer_Info_Text")[i].style.display = "inline";
            document.getElementsByClassName("Customer_Info_Input")[i].style.display = "none";
            document.getElementsByClassName("Customer_Info_Input")[i].value = document.getElementsByClassName("Customer_Info_Text")[i].innerHTML;
            if(document.getElementsByClassName("Customer_Info_Input")[i].type == "select-one"){
                for(x=0;x<document.getElementsByClassName("Customer_Info_Input")[i].getElementsByTagName('option').length;x++){
                    document.getElementsByClassName("Customer_Info_Input")[i].getElementsByTagName('option')[x].selected = "";
                    if(x == sex.indexOf(document.getElementsByClassName("Customer_Info_Text")[i].innerHTML))
                        document.getElementsByClassName("Customer_Info_Input")[i].getElementsByTagName('option')[x].selected = "selected";
                }

            }
        }
        // for(i=0;i<document.getElementsByClassName("Customer_Info_Text").length;i++){
        //     document.getElementsByClassName("Customer_Info_Text")[i].style.display = "inline";
        // }
    }

    function goback(){
        window.history.back();
    }

    function check_sign(target){
        if(document.getElementById("Overall_Status").value=='8'){
            if(target.value>0)
                target.value=target.value*(-1);
            if((target.value*(-1))>document.getElementById("already_paid").innerHTML)
                target.value = document.getElementById("already_paid").innerHTML*(-1);
        }
        else{
            if(target.value<0)
                target.value=target.value*(-1);
        }
    }
</script>