<?php
    header('Content-Type: text/html; charset=UTF-8');
?>
<head>
    <script type="text/javascript" src="../functions.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="../lightpick.js"></script>
    <link rel="stylesheet" type="text/css" href="../lightpick.css">

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    <style>
        .info_td{
            padding-left: 35px;
            font-size: 22px;
        }
        .function_td{
            font-size: 22px;
            text-align: center;
        }
        .separator {
            display: flex;
            align-items: center;
            text-align: left;
            font-size: 35px;
            width: 90%;
        }
        .separator::after {
            content: '';
            flex: 1;
            border-bottom: 1px solid #000;
            margin-left: 45px;
        }
        .info_button_table{
            width: 90%;
            height: 225px;
            background-color: WHITE;
            border: 2.5px solid #979797;
            border-radius:15px;
            margin: 25px;
        }
        .info_button_table:hover{
            /* border: 1px solid #DADADA; */
            background-color: #DADADA;
        }
        .blue_dot {
            height: 35px;
            width: 35px;
            background-color: #32C5FF;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 2.5px; */
            margin: 0px 20px;
        }
        .red_dot {
            height: 35px;
            width: 35px;
            background-color: #F94D4D;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 5px; */
            margin: 0px 20px;
        }
        .grey_dot {
            height: 30px;
            width: 35px;
            background-color: #C9C9C9;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            padding-bottom: 5px;
            margin: 0px 20px;
        }
        .yellow_dot {
            height: 35px;
            width: 35px;
            background-color: #F7C700;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 5px; */
            margin: 0px 20px;
        }
        .purple_dot {
            height: 35px;
            width: 35px;
            background-color: #c632e6;
            color:WHITE;
            border-radius: 50%;
            display: inline-block;
            text-align: center;
            /* padding-bottom: 5px; */
            margin: 0px 20px;
        }
        .info_table{
            width: 100%;
        }
        .info_table tr,td{
            vertical-align: center;
            padding: 5px 0px;
            font-size: 22px;
        }
    </style>
</head>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div>
    <div class='right'>
        <center>
            <p id='result'></p>
            <div style='width: 100%'>
                <input type='text' class='Input_Search_Field' id='Fuzzy_Search' placeholder='關鍵字搜尋'>
                <select style='width:270px;height:50px;font-size:20px' class='modified_select' id='Sorting'>
                    <option value=''>日期類型</option>
                    <option value='BDATE'>訂房日</option>
                    <option value='CIN'>入住日期</option>
                    <option value='COUT'>退房日期</option>
                </select>
                <div style='display:inline-block'>
                    <input type='text' id='Start' value='' style='width:270px;height:50px;font-size:20px;display:inline-block' placeholder='點擊以選擇起始日'>
                    <div style='display:inline-block'> ～ </div>
                    <input type='text' id='End' value='' style='width:270px;height:50px;font-size:20px;display:inline-block' placeholder='點擊以選擇結束日'>
                </div>
                <button class='function_btn' id='Reset' style='width:110px;height:50px;font-size:20px;background-color:#0091FF'>重設</button>

                <button class='function_btn' id='Today' style='width:160px;height:50px;font-size:20px;background-color:#0091FF'>今日</button>
                <button class='function_btn' id='New_Order' style='width:160px;height:50px;font-size:20px;background-color:#F79B00'>新增訂單</button>
            </div>
            <br>
            <div id='container'>
            </div>
        </center>
    </div>
    <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
</body>

<script>
    var picker = new Lightpick({
        field: document.getElementById('Start'),
        // startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('End').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Phone = json_array[i].Customer_Phone;
                        var Customer_Email = json_array[i].Customer_Email;
                        var CIN_Date = json_array[i].CIN_Date;
                        var COUT_Date = json_array[i].COUT_Date;

                        var Room_Count = parseInt(json_array[i].Room_Count);

                        var Yet = parseInt(json_array[i].Yet);
                        var Checked_In = parseInt(json_array[i].Checked_In);
                        var Checked_Out = parseInt(json_array[i].Checked_Out);
                        var Holding = parseInt(json_array[i].Holding);
                        var Cancel = parseInt(json_array[i].Cancel);

                        if(i==0){
                            switch(document.getElementById("Sorting").value){
                                case 'BDATE':
                                    pre_date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    pre_date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                case '':
                                    pre_date = json_array[i].CIN_Date;
                                break;
                            }
                        }
                        else{
                            switch($("#Sorting").val()){
                                case 'BDATE':
                                    date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                default:
                                    date = json_array[i].CIN_Date;
                            }
                        }
                        if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                            if(date != '')
                                pre_date = date;
                            row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                        }
                        else
                            row = "";
                        var room_status = '';

                        if(Room_Count == Cancel){
                            room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                        }
                        else if(Holding>0){
                            if(Holding>1)
                                room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                            else
                                room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                        }
                        else{
                            if(Yet>0){
                                if(Yet>1)
                                    room_status = '<span class=\'red_dot\'>'+Yet+'</span>';
                                else
                                    room_status = '<span class=\'red_dot\' style="color:#F94D4D">'+Yet+'</span>';
                            }
                            else
                                room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_In>0){
                                if(Checked_In>1)
                                    room_status += '<span class=\'blue_dot\'>'+Checked_In+'</span>';
                                else
                                    room_status += '<span class=\'blue_dot\' style="color:#32C5FF">'+Checked_In+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_Out>0){
                                if(Checked_Out>1)
                                    room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                                else
                                    room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                        }
                        row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    var picker2 = new Lightpick({
        field: document.getElementById('End'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('Start').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                console.log("Start: " + document.getElementById('Start').value + "\nEnd: " + document.getElementById('End').value);
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Phone = json_array[i].Customer_Phone;
                        var Customer_Email = json_array[i].Customer_Email;
                        var CIN_Date = json_array[i].CIN_Date;
                        var COUT_Date = json_array[i].COUT_Date;

                        var Room_Count = parseInt(json_array[i].Room_Count);

                        var Yet = parseInt(json_array[i].Yet);
                        var Checked_In = parseInt(json_array[i].Checked_In);
                        var Checked_Out = parseInt(json_array[i].Checked_Out);
                        var Holding = parseInt(json_array[i].Holding);
                        var Cancel = parseInt(json_array[i].Cancel);

                        if(i==0){
                            switch(document.getElementById("Sorting").value){
                                case 'BDATE':
                                    pre_date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    pre_date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                case '':
                                    pre_date = json_array[i].CIN_Date;
                                break;
                            }
                        }
                        else{
                            switch($("#Sorting").val()){
                                case 'BDATE':
                                    date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                default:
                                    date = json_array[i].CIN_Date;
                            }
                        }
                        if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                            if(date != '')
                                pre_date = date;
                            row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                        }
                        else
                            row = "";
                        var room_status = '';

                        if(Room_Count == Cancel){
                            room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                        }
                        else if(Holding>0){
                            if(Holding>1)
                                room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                            else
                                room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                        }
                        else{
                            if(Yet>0){
                                if(Yet>1)
                                    room_status = '<span class=\'red_dot\'>'+Yet+'</span>';
                                else
                                    room_status = '<span class=\'red_dot\' style="color:#F94D4D">'+Yet+'</span>';
                            }
                            else
                                room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_In>0){
                                if(Checked_In>1)
                                    room_status += '<span class=\'blue_dot\'>'+Checked_In+'</span>';
                                else
                                    room_status += '<span class=\'blue_dot\' style="color:#32C5FF">'+Checked_In+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_Out>0){
                                if(Checked_Out>1)
                                    room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                                else
                                    room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                        }
                        row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    $(document).ready(function() {

        $("#Sorting").change(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Phone = json_array[i].Customer_Phone;
                        var Customer_Email = json_array[i].Customer_Email;
                        var CIN_Date = json_array[i].CIN_Date;
                        var COUT_Date = json_array[i].COUT_Date;

                        var Room_Count = parseInt(json_array[i].Room_Count);

                        var Yet = parseInt(json_array[i].Yet);
                        var Checked_In = parseInt(json_array[i].Checked_In);
                        var Checked_Out = parseInt(json_array[i].Checked_Out);
                        var Holding = parseInt(json_array[i].Holding);
                        var Cancel = parseInt(json_array[i].Cancel);

                        if(i==0){
                            switch(document.getElementById("Sorting").value){
                                case 'BDATE':
                                    pre_date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    pre_date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                case '':
                                    pre_date = json_array[i].CIN_Date;
                                break;
                            }
                        }
                        else{
                            switch($("#Sorting").val()){
                                case 'BDATE':
                                    date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                default:
                                    date = json_array[i].CIN_Date;
                            }
                        }
                        if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                            if(date != '')
                                pre_date = date;
                            row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                        }
                        else
                            row = "";
                        var room_status = '';

                        if(Room_Count == Cancel){
                            room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                        }
                        else if(Holding>0){
                            if(Holding>1)
                                room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                            else
                                room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                        }
                        else{
                            if(Yet>0){
                                if(Yet>1)
                                    room_status = '<span class=\'red_dot\'>'+Yet+'</span>';
                                else
                                    room_status = '<span class=\'red_dot\' style="color:#F94D4D">'+Yet+'</span>';
                            }
                            else
                                room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_In>0){
                                if(Checked_In>1)
                                    room_status += '<span class=\'blue_dot\'>'+Checked_In+'</span>';
                                else
                                    room_status += '<span class=\'blue_dot\' style="color:#32C5FF">'+Checked_In+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_Out>0){
                                if(Checked_Out>1)
                                    room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                                else
                                    room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                        }
                        row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Fuzzy_Search").keyup(function() {
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Phone = json_array[i].Customer_Phone;
                        var Customer_Email = json_array[i].Customer_Email;
                        var CIN_Date = json_array[i].CIN_Date;
                        var COUT_Date = json_array[i].COUT_Date;

                        var Room_Count = parseInt(json_array[i].Room_Count);

                        var Yet = parseInt(json_array[i].Yet);
                        var Checked_In = parseInt(json_array[i].Checked_In);
                        var Checked_Out = parseInt(json_array[i].Checked_Out);
                        var Holding = parseInt(json_array[i].Holding);
                        var Cancel = parseInt(json_array[i].Cancel);

                        if(i==0){
                            switch(document.getElementById("Sorting").value){
                                case 'BDATE':
                                    pre_date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    pre_date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                case '':
                                    pre_date = json_array[i].CIN_Date;
                                break;
                            }
                        }
                        else{
                            switch($("#Sorting").val()){
                                case 'BDATE':
                                    date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                default:
                                    date = json_array[i].CIN_Date;
                            }
                        }
                        if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                            if(date != '')
                                pre_date = date;
                            row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                        }
                        else
                            row = "";
                        var room_status = '';

                        if(Room_Count == Cancel){
                            room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                        }
                        else if(Holding>0){
                            if(Holding>1)
                                room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                            else
                                room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                        }
                        else{
                            if(Yet>0){
                                if(Yet>1)
                                    room_status = '<span class=\'red_dot\'>'+Yet+'</span>';
                                else
                                    room_status = '<span class=\'red_dot\' style="color:#F94D4D">'+Yet+'</span>';
                            }
                            else
                                room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_In>0){
                                if(Checked_In>1)
                                    room_status += '<span class=\'blue_dot\'>'+Checked_In+'</span>';
                                else
                                    room_status += '<span class=\'blue_dot\' style="color:#32C5FF">'+Checked_In+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_Out>0){
                                if(Checked_Out>1)
                                    room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                                else
                                    room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                        }
                        row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Today").click(function(){
            var d = new Date();
            var Y = d.getFullYear();
            var M = d.getMonth()+1;
            var D = d.getDate();
            D = parseInt(D);
            if(D/10==0)
                D = "0"+D;
            // $("#Grouping").prop('selectedIndex',0);
            $("#Sorting").prop('selectedIndex',0);
            $("#Fuzzy_Search").val('');
            $("#Start").val(Y+"/"+M+"/"+D);
            $("#End").val(Y+"/"+M+"/"+D);
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Phone = json_array[i].Customer_Phone;
                        var Customer_Email = json_array[i].Customer_Email;
                        var CIN_Date = json_array[i].CIN_Date;
                        var COUT_Date = json_array[i].COUT_Date;

                        var Room_Count = parseInt(json_array[i].Room_Count);

                        var Yet = parseInt(json_array[i].Yet);
                        var Checked_In = parseInt(json_array[i].Checked_In);
                        var Checked_Out = parseInt(json_array[i].Checked_Out);
                        var Holding = parseInt(json_array[i].Holding);
                        var Cancel = parseInt(json_array[i].Cancel);

                        if(i==0){
                            switch(document.getElementById("Sorting").value){
                                case 'BDATE':
                                    pre_date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    pre_date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                case '':
                                    pre_date = json_array[i].CIN_Date;
                                break;
                            }
                        }
                        else{
                            switch($("#Sorting").val()){
                                case 'BDATE':
                                    date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                default:
                                    date = json_array[i].CIN_Date;
                            }
                        }
                        if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                            if(date != '')
                                pre_date = date;
                            row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                        }
                        else
                            row = "";
                        var room_status = '';

                        if(Room_Count == Cancel){
                            room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                        }
                        else if(Holding>0){
                            if(Holding>1)
                                room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                            else
                                room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                        }
                        else{
                            if(Yet>0){
                                if(Yet>1)
                                    room_status = '<span class=\'red_dot\'>'+Yet+'</span>';
                                else
                                    room_status = '<span class=\'red_dot\' style="color:#F94D4D">'+Yet+'</span>';
                            }
                            else
                                room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_In>0){
                                if(Checked_In>1)
                                    room_status += '<span class=\'blue_dot\'>'+Checked_In+'</span>';
                                else
                                    room_status += '<span class=\'blue_dot\' style="color:#32C5FF">'+Checked_In+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_Out>0){
                                if(Checked_Out>1)
                                    room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                                else
                                    room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                        }
                        row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        })

        $("#Reset").click(function(){
            // $("#Grouping").prop('selectedIndex',0);
            $("#Sorting").prop('selectedIndex',0);
            $("#Fuzzy_Search").val('');
            $("#Start").val('');
            $("#End").val('');
            $.ajax({
                type: "POST",
                url: "service.php",
                dataType: "json",
                data: {
                    Feature: "show_all",
                    Fuzzy_Search: $("#Fuzzy_Search").val(),
                    Sorting: $("#Sorting").val(),
                    // Grouping: $("#Grouping").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#result").html('');
                    $("#container").html('');
                    var json_array = data;
                    var pre_date = '';
                    var date = '';
                    var row = '';
                    for(i=0;i<json_array.length;i++){
                        var Booking_ID = json_array[i].Booking_ID;
                        var Customer_Name = json_array[i].Customer_Name;
                        var Customer_Phone = json_array[i].Customer_Phone;
                        var Customer_Email = json_array[i].Customer_Email;
                        var CIN_Date = json_array[i].CIN_Date;
                        var COUT_Date = json_array[i].COUT_Date;

                        var Room_Count = parseInt(json_array[i].Room_Count);

                        var Yet = parseInt(json_array[i].Yet);
                        var Checked_In = parseInt(json_array[i].Checked_In);
                        var Checked_Out = parseInt(json_array[i].Checked_Out);
                        var Holding = parseInt(json_array[i].Holding);
                        var Cancel = parseInt(json_array[i].Cancel);

                        if(i==0){
                            switch(document.getElementById("Sorting").value){
                                case 'BDATE':
                                    pre_date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    pre_date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                case '':
                                    pre_date = json_array[i].CIN_Date;
                                break;
                            }
                        }
                        else{
                            switch($("#Sorting").val()){
                                case 'BDATE':
                                    date = json_array[i].Booking_Date;
                                break;

                                case 'COUT':
                                    date = json_array[i].COUT_Date;
                                break;

                                case 'CIN':
                                default:
                                    date = json_array[i].CIN_Date;
                            }
                        }
                        if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                            if(date != '')
                                pre_date = date;
                            row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                        }
                        else
                            row = "";
                        var room_status = '';

                        if(Room_Count == Cancel){
                            room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                        }
                        else if(Holding>0){
                            if(Holding>1)
                                room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                            else
                                room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                        }
                        else{
                            if(Yet>0){
                                if(Yet>1)
                                    room_status = '<span class=\'red_dot\'>'+Yet+'</span>';
                                else
                                    room_status = '<span class=\'red_dot\' style="color:#F94D4D">'+Yet+'</span>';
                            }
                            else
                                room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_In>0){
                                if(Checked_In>1)
                                    room_status += '<span class=\'blue_dot\'>'+Checked_In+'</span>';
                                else
                                    room_status += '<span class=\'blue_dot\' style="color:#32C5FF">'+Checked_In+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                            if(Checked_Out>0){
                                if(Checked_Out>1)
                                    room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                                else
                                    room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                            }
                            else
                                room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                        }
                        row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                        $("#container").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
                }
            })
        })

        $("#New_Order").click(function(){
            location.href = '../check_in/reservation.php';
        })

        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_all",
                Fuzzy_Search: $("#Fuzzy_Search").val(),
                Sorting: $("#Sorting").val(),
                // Grouping: $("#Grouping").val(),
                Start: $("#Start").val(),
                End: $("#End").val()
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html('');
                var json_array = data;
                var pre_date = '';
                var date = '';
                var row = '';
                for(i=0;i<json_array.length;i++){
                    console.log(json_array[i]);
                    var Booking_ID = json_array[i].Booking_ID;
                    var Customer_Name = json_array[i].Customer_Name;
                    var Customer_Phone = json_array[i].Customer_Phone;
                    var Customer_Email = json_array[i].Customer_Email;
                    var CIN_Date = json_array[i].CIN_Date;
                    var COUT_Date = json_array[i].COUT_Date;

                    var flag = 0;

                    var Yet = parseInt(json_array[i].Yet);
                    var Checked_In = parseInt(json_array[i].Checked_In);
                    var Checked_Out = parseInt(json_array[i].Checked_Out);
                    var Holding = parseInt(json_array[i].Holding);
                    var Cancel = parseInt(json_array[i].Cancel);

                    if(i==0){
                        switch(document.getElementById("Sorting").value){
                            case 'BDATE':
                                pre_date = json_array[i].Booking_Date;
                            break;

                            case 'COUT':
                                pre_date = json_array[i].COUT_Date;
                            break;

                            case 'CIN':
                            case '':
                                pre_date = json_array[i].CIN_Date;
                            break;
                        }
                    }
                    else{
                        switch($("#Sorting").val()){
                            case 'BDATE':
                                date = json_array[i].Booking_Date;
                            break;

                            case 'COUT':
                                date = json_array[i].COUT_Date;
                            break;

                            case 'CIN':
                            default:
                                date = json_array[i].CIN_Date;
                        }
                    }
                    if(pre_date.split(" ")[0]!=date.split(" ")[0]){
                        if(date != '')
                            pre_date = date;
                        row = "<div class='separator'>"+pre_date.replace(/-/g, "/").split(" ")[0]+"</div>";
                    }
                    else
                        row = "";
                    var room_status = '';

                    if(Holding>0){
                        flag = 1;
                        if(Holding>1)
                            room_status = '<span class=\'purple_dot\'>'+Holding+'</span>';
                        else
                            room_status = '<span class=\'purple_dot\' style="color:#c632e6">0</span>';
                    }
                    else{
                        if(Yet>0){
                            flag = 1;
                            if(Yet>1)
                                room_status = '<span class=\'blue_dot\'>'+Yet+'</span>';
                            else
                                room_status = '<span class=\'blue_dot\' style="color:#32C5FF">'+Yet+'</span>';
                        }
                        else
                            room_status = '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                        if(Checked_In>0){
                            flag = 1;
                            if(Checked_In>1)
                                room_status += '<span class=\'red_dot\'>'+Checked_In+'</span>';
                            else
                                room_status += '<span class=\'red_dot\' style="color:#F94D4D">'+Checked_In+'</span>';
                        }
                        else
                            room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';

                        if(Checked_Out>0){
                            flag = 1;
                            if(Checked_Out>1)
                                room_status += '<span class=\'yellow_dot\'>'+Checked_Out+'</span>';
                            else
                                room_status += '<span class=\'yellow_dot\' style="color:#F7C700">'+Checked_Out+'</span>';
                        }
                        else
                            room_status += '<span class=\'grey_dot\' style="color:#C9C9C9">0</span>';
                    }
                    if(flag == 0){
                        room_status = '<img src="/hotel_management_system/images/cross.png" width="35px" height="35px">';
                    }
                    row += "<form action='change_info.php' method='POST'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='info_button_table'><center><table class='info_table'><tr><td class='info_td' width='35%'>訂單編號：<span style='color:#0091FF'>"+Booking_ID+"</span></td><td class='info_td' width='40%'>旅客姓名："+Customer_Name+"</td><td class='function_td'>" + room_status + "</td></tr><tr><td class='info_td'>入住日期："+CIN_Date+"</td><td class='info_td'>連絡電話："+Customer_Phone+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_info' class='function_btn' style='font-size:20px;width:200px;height:50px;background-color:#0091FF'>訂單資訊</button></td></tr><tr><td class='info_td'>退房日期："+COUT_Date+"</td><td class='info_td'>E-mail："+Customer_Email+"</td><td class='function_td'><button type='submit' name='Action' value='"+Booking_ID+"_payment' class='function_btn' style='font-size:20px;;width:200px;height:50px;background-color:#0091FF'>收款結帳</button></td></tr></table></center></button></form>";
                    $("#container").append(row);
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">RRRRRRRRRRR' + jqXHR.responseText + '</font>');
            }
        })
    });
</script>