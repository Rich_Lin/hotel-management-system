<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    // error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //////////////////Customer info//////////////////
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'show_all':
                    show_all('show_all',$_POST,$conn);
                break;

                case 'check_in':
                    $message = '';
                    date_default_timezone_set('Asia/Taipei');
                    $ACIN_Datetime = date('Y-m-d H:i:s', time());
                    $Now_Date = date('Y-m-d', time());
                    for($i=0;$i<sizeof($_POST['room_num_array']);$i++){
                        $sql = "SELECT * FROM `booking_detail`,`booking_index` WHERE `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND `booking_detail`.`Room_Num`='".$_POST['room_num_array'][$i]."' AND (`booking_detail`.`Room_Status` BETWEEN 0 AND 4) AND `booking_index`.`CIN_Date`<'".$_POST['CIN_Date']."' ORDER BY `booking_index`.`CIN_Date` DESC";
                        $result = mysqli_query($conn,$sql);
                        if(mysqli_num_rows($result)==0){
                            $sql = "SELECT * FROM `booking_detail` WHERE `Booking_ID`='".$_POST['Booking_ID']."' AND `Room_Num`='".$_POST['room_num_array'][$i]."'";
                            $result = mysqli_query($conn,$sql);
                            $row = $result->fetch_assoc();
                            // if($row['Keycard_Counter']<1)
                            //     $message .= "房間：" . $_POST['room_num_array'][$i] . " 之房卡張數不得為「0」\n";
                            // else{
                                $sql = "SELECT DATE(`CIN_Date`) FROM `booking_index` WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                                $result = mysqli_query($conn,$sql);
                                $row = $result->fetch_assoc();
                                if($Now_Date >= $row['DATE(`CIN_Date`)']){
                                    $sql = "UPDATE `booking_detail` SET `Actual_CIN_Datetime`='".$ACIN_Datetime."',`Room_Status`='1' WHERE `Booking_ID`='".$_POST['Booking_ID']."' AND `Room_Num`='".$_POST['room_num_array'][$i]."'";
                                    if(!mysqli_query($conn,$sql)){
                                        echo "This SQL: " . $sql . "<br>";
                                        die;
                                    }
                                }
                                else{
                                    $message .= "房間：" . $_POST['room_num_array'][$i] . " 尚未到能入住之時間\n";
                                }
                            // }
                        }
                        else{
                            $message .= "房間：" . $_POST['room_num_array'][$i] . " 目前仍在佔用中\n";
                        }
                    }
                    get_room_status($_POST['Booking_ID'],$message,$conn);
                break;

                case 'pay':
                    date_default_timezone_set('Asia/Taipei');
                    $Payment_Datetime = date('Y/m/d H:i:s', time());
                    $sql = "UPDATE `booking_index` SET `Total_Price`='".$_POST['cal_end_price']."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                    for($i=0;$i<2 && $i<=$_POST['Flag'];$i++){
                        if(($_POST['Payment'][$i]!='0' || $_POST['Payment'][$i]!='') || $_POST['Remark'][$i]!=''){
                            $Remark = addslashes(str_replace("<br />", '',nl2br($_POST['Remark'][$i])));
                            if($_POST['Payment_Type'] == '4')
                                $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$_POST['Booking_ID']."','".$_POST['Payment_Type']."','".$_POST['Payment_Method'][$i]."','-".$_POST['Payment'][$i]."','".$Remark."','".$_POST['Staff_ID']."','".$Payment_Datetime."')";
                            else
                                $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$_POST['Booking_ID']."','".$_POST['Payment_Type']."','".$_POST['Payment_Method'][$i]."','".$_POST['Payment'][$i]."','".$Remark."','".$_POST['Staff_ID']."','".$Payment_Datetime."')";
                            if(!mysqli_query($conn,$sql)){
                                echo "This SQL: " . $sql . "<br>";
                                die;
                            }
                        }
                    }
                    get_payment_status($_POST['Booking_ID'],$conn);
                break;

                case 'display_expense':
                    $info_array = array();
                    $sql = "SELECT * FROM `expense_order`,`booking_index`,`extra_expense` WHERE `booking_index`.`Booking_ID`=`expense_order`.`Booking_ID` AND `expense_order`.`Extra_Expense_ID`=`extra_expense`.`Extra_Expense_ID` AND `booking_index`.`Booking_ID`='".$_POST['Booking_ID']."'";
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result)>0){
                        while($rows = $result->fetch_assoc()){
                            $info_array[$rows['EO_ID']]['EO_ID'] = $rows['EO_ID'];
                            $info_array[$rows['EO_ID']]['Extra_Expense_Name'] = $rows['Extra_Expense_Name'];
                            $info_array[$rows['EO_ID']]['Extra_Expense_Price'] = $rows['Extra_Expense_Price'];
                            $info_array[$rows['EO_ID']]['Expense_Count'] = $rows['Expense_Count'];
                            $info_array[$rows['EO_ID']]['Expense_Total_Price'] = $rows['Expense_Total_Price'];
                            $info_array[$rows['EO_ID']]['Expense_Remark'] = str_replace(chr(13).chr(10), "<br />",nl2br($rows['Expense_Remark']));
                        }
                    }
                    sort($info_array);
                    // print_r($info_array);
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'add_expense':
                    $sql = "INSERT INTO `expense_order`(`Booking_ID`, `Extra_Expense_ID`, `Expense_Count`, `Expense_Total_Price`, `Expense_Remark`) VALUES ('".$_POST['Booking_ID']."','".$_POST['Extra_Expense_ID']."','".$_POST['Expense_Count']."','".addslashes($_POST['Expense_Total_Price'])."','".addslashes(str_replace("<br />", '',nl2br($_POST['Expense_Remark'])))."')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                        die;
                    }
                    $sql = "UPDATE `booking_index` SET `Total_Price`=`Total_Price`+'".$_POST['Expense_Total_Price']."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                        die;
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'delete_expense':
                    $sql = "SELECT * FROM `expense_order` WHERE `EO_ID`='".$_POST['EO_ID']."'";
                    $result = mysqli_query($conn,$sql);
                    $row = $result->fetch_assoc();
                    $sql = "DELETE FROM `expense_order` WHERE `EO_ID`='".$_POST['EO_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                        die;
                    }
                    $sql = "UPDATE `booking_index` SET `Total_Price`=`Total_Price`-'".$row['Expense_Total_Price']."' WHERE `Booking_ID`='".$row['Booking_ID']."'";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                        die;
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'get_payment_status':
                    get_payment_status($_POST['Booking_ID'],$conn);
                break;

                case 'modify_data':
                    modify_data($_POST,$conn);
                break;

                case 'check_clash':
                    check_clash($_POST,$conn);
                break;

                case 'cancel_booking':
                    cancel_booking($_POST,$conn);
                break;
            }
        }
    }

    function show_all($From,$Update_Info,$conn){
        $sql = "SELECT * FROM `booking_index`,`customer`,`booking_detail`";
        $WHERE = " WHERE (`customer`.`Customer_ID`=`booking_index`.`Customer_ID` AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `customer`.`Enable`=1)";
        $group_by = " GROUP BY `booking_index`.`Booking_ID`";
        $order_by = " ORDER BY `booking_index`.`CIN_Date` DESC";
        $Fuzzy_Search = "";
        $Sorting = " ORDER BY `booking_index`.`COUT_Date`";
        $Date = "";
        if(!empty($Update_Info['Fuzzy_Search'])){
            $Update_Info['Fuzzy_Search'] = addslashes($Update_Info['Fuzzy_Search']);
            $Fuzzy_Search = " (`customer`.`Customer_Name` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_Phone` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_Email` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_SSID` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `customer`.`Customer_Passport` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `booking_index`.`Booking_ID` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `booking_detail`.`Room_Type` LIKE '%".$Update_Info['Fuzzy_Search']."%' OR `booking_detail`.`Room_Num` LIKE '%".$Update_Info['Fuzzy_Search']."%') ";
        }
        
        if(isset($Update_Info["Sorting"])){
            switch($Update_Info["Sorting"]){

                case "CIN":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`booking_index`.`CIN_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `booking_index`.`CIN_Date`";
                break;

                case "COUT":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`COUT_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`COUT_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`booking_index`.`COUT_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`COUT_Date` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`booking_index`.`COUT_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`COUT_Date` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `booking_index`.`COUT_Date`";
                break;

                
                case "BDATE":
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`Booking_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`Booking_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                if($Update_Info['Start'] > $Update_Info['End'])
                                    $Date = " ((`booking_index`.`Booking_Date` >= '" . $Update_Info['End'] . "') AND (`booking_index`.`Booking_Date` <= '" . $Update_Info['Start'] . "'))";
                                else
                                    $Date = " ((`booking_index`.`Booking_Date` >= '" . $Update_Info['Start'] . "') AND (`booking_index`.`Booking_Date` <= '" . $Update_Info['End'] . "'))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `booking_index`.`Booking_Date`";
                break;

                default:
                    if(isset($Update_Info['Start']) || isset($Update_Info['End'])){
                        $Update_Info['Start'] = addslashes($Update_Info['Start']);
                        $Update_Info['End'] = addslashes($Update_Info['End']);
                        if(!empty($Update_Info['Start']) || !empty($Update_Info['End'])){
                            if(empty($Update_Info['End']))
                                $Date = " (`booking_index`.`COUT_Date` >= '" . $Update_Info['Start'] . "')";
                            else if(empty($Update_Info['Start']))
                                $Date = " (`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "')";
                            else{
                                $Date = " ((`booking_index`.`CIN_Date` <= '" . $Update_Info['End'] . "' AND '" . $Update_Info['End'] . "' <= `booking_index`.`COUT_Date`) OR (`booking_index`.`CIN_Date` <= '" . $Update_Info['Start'] . "' AND '" . $Update_Info['Start'] . "' <= `booking_index`.`COUT_Date`))";
                            }
                        }
                    }
                    $Sorting = " ORDER BY `booking_index`.`CIN_Date`";
            
            }
        }
        $sql .= $WHERE;
        if($Fuzzy_Search != '')
            $sql = $sql . " AND " . $Fuzzy_Search;
        if($Date != '')
            $sql .= " AND " . $Date;
        $sql = $sql . $group_by . $Sorting . " DESC";
        // echo $sql;die;
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            $info_array[$count]['Yet']=0;
            $info_array[$count]['Checked_In']=0;
            $info_array[$count]['Checked_Out']=0;
            $info_array[$count]['Holding']=0;
            $info_array[$count]['Cancel']=0;
            foreach($row as $key => $value){
                $info_array[$count][$key] = $value;
                if($key=='Booking_ID'){
                    $sql = "SELECT `Room_Status` FROM `booking_detail` WHERE `Booking_ID`='".$value."'";
                    $rs_result = mysqli_query($conn,$sql);
                    $Booking_Array = array();
                    while($rs_row = $rs_result->fetch_assoc()){
                        switch($rs_row['Room_Status']){
                            case '0':
                                $info_array[$count]['Yet']+=1;
                            break;
                            
                            case '1':
                                $info_array[$count]['Checked_In']+=1;
                            break;
                            
                            case '2':
                            case '3':
                            case '5':
                                $info_array[$count]['Checked_Out']+=1;
                            break;
                            
                            case '7':
                                $info_array[$count]['Holding']+=1;
                            break;
                            
                            case '8':
                                $info_array[$count]['Cancel']+=1;
                            break;
                        }
                    }
                }
            }
            $count++;
        }
        // echo "<pre>";
        // print_r($info_array);
        // echo "</pre>";die;
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }

    function get_room_status($Booking_ID,$message,$conn){
        $sql = "SELECT * FROM `booking_detail` WHERE `Booking_ID`='".$Booking_ID."'";
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                if($key == 'Detail_Remark')
                    $value = str_replace(chr(13).chr(10), "<br />",$value);
                else if($key == 'Actual_CIN_Datetime')
                    $value = str_replace("-", "/",$value);
                $info_array['Rooms'][$count][$key] = $value;
            }
            $count++;
        }
        $info_array['message'] = $message;
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }

    function get_payment_status($Booking_ID,$conn){
        $sql = "SELECT * FROM `payment` WHERE `Booking_ID`='$Booking_ID'";
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                // if($key == 'Payment_Remark')
                //     $value = str_replace(chr(13).chr(10), "<br />",nl2br($value));
                $info_array[$count][$key] = $value;
            }
            $count++;
        }
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }

    function modify_data($infos,$conn){
        $Sep_Price = $infos['Total_Price'] / sizeof($infos['Keycard_Counter']);
        $sql = "UPDATE `booking_index` SET `CIN_Date`='".$infos['CIN_Date']."', `COUT_Date`='".$infos['COUT_Date']."', `Day_Count`='".$infos['Day_Count']."', `People_Count`='".$infos['People_Count']."', `Total_Price`='".$infos['Total_Price']."', `Index_Remark`='".addslashes($infos['Index_Remark'])."' WHERE `Booking_ID`='".$infos['Booking_ID']."'";
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }

        $sql = "SELECT `Room_Num` FROM `booking_detail` WHERE `Booking_ID`='".$infos['Booking_ID']."'";
        $result = mysqli_query($conn,$sql);
        for($i=0;$row = $result->fetch_assoc();$i++){
            $sql = "UPDATE `booking_detail` SET `Keycard_Counter`='".$infos['Keycard_Counter'][$i]."' WHERE `Booking_ID`='".$infos['Booking_ID']."' AND `Room_Num`='".$row['Room_Num']."'";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
        }

        $sql = "UPDATE `customer` SET `Customer_Name`='".$_POST['Customer_Name']."', `Customer_Sex`='".$_POST['Customer_Sex']."', `Customer_Phone`='".$_POST['Customer_Phone']."', `Customer_Email`='".$_POST['Customer_Email']."', `Customer_Nationality`='".$_POST['Customer_Nationality']."', `Customer_SSID`='".$_POST['Customer_SSID']."', `Customer_Passport`='".$_POST['Customer_Passport']."' WHERE `Customer_ID`='".$_POST['Customer_ID']."'";
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
            die;
        }

        echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
    }

    function check_clash($infos,$conn){
        $Total_Price = 0;
        for($i=0;$i<sizeof($infos['Room_Num']);$i++){
            $sql = "SELECT `booking_detail`.`Booking_ID`, `booking_detail`.`Room_Num` FROM `booking_index`,`booking_detail` WHERE `booking_detail`.`Room_Num`='".$infos['Room_Num'][$i]."' AND `booking_detail`.`Booking_ID`!='".$infos['Booking_ID']."' AND `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND (`booking_detail`.`Room_Status`!='5' AND `booking_detail`.`Room_Status`!='8')";
            $Time_Argument = "(('".$infos['CIN_Date']."' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR ('".$infos['COUT_Date']."' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR (`booking_index`.`COUT_Date` BETWEEN '".$infos['CIN_Date']."' AND '".$infos['COUT_Date']."') OR (`booking_index`.`CIN_Date` BETWEEN '".$infos['CIN_Date']."' AND '".$infos['COUT_Date']."'))";
            $sql = $sql . " AND " . $Time_Argument;
            $result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($result)>0){
                echo $sql;
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
        }

        $sql = "SELECT * FROM `discount`,`booking_index` WHERE `booking_index`.`Discount_ID`=`discount`.`Discount_ID` AND `booking_index`.`Booking_ID`='".$infos['Booking_ID']."'";
        $result = mysqli_query($conn,$sql);
        $Discount_row = $result->fetch_assoc();

        $sql = "SELECT * FROM `room_type`,`booking_index`,`booking_detail` WHERE `booking_detail`.`Room_Type`=`room_type`.`Room_Type` AND `booking_index`.`Booking_ID`='".$infos['Booking_ID']."' AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID`";
        $result = mysqli_query($conn,$sql);
        $Room_row = $result->fetch_assoc();

        $Day_Count = $infos['Day_Count'];

        $temp_date = date($infos['CIN_Date']);
        for($i=0;$i<$Day_Count;$i++){
            if(($temp_date <= $Discount_row['Discount_End_Date'] && $temp_date >= $Discount_row['Discount_Start_Date'])){
                switch($Discount_row['Discount_Type']){
                    case '0':
                        if(strpos($Room_row['WeekDay_Days'],date_format(date_create($temp_date),'w')) !== false)
                            $Total_Price+=ceil($Room_row['WeekDay_Price']*($Discount_row['Cal_Method']/10));
                        else
                            $Total_Price+=ceil($Room_row['Weekend_Price']*($Discount_row['Cal_Method']/10));
                    break;

                    case '1':
                        $Total_Price+=$Discount_row['Change_Price'];
                    break;
                }
            }
            else{
                if(strpos($Room_row['WeekDay_Days'],date_format(date_create($temp_date),'w')) !== false)
                    $Total_Price+=$Room_row['WeekDay_Price'];
                else
                    $Total_Price+=$Room_row['Weekend_Price'];
            }
            $temp_date = date_format(date_create($temp_date)->modify('+1 day'), 'Y-m-d');
        }
        // echo $Total_Price;die;
        echo json_encode(array('Success' => true,'Total_Price' => $Total_Price),JSON_UNESCAPED_UNICODE);
    }

    function cancel_booking($infos,$conn){
        $Booking_ID = $infos['Booking_ID'];
        if(isset($infos['Room_Num_Array'])){
            $room_num_array = $infos['Room_Num_Array'];
            for($i=0;$i<sizeof($room_num_array);$i++){
                $sql = "UPDATE `booking_detail` SET `Room_Status`='8' WHERE `Booking_ID`='".$Booking_ID."' AND `Room_Num`='".$room_num_array[$i]."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                    die;
                }
            }
            $sql = "SELECT * FROM `booking_detail` WHERE `Booking_ID`='".$Booking_ID."' AND `Room_Status`!='8'";
            $result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($result)==0){
                $sql = "UPDATE `booking_index` SET `Overall_Status`='8' WHERE `Booking_ID`='".$Booking_ID."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                    die;
                }
            }
        }
        else{
            $sql = "UPDATE `booking_detail` SET `Room_Status`='8' WHERE `Booking_ID`='".$Booking_ID."'";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
            $sql = "UPDATE `booking_index` SET `Overall_Status`='8' WHERE `Booking_ID`='".$Booking_ID."'";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                echo json_encode(array('Success' => false),JSON_UNESCAPED_UNICODE);
                die;
            }
        }
        $sql = "SELECT `Room_Num`, `Actual_CIN_Datetime`, `Price`, `Room_Status`, `Keycard_Counter`,`Detail_Remark` FROM `booking_detail` WHERE `Booking_ID`='".$Booking_ID."'";
        $info_array = array();
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            foreach($row as $key => $value){
                if($key == 'Detail_Remark')
                    $value = stripslashes(str_replace(chr(13).chr(10), "<br />",$value));
                else if($key == 'Actual_CIN_Datetime')
                    $value = str_replace("-", "/",$value);
                $info_array['Rooms'][$count][$key] = $value;
            }
            $count++;
        }
        echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
    }
?>