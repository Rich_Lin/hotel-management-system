<head>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    
    <style>
        td{
            width: 50%;
            /* overflow:hidden;
            white-space: nowrap; */
            /* text-overflow: ellipsis; */
            height: 65px;
        }
        .function_btn{
            border-radius:15px;
        }
        .info_table{
            border-radius: 15px;
            margin-top: 30px;
            table-layout: fixed;
            width: 100%;
            font-size:23px;
            /* margin: 17.5px 0px; */
        }
        .info_td{
            padding-left: 10px;
            border: 0;
        }
        .rooms{
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .ui-dialog-titlebar{
            display: none
        }
        .ui-widget, .ui-widget-content, .ui-dialog-content{
            border-radius: 20px;
            /* border-width: 20px; */
            /* background-color: #DADADA;
            border: 1px solid #DADADA; */
        }
        .ui-widget-content {
            /* border-radius: 20px; */
            /* border-width: 20px; */
        }
        .ui-widget-overlay{
            background-color: transparent;
        }
        input[type='text'],input[type='password'],select{
            border-radius: 7px;
        }
    </style>
</head>
<script type="text/javascript" src="../functions.js"></script>
<body onload="includeHTML();ajax_function();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div> 
    <div class='right'>
        <div id='result'></div>
        <center>
            <table border="0" cellpadding="10" style='width:95%'>
                <tr>
                    <td width='50%'><span style='font-size:50px'>人員管理</span></td><td width='50%' style='text-align:right'><input class='function_btn access_level AC_2' style='width:180px;height:50px;background-color:#F79B00;color:white;font-size:20px' type='button' id='Add_Staff' value='新增員工'></td>
                </tr>
            </table>
            <table style='width:95%;border-collapse:collapse;font-size:23px;table-layout:fixed' id='container' border='0'></table>
        </center>
    </div>
</body>

<div id="dialog">
    <br>
    <center>
        <form id='myform' action='service.php'>
            <input type="hidden" autofocus="true" />
            <table width='85%' style='table-layout: fixed;'>
                <tr>
                    <td style='font-size:36px;' colspan='2'>新增/編輯人員：</td>
                </tr>
                <tr>
                    <td style='font-size:26px'>員工編號：<input type='text' style='width:150px' name='Staff_ID' id='Staff_ID' required>
                    <td style='font-size:26px'>職稱：<input type='text' style='width:160px' class='edit_column' name='Staff_Title' id='Staff_Title' required></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>人員名稱：<input type='text' style='width:150px' class='edit_column' name='Staff_Name' id='Staff_Name' required></td>
                    <td style='font-size:26px'>職等：
                        <select class='modified_select' name='Staff_Level' id='Staff_Level' required>
                            <option value=''>請選擇職等</option>
                            <option value='1'>老闆</option>
                            <option value='2'>經理</option>
                            <option value='3'>櫃台人員</option>
                            <option value='4'>清潔人員</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style='font-size:26px' colspan='2'>連絡電話：<input type='text' style='width:400px' class='edit_column' name='Staff_Phone' id='Staff_Phone' required></td>
                </tr>
                <tr>
                    <td style='font-size:26px' colspan='2'>所屬部門：<input type='text' style='width:400px' class='edit_column' name='Department' id='Department' required></td>
                </tr>
                <tr>
                    <td style='font-size:26px' colspan='2'>員工帳號：<input type='text' style='width:400px' class='edit_column' name='Staff_Account' id='Staff_Account' required></td>
                </tr>
                <tr>
                    <td style='font-size:26px' colspan='2'>員工密碼：<input type='password' style='width:400px' class='edit_column' name='Staff_PWD' id='Staff_PWD' required></td>
                </tr>
                <tr>
                    <td style='font-size:26px' colspan='2'><center><input type='submit' class='function_btn' style='width:435px;height:80px;background-color:#0091FF;color:WHITE' name='Customer_SSID' value='完成'></center></td>
                </tr>
            </table>
            <input type='hidden' name='Feature' id='Feature'>
        </form>
    </center>
</div>

<script>
    function add_num(){
        var rows = document.getElementById('add_room').getElementsByTagName('span').length;
        var table = document.getElementById('add_room');
        var row = table.insertRow(rows);
        var cell = row.insertCell(0);
        cell.innerHTML = "<span>" + paddingLeft( rows+1 , 2) + ". <input type='text' name='Room_Num["+rows+"]' required></span>";
    }
    
    function paddingLeft(str,lenght){
        if(str.toString().length >= lenght)
            return str;
        else
            return paddingLeft("0" +str,lenght);
    }

    function autogrow(textarea){
        var adjustedHeight=textarea.clientHeight;
        adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
        if (adjustedHeight>textarea.clientHeight){
            textarea.style.height=adjustedHeight+'px';
        }
    }
    
    $(document).ready(function() {
        $("#dialog").dialog({
            height: 630,
            width: 745,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Add_Staff").click(function(){
            $("#Feature").val('Add_Staff');
            $("#Staff_ID").val('');
            $("#Staff_Name").val('');
            $("#Department").val('');
            $("#Staff_Title").val('');
            $("#Staff_Phone").val('');
            $("#Staff_Level")[0].selectedIndex = -1
            $("#Staff_Level").val('').attr("selected", "true");
            $("#Staff_Account").val('');
            $("#Staff_PWD").val('');
            $( "#dialog" ).dialog( "open" );
        });

        $("#myform").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#myform").serialize(),
                success: function(data) {
                    // $("#result").html('');
                    // $("#container").html("<tr style='background-color:#6236FF;color:white'><td style='padding:15px;width:15%'>員工編號</td><td style='padding:15px;width:15%'>姓名</td><td style='padding:15px;width:15%'>部門</td><td style='padding:15px;width:15%'>職稱</td><td style='padding:15px;width:15%'>連絡電話</td><td style='padding:15px;width:20%'></td></tr>");
                    // // var row='';
                    // var json_array = data;
                    // for(i=0;i<json_array.length;i++){
                    //     var row='';
                    //     var Staff_ID = json_array[i].Staff_ID;
                    //     var Staff_Name = json_array[i].Staff_Name;
                    //     var Department = json_array[i].Department;
                    //     var Staff_Title = json_array[i].Staff_Title;
                    //     var Staff_Phone = json_array[i].Staff_Phone;
                    //     row += "<tr><td colspan='6'><table border='1' class='info_table' width='100%'><tr><td class='info_td' style='padding:15px;width:15%'>"+Staff_ID+"</td><td class='info_td' style='padding:15px;width:15%'>"+Staff_Name+"</td><td class='info_td rooms' style='padding:15px;width:15%'>"+Department+"</td><td class='info_td' style='padding:15px;width:15%'>"+Staff_Title+"</td><td class='info_td' style='padding:15px;width:15%'>"+Staff_Phone+"</td><td class='info_td' style='padding:15px;width:20%;text-align:right'><button class='function_btn access_level AC_3 self_check' type='submit' name='edit_staff' style='width:100px;height:50px;font-size:24px;margin-right:20px;background-color:WHITE;color:BLACK;' value='"+Staff_ID+"' onClick='Edit_Staff(this)'>編輯</button><button type='submit' class='function_btn access_level AC_2 self_check' name='delete' style='width:100px;height:50px;font-size:24px;background-color:#E02020;color:WHITE' value='"+Staff_ID+"' onClick='Delete_Staff(this)'>刪除</button></td></tr></table></td></tr>";
                    //     $("#container").append(row);
                    // }
                    close_dialog();
                    ajax_function();
                },
                error: function(jqXHR) {
                    alert(jqXHR.responseText);
                }
            })
        })
    });

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        $("#Staff_ID").val('');
        $("#Staff_ID").attr('readonly', false);
        $("#Staff_Name").val('');
        $("#Department").val('');
        $("#Staff_Title").val('');
        $("#Staff_Phone").val('');
        $('#dialog').dialog( "close" );
    }

    function Edit_Staff(Target){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "Show_Staff_Info",
                Staff_ID: Target.value
            },
            success: function(data) {
                $("#result").html('');
                $("#Feature").val('Edit_Staff');
                $("#Staff_ID").val(data.Staff_ID);
                $("#Staff_ID").attr('readonly', true);
                $("#Staff_Name").val(data.Staff_Name);
                $("#Department").val(data.Department);
                $("#Staff_Title").val(data.Staff_Title);
                $("#Staff_Phone").val(data.Staff_Phone);
                $("#Staff_Account").val(data.Staff_Account);
                $("#Staff_PWD").val(data.Staff_PWD);
                $("#Staff_Level").val(data.Staff_Level).attr("selected", "true");
                
                
                $( "#dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }
    
    function Delete_Staff(Staff_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "delete_staff",
                Staff_ID: Staff_ID.value
            },
            success: function(data) {
                ajax_function();
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }

    function ajax_function(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_all",
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html("<tr style='background-color:#6236FF;color:white'><td class='info_td' style='width:10%'>員工編號</td><td class='info_td' style='width:15%'>姓名</td><td class='info_td' style='width:15%'>部門</td><td class='info_td' style='width:17%'>職稱</td><td class='info_td' style='width:40%'>連絡電話</td></tr>");
                // var row='';
                var json_array = data;
                for(i=0;i<json_array.length;i++){
                    var row='';
                    var Staff_ID = json_array[i].Staff_ID;
                    var Staff_Name = json_array[i].Staff_Name;
                    var Department = json_array[i].Department;
                    var Staff_Title = json_array[i].Staff_Title;
                    var Staff_Phone = json_array[i].Staff_Phone;
                    row += "<tr><td colspan='5'><table border='1' class='info_table' width='100%'><tr><td class='info_td' style='width:10%'>"+Staff_ID+"</td><td class='info_td' style='width:15%'>"+Staff_Name+"</td><td class='info_td rooms' style='width:15%'>"+Department+"</td><td class='info_td' style='width:17.5%'>"+Staff_Title+"</td><td class='info_td' style='width:20%'>"+Staff_Phone+"</td><td class='info_td' style='width:20%;text-align:right;padding-right:10px'><button class='function_btn access_level AC_3 self_check' type='submit' name='edit_staff' style='width:100px;height:50px;font-size:24px;background-color:WHITE;color:BLACK;' value='"+Staff_ID+"' onClick='Edit_Staff(this)'>編輯</button><button type='submit' class='function_btn access_level AC_2 self_check' name='delete' style='width:100px;height:50px;font-size:24px;margin-left:20px;background-color:#E02020;color:WHITE' value='"+Staff_ID+"' onClick='Delete_Staff(this)'>刪除</button></td></tr></table></td></tr>";
                    $("#container").append(row);
                }
                $("#container").append("<tr><td colspan='5'></td></tr>");
            },
            error: function(jqXHR) {
                // $("#result").html('<font color="#ff0000">發生錯誤：GGGGGGGGGGGGGGGG' + jqXHR.responseText + '</font>');
                console.log(jqXHR.responseText);

            }
        })
    }
</script>