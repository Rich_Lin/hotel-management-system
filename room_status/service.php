<?php
header('Content-Type: text/html; charset=UTF-8');
include_once "../mysql_connect.inc.php";
$addable = array('可','否');
if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $sql1="SELECT * FROM `room_status`";
    $result = mysqli_query($conn,$sql1);
    $jSON_Array = array();
    $col_count = 0;
    $key_flag1 = 0;
    $key_flag2 = 0;
    $value_flag = 0;
    $count = 0;
    
    echo "<table border='1'><tr>";
    while($row = $result->fetch_assoc())
    {
        foreach($row as $key => $value){
            if($key_flag1 == 0){
                // echo "<td></td>";
                $key_flag1 = 1;
            }
            if($key_flag2 == 0){
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = '$key'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo "<td style='text-align:center'>" . $rows[2] . "</td>";
            }
        }
        if($key_flag2 == 0)
            echo "</tr>";
        $key_flag2 = 1;
    }
    $result = mysqli_query($conn,$sql1);
    while($row = $result->fetch_assoc())
    {
        $room_num='';
        echo "<tr>";
        foreach($row as $key => $value){
            if($key == 'Addable')
                echo "<td style='text-align:center'>".$addable[$value]."</td>";
            else{
                $value = str_replace(chr(13).chr(10), "<br />",$value);
                echo "<td>" . $value . "</td>";
                if($key=='Room_Num') $room_num=$value;
                $jSON_Array[$count][] = array($key => $value);
            }
        }
        $count++;
        echo "<td><form action='change_info.php' method='POST'><button name='Change_Info' value='".$room_num."'>更改資訊</button></form></td>";
    }
} else {
    echo json_encode(array(
        'errorMsg' => '請求無效，只允許 POST 方式訪問！'
    ),JSON_UNESCAPED_UNICODE);
}

mysqli_close($conn);
?>