<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <style>
            * {
                font-family: Microsoft JhengHei;
            }
            #login_form{
                position: absolute;
                left: 50%;
                top: 50%;
                transform: translate(-50%, -50%);
                text-align: center;
            }
            #contain{
                position: relative;
                border: 1px solid #979797;
                border-radius: 15%;
                padding: 50px;
                font-size: 30px;
                text-align: center;
            }
            input{
                margin:10px
            }
            input[type='text'],input[type='password']{
                width: 280px;
                height: 50px;
                font-size: 30px;
                border: 1px solid #979797;
                border-radius: 15px;
            }
            input[type=checkbox], input[type=radio] {
                vertical-align: middle;
                position: relative;
                bottom: 1px;
            }
            input[type=submit]{
                width: 370px;
                height: 80;
                font-size: 32px;
                background-color: #F79B00;
                color: WHITE;
                border-radius: 15px;
            }
        </style>
        
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    </head>
    <body onload="check_login_status();">

        <div id='login_form'>
            <p style='font-size:30px'>10Night Studio 飯店後臺管理系統</p>
            <div id='contain'>
                <img src='images\LWithoutN.png' style='width:180px;height:180px;'>
                <br>
                <span id='message'></span>
                <form id='login' action='login.php' method='POST'>
                    <input type='hidden' name='Feature' value='login'>
                    帳號：<input type='text' name='Account' placeHolder='' value='Engineer.One' required>
                    <br>
                    密碼：<input type='password' name='PassWord' id='PassWord' value='111' required>
                    <br>
                    <label><input type='checkbox' style='zoom:2' id='Remember_Me' name='Remember_Me' value='true'>記住我</label>
                    <br>
                    <input type='submit' value='登入'>
                </form>
            </div>
        </div>

    </body>
</html>

<script>
    function check_login_status(){
        var new_decodedCookie = decodeURIComponent(document.cookie).split(';');
        var ID = new_decodedCookie[0].split('=');
        var Level = new_decodedCookie[1].split('=');
        if(ID[0].replace(/\s/g, '')=='Staff_ID' && ID[1].replace(/\s/g, '')!='' && Level[0].replace(/\s/g, '')=='Staff_Level' && Level[1].replace(/\s/g, '')!=''){
            $.ajax({
                type: "POST",
                url: "login.php",
                dataType: "json",
                data: {
                    Feature: "check_login",
                    ID: ID[1],
                    Level: Level[1],
                },
                success: function(data) {
                    if(data.Success)
                        window.location.href = 'schedule/';
                },
                error: function(jqXHR) {
                    console.log(jqXHR.responseText);
                }
            })
        }
    }

    $(document).ready(function() {
        $("#login").submit(function(){
            var checked = false;
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "login.php",
                data: $("#login").serialize(),
                success: function(data) {
                    if(data.Success){
                        if($("#Remember_Me").prop("checked")){
                            checked = true;
                        }
                        doCookieSetup('Staff_ID', data.Staff_ID, checked);
                        doCookieSetup('Staff_Level', data.Staff_Level, checked);
                        window.location.href = 'schedule/';
                    }
                    else{
                        $("#message").html('<font color="#ff0000">帳號或密碼輸入有誤，請重新輸入</font>');
                        $("#PassWord").val('');
                    }
                },
                error: function(jqXHR) {
                    $("#message").html(jqXHR.responseText);
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });
    });

    function doCookieSetup(name, value, checked) {
        var expires = new Date();
        //有效時間保存 14 天 14*24*60*60*1000 = 1209600000
        if(checked)
            expires.setTime(expires.getTime() + 9999999999);
        else
            expires.setTime(expires.getTime() + 86400000);
        document.cookie = name + "=" + escape(value) + ";expires=" + expires.toGMTString()
    }
</script>