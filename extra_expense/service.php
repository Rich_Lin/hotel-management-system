<?php
header('Content-Type: application/json; charset=UTF-8');
include_once "../mysql_connect.inc.php";
if ($_SERVER['REQUEST_METHOD'] == "POST") {
//////////////////Customer info//////////////////
    if(isset($_POST)){
        switch($_POST['Feature']){
            case 'show_all':
                display($conn);
            break;

            case 'Show_Expense_Info':
                $sql = "SELECT * FROM `extra_expense` WHERE `Extra_Expense_ID`='".$_POST['Extra_Expense_ID']."'";
                $result = mysqli_query($conn,$sql);
                echo json_encode($result->fetch_assoc(),JSON_UNESCAPED_UNICODE);
            break;

            case 'Add_Expense':
                $sql = "INSERT INTO `extra_expense`(`Extra_Expense_Name`, `Extra_Expense_Price`) VALUES ('".$_POST['Extra_Expense_Name']."','".$_POST['Extra_Expense_Price']."')";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
                display($conn);
            break;
            
            case 'Edit_Expense':
                $sql = "UPDATE `extra_expense` SET `Extra_Expense_Name`='".$_POST['Extra_Expense_Name']."',`Extra_Expense_Price`='".$_POST['Extra_Expense_Price']."' WHERE `Extra_Expense_ID`='".$_POST['Extra_Expense_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
                display($conn);
            break;

            case 'delete_Expense':
                $sql = "UPDATE `extra_expense` SET `Extra_Expense_Enable`=0 WHERE `Extra_Expense_ID`='".$_POST['Extra_Expense_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
                display($conn);
            break;
        
        }
    }
}

function display($conn){
    $sql = "SELECT * FROM `extra_expense` WHERE `Extra_Expense_Enable`=1";
    $info_array = array();
    $result = mysqli_query($conn,$sql);
    while($row = $result->fetch_assoc()){
        foreach($row as $key => $value){
            $info_array[$row['Extra_Expense_ID']][$key] = $value;
        }
    }
    sort($info_array);
    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
}
?>