<head>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
    
    <style>
        td{
            /* width: 50%; */
            overflow:hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
            height: 65px;
        }
        .function_btn{
            border-radius:15px;
        }
        .info_table{
            border-radius: 15px;
            margin-top: 30px;
            table-layout: fixed;
            width: 100%;
            font-size:23px;
        }
        .info_td{
            padding-left: 30px;
            border: 0;
        }
        .ui-dialog-titlebar{
            display: none
        }
        .ui-widget, .ui-widget-content, .ui-dialog-content{
            border-radius: 20px;
            /* border-width: 20px; */
            /* background-color: #DADADA;
            border: 1px solid #DADADA; */
        }
        .ui-widget-content {
            /* border-radius: 20px; */
            /* border-width: 20px; */
        }
        .ui-widget-overlay{
            background-color: transparent;
        }
        input{
            height: 50px;
            border-radius: 7px;
        }
    </style>
</head>
<script type="text/javascript" src="../functions.js"></script>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div> 
    <div class='right'>
        <div id='result'></div>
        <center>
            <table border="0" cellpadding="10" style='width:90%'>
                <tr>
                    <td width='50%'><span style='font-size:50px'>加值中心</span></td><td width='50%' style='text-align:right'><input class='function_btn access_level AC_2' style='width:180px;height:50px;background-color:#F79B00;color:white;font-size:20px' type='button' id='Add_Expense' value='新增品項'></td>
                </tr>
            </table>
            <table style='width:90%;border-collapse:collapse;font-size:23px;' id='container' border='0'>
                <!-- <tr style='background-color:#6236FF;color:white'> -->
                <!-- <tr style='background-color:#6236FF;color:white'><td style='padding:15px;'>名稱</td><td style='padding:15px;'>價格</td><td style='width:300px;'></td></tr> -->
                </tr>
            </table>
        </center>
    </div>
</body>

<div id="dialog">
    <center>
        <form id='myform' action='service.php'>
            <input type="hidden" autofocus="true" />
            <table width='85%' style='table-layout: fixed;margin-top:40px'>
                <tr>
                    <td style='font-size:36px;'>新增/編輯品項：</td>
                </tr>
                <tr style='display:none;'>
                    <td>名稱：<input type='text' style='width:150px' name='Extra_Expense_ID' id='Extra_Expense_ID'>
                </tr>
                <tr>
                    <td style='font-size:26px;'>名稱：<input type='text' style='width:456px' name='Extra_Expense_Name' id='Extra_Expense_Name' required>
                </tr>
                <tr>
                    <td style='font-size:26px;'>價錢：<input type='number' style='width:456px' class='edit_column' name='Extra_Expense_Price' id='Extra_Expense_Price' required></td>
                </tr>
                <tr>
                    <td><center><input type='submit' class='function_btn' style='width:435px;height:80px;background-color:#0091FF;font-size:32px;color:WHITE' name='Customer_SSID' value='完成'></center></td>
                </tr>
            </table>
            <input type='hidden' name='Feature' id='Feature'>
        </form>
    </center>
</div>

<script>
    $(document).ready(function() {
        $("#dialog").dialog({
            height: 435,
            width: 745,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#Add_Expense").click(function(){
            $("#Feature").val('Add_Expense');
            $("#Extra_Expense_ID").val('');
            $("#Extra_Expense_Name").val('');
            $("#Extra_Expense_Price").val('');
            $( "#dialog" ).dialog( "open" );
        });

        $("#myform").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#myform").serialize(),
                success: function(data) {
                    $("#result").html('');
                    $("#container").html("<tr style='background-color:#6236FF;color:white'><td class='info_td'>名稱</td><td class='info_td''>價格</td><td style='width:300px;'></td></tr>");
                    var json_array = data;
                    console.log(data);
                    for(i=0;i<json_array.length;i++){
                        if(json_array[i].Extra_Expense_Enable){
                            var row='';
                            var Extra_Expense_ID = json_array[i].Extra_Expense_ID;
                            var Extra_Expense_Name = json_array[i].Extra_Expense_Name;
                            var Extra_Expense_Price = json_array[i].Extra_Expense_Price;
                            row += "<tr><td colspan='3'><table border='1' class='info_table' width='100%'><tr><td class='info_td'>"+Extra_Expense_Name+"</td><td class='info_td'>"+Extra_Expense_Price+"</td><td style='width:300px;text-align:right;border:0'><button class='function_btn access_level AC_3 self_check' type='submit' style='width:100px;height:50px;font-size:24px;margin-right:20px;background-color:WHITE;color:BLACK;' value='"+Extra_Expense_ID+"' onClick='Edit_Expense(this)'>編輯</button><button type='submit' class='function_btn access_level AC_2 self_check' name='delete' style='width:100px;height:50px;font-size:24px;background-color:#E02020;color:WHITE' value='"+Extra_Expense_ID+"' onClick='Delete_Expense(this)'>刪除</button></td></tr></table></td></tr>";
                            $("#container").append(row);
                        }
                    }
                    close_dialog();
                },
                error: function(jqXHR) {
                    alert(jqXHR.responseText);
                }
            })
        })

        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_all",
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html("<tr style='background-color:#6236FF;color:white'><td class='info_td'>名稱</td><td class='info_td''>價格</td><td style='width:300px;'></td></tr>");
                var json_array = data;
                console.log(data);
                for(i=0;i<json_array.length;i++){
                    if(json_array[i].Extra_Expense_Enable){
                        var row='';
                        var Extra_Expense_ID = json_array[i].Extra_Expense_ID;
                        var Extra_Expense_Name = json_array[i].Extra_Expense_Name;
                        var Extra_Expense_Price = json_array[i].Extra_Expense_Price;
                        row += "<tr><td colspan='3'><table border='1' class='info_table' width='100%'><tr><td class='info_td'>"+Extra_Expense_Name+"</td><td class='info_td'>"+Extra_Expense_Price+"</td><td style='width:300px;text-align:right;border:0'><button class='function_btn access_level AC_3 self_check' type='submit' style='width:100px;height:50px;font-size:24px;margin-right:20px;background-color:WHITE;color:BLACK;' value='"+Extra_Expense_ID+"' onClick='Edit_Expense(this)'>編輯</button><button type='submit' class='function_btn access_level AC_2 self_check' name='delete' style='width:100px;height:50px;font-size:24px;background-color:#E02020;color:WHITE' value='"+Extra_Expense_ID+"' onClick='Delete_Expense(this)'>刪除</button></td></tr></table></td></tr>";
                        $("#container").append(row);
                    }
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：請連絡廠商！</font>');
                console.log(jqXHR.responseText);
            }
        })
    });

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        $("#Extra_Expense_ID").val('');
        $("#Extra_Expense_Name").val('');
        $("#Extra_Expense_Price").val('');
        $('#dialog').dialog( "close" );
    }

    function Edit_Expense(Target){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "Show_Expense_Info",
                Extra_Expense_ID: Target.value
            },
            success: function(data) {
                $("#result").html('');
                $("#Feature").val('Edit_Expense');
                $("#Extra_Expense_ID").val(data.Extra_Expense_ID);
                $("#Extra_Expense_Name").val(data.Extra_Expense_Name);
                $("#Extra_Expense_Price").val(data.Extra_Expense_Price);
                
                $( "#dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：請連絡廠商！</font>');
                console.log(jqXHR.responseText);
            }
        })
    }
    
    function Delete_Expense(Extra_Expense_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "delete_Expense",
                Extra_Expense_ID: Extra_Expense_ID.value
            },
            success: function(data) {
                $("#result").html('');
                $("#container").html("<tr style='background-color:#6236FF;color:white'><td class='info_td'>名稱</td><td class='info_td''>價格</td><td style='width:300px;'></td></tr>");
                var json_array = data;
                for(i=0;i<json_array.length;i++){
                    if(json_array[i].Extra_Expense_Enable){
                        var row='';
                        var Extra_Expense_ID = json_array[i].Extra_Expense_ID;
                        var Extra_Expense_Name = json_array[i].Extra_Expense_Name;
                        var Extra_Expense_Price = json_array[i].Extra_Expense_Price;
                        row += "<tr><td colspan='3'><table border='1' class='info_table' width='100%'><tr><td class='info_td'>"+Extra_Expense_Name+"</td><td class='info_td'>"+Extra_Expense_Price+"</td><td style='width:300px;text-align:right;border:0'><button class='function_btn access_level AC_3 self_check' type='submit' style='width:100px;height:50px;font-size:24px;margin-right:20px;background-color:WHITE;color:BLACK;' value='"+Extra_Expense_ID+"' onClick='Edit_Expense(this)'>編輯</button><button type='submit' class='function_btn access_level AC_2 self_check' name='delete' style='width:100px;height:50px;font-size:24px;background-color:#E02020;color:WHITE' value='"+Extra_Expense_ID+"' onClick='Delete_Expense(this)'>刪除</button></td></tr></table></td></tr>";
                        $("#container").append(row);
                    }
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：請連絡廠商！</font>');
                console.log(jqXHR.responseText);
            }
        })
    }

</script>