-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- 主機: 127.0.0.1
-- 產生時間： 2020 年 10 朁E31 日 07:51
-- 伺服器版本: 10.1.37-MariaDB
-- PHP 版本： 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 資料庫： `hotel_sysyem_test`
--

-- --------------------------------------------------------

--
-- 資料表結構 `booking_detail`
--

CREATE TABLE `booking_detail` (
  `Code` bigint(20) NOT NULL,
  `Booking_ID` varchar(255) NOT NULL,
  `Room_Type` varchar(20) NOT NULL,
  `Room_Num` varchar(10) NOT NULL,
  `Actual_CIN_Datetime` datetime NOT NULL,
  `Actual_COUT_Datetime` datetime NOT NULL,
  `Price` int(11) NOT NULL,
  `Room_Status` tinyint(1) NOT NULL,
  `Cleaning_Staff_ID` varchar(20) NOT NULL,
  `Actual_Starteded_Cleaning_Datetime` datetime NOT NULL,
  `Actual_Finished_Cleaning_Datetime` datetime NOT NULL,
  `Enter_Method` tinyint(1) NOT NULL,
  `Keycard_Counter` int(11) NOT NULL,
  `Returned_Keycard` int(11) NOT NULL,
  `Detail_Remark` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `booking_detail`
--

INSERT INTO `booking_detail` (`Code`, `Booking_ID`, `Room_Type`, `Room_Num`, `Actual_CIN_Datetime`, `Actual_COUT_Datetime`, `Price`, `Room_Status`, `Cleaning_Staff_ID`, `Actual_Starteded_Cleaning_Datetime`, `Actual_Finished_Cleaning_Datetime`, `Enter_Method`, `Keycard_Counter`, `Returned_Keycard`, `Detail_Remark`) VALUES
(479, 'B7E4A1F2162F001', '九樓', '901', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(480, 'B7E4A1F2162F001', '八樓', '801', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(481, 'B7E4A1F2162F001', '奇妙套房', '101', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 7568, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(482, 'B7E4A1F2162F001', '酷角客房', '401', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 6784, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(483, 'B7E4A1F218C002', '奇妙套房', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 52840, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(484, 'B7E4A1F218C002', '奇異套房', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 53624, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(485, 'B7E4A1F21A0003', '六樓', '603', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(486, 'B7E4A1F21A0003', '奇妙套房', '103', '2020-10-31 14:27:31', '0000-00-00 00:00:00', 7176, 1, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(487, 'B7E4A1F21A0003', '奇幻套房', '202', '2020-10-31 14:27:25', '2020-10-31 14:27:29', 7176, 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(488, 'B7E4A1F21A0003', '奇異套房', '301', '2020-10-31 14:27:33', '2020-10-31 14:27:36', 6784, 3, 'cs02', '2020-10-31 14:27:38', '0000-00-00 00:00:00', 0, 0, 0, ''),
(489, 'B7E4A1F21B3A004', '九樓', '901', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(490, 'B7E4A1F21B3A004', '九樓', '902', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 7, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(491, 'B7E4A1F21C280005', '奇幻套房', '203', '2020-10-31 14:28:00', '0000-00-00 00:00:00', 0, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(492, 'B7E4A1F21C2B0006', '奇幻套房', '204', '2020-10-31 14:28:00', '0000-00-00 00:00:00', 0, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, ''),
(493, 'B7E4A1F21E320007', '酷角客房', '402', '2020-10-31 14:30:00', '0000-00-00 00:00:00', 0, 4, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- 資料表結構 `booking_index`
--

CREATE TABLE `booking_index` (
  `Booking_ID` varchar(255) NOT NULL,
  `Booking_Date` date NOT NULL,
  `Customer_ID` int(11) NOT NULL,
  `Discount_ID` bigint(20) DEFAULT NULL,
  `CIN_Date` datetime NOT NULL,
  `COUT_Date` datetime NOT NULL,
  `Day_Count` int(11) NOT NULL,
  `People_Count` int(11) NOT NULL,
  `Total_Price` int(11) NOT NULL,
  `Overall_Status` int(11) NOT NULL,
  `Payment_Status` tinyint(1) NOT NULL,
  `Index_Remark` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `booking_index`
--

INSERT INTO `booking_index` (`Booking_ID`, `Booking_Date`, `Customer_ID`, `Discount_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `People_Count`, `Total_Price`, `Overall_Status`, `Payment_Status`, `Index_Remark`) VALUES
('B7E4A1F2162F001', '2020-10-31', 2, 1, '2020-11-01 10:00:00', '2020-11-05 12:00:00', 4, 0, 14352, 0, 0, ''),
('B7E4A1F218C002', '2020-10-31', 3, 2, '2020-10-31 10:00:00', '2020-11-30 12:00:00', 30, 0, 106464, 0, 0, 0xe58299e8a8bbe585a7e5aeb9),
('B7E4A1F21A0003', '2020-10-31', 4, 2, '2020-10-21 10:00:00', '2020-11-04 12:00:00', 4, 0, 21136, 0, 0, 0xe6b8ace8a9a6e794a832e4b98be8a882e596ae),
('B7E4A1F21B3A004', '2020-10-31', 0, 1, '2020-11-11 10:00:00', '2020-11-19 12:00:00', 8, 0, 0, 7, 0, ''),
('B7E4A1F21C280005', '2020-10-31', 1, NULL, '2020-10-31 14:28:00', '2020-10-31 17:28:00', 0, 0, 0, 4, 0, ''),
('B7E4A1F21C2B0006', '2020-10-31', 1, NULL, '2020-10-31 14:28:00', '2020-10-31 17:28:00', 0, 0, 0, 4, 0, ''),
('B7E4A1F21E320007', '2020-10-31', 1, NULL, '2020-10-31 14:30:00', '2020-10-31 15:30:00', 0, 0, 0, 4, 0, '');

-- --------------------------------------------------------

--
-- 資料表結構 `customer`
--

CREATE TABLE `customer` (
  `Customer_ID` bigint(20) NOT NULL,
  `Customer_Name` varchar(255) NOT NULL,
  `Customer_Sex` tinyint(1) NOT NULL,
  `Customer_Phone` varchar(20) NOT NULL,
  `Customer_Email` varchar(200) NOT NULL,
  `Customer_Nationality` varchar(255) NOT NULL,
  `Customer_SSID` varchar(15) NOT NULL,
  `Customer_Passport` varchar(100) NOT NULL,
  `Customer_Remark` blob NOT NULL,
  `Enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `customer`
--

INSERT INTO `customer` (`Customer_ID`, `Customer_Name`, `Customer_Sex`, `Customer_Phone`, `Customer_Email`, `Customer_Nationality`, `Customer_SSID`, `Customer_Passport`, `Customer_Remark`, `Enable`) VALUES
(0, '***', 2, '***', '***', '***', '***', '***', '', 1),
(1, '休息', 2, '***', '***', '***', '***', '***', '', 1),
(2, '秋本嵐', 1, '0922-333-444', 'rich.lin@think-x-tech.com', '中華民國', 'A123456789', '', '', 1),
(3, '測試用', 0, '0900111222', 'faf@gmail.com', '中華民國', 'A223456789', '', '', 1),
(4, '測試用2', 2, '0912345678', 'email@yahoo.com.tw', '日本', 'A111111111', '', '', 1);

-- --------------------------------------------------------

--
-- 資料表結構 `dictionary`
--

CREATE TABLE `dictionary` (
  `Code` bigint(20) NOT NULL,
  `Col_Name` varchar(255) NOT NULL,
  `Translation_TW` varchar(255) NOT NULL,
  `Translation_CN` varchar(255) NOT NULL,
  `Translation_JP` varchar(255) NOT NULL,
  `Translation_KR` varchar(255) NOT NULL,
  `Translation_EN` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `dictionary`
--

INSERT INTO `dictionary` (`Code`, `Col_Name`, `Translation_TW`, `Translation_CN`, `Translation_JP`, `Translation_KR`, `Translation_EN`) VALUES
(1, 'Booking_ID', '訂單編號', '', '', '', ''),
(2, 'Customer_ID', '顧客編號', '', '', '', ''),
(3, 'Room_Type', '房間類型', '', '', '', ''),
(4, 'Room_Num', '房間號碼', '', '', '', ''),
(5, 'CIN_Date', '入住時間', '', '', '', ''),
(6, 'COUT_Date', '退房時間', '', '', '', ''),
(7, 'Staying_Date', '滯留天數', '', '', '', ''),
(8, 'Total_Price', '總金額', '', '', '', ''),
(9, 'Payment_Status', '付款狀態', '', '', '', ''),
(10, 'Room_Status', '房間狀態', '', '', '', ''),
(11, 'Keycard_Counter', '房卡張數', '', '', '', ''),
(12, 'Enter_Method', '入房方式', '', '', '', ''),
(13, 'Customer_Name', '顧客姓名', '', '', '', ''),
(14, 'Customer_Phone', '顧客電話', '', '', '', ''),
(15, 'Customer_Email', '顧客信箱', '', '', '', ''),
(16, 'Customer_Sex', '顧客性別', '', '', '', ''),
(17, 'Customer_SSID', '顧客身分證', '', '', '', ''),
(18, 'Customer_Passport', '顧客護照號碼', '', '', '', ''),
(19, 'Status', '狀態', '', '', '', ''),
(20, 'Addable', '可否新增床位', '', '', '', ''),
(21, 'Room_Counter', '房間數量', '', '', '', ''),
(22, 'Description', '房型描述', '', '', '', ''),
(23, 'Internet_Price', '網路房價', '', '', '', ''),
(24, 'OnSite_Price', '現場房價', '', '', '', ''),
(25, 'Remark', '備註', '', '', '', ''),
(26, 'Booking_Date', '訂房時間', '', '', '', ''),
(27, 'Actual_CIN_Datetime', '實際入住時間', '', '', '', ''),
(28, 'Customer_Nationality', '顧客國籍', '', '', '', ''),
(29, 'Paid', '已付金額', '', '', '', ''),
(30, 'Enable', '啟用', '', '', '', ''),
(31, 'Discount_ID', '房價專案ID', '', '', '', ''),
(32, 'Discount_Name', '專案名稱', '', '', '', ''),
(33, 'Discount_Desc', '專案說明', '', '', '', ''),
(34, 'Cal_Method', '計算方式', '', '', '', ''),
(35, 'Change_Price', '自訂房價', '', '', '', ''),
(36, 'Discount_Start_Date', '起始時間', '', '', '', ''),
(37, 'Discount_End_Date', '結束時間', '', '', '', ''),
(38, 'Fits', '適用房型', '', '', '', ''),
(39, 'Tenant', '容納人數', '', '', '', ''),
(40, 'Discount_Type', '折扣類型', '', '', '', ''),
(41, 'Original_Price', '初始價格', '', '', '', ''),
(42, 'WeekDay_Price', '平日房價', '', '', '', ''),
(43, 'Weekend_Price', '假日房價', '', '', '', ''),
(44, 'Code', '編號', '', '', '', ''),
(45, 'Room_Count', '房間數量', '', '', '', ''),
(46, 'Day_Count', '住房天數', '', '', '', ''),
(47, 'Overall_Status', '總體狀態', '', '', '', ''),
(48, 'Trade_Type', '通路名稱', '', '', '', ''),
(49, 'WeekDay_Days', '平日', '', '', '', ''),
(50, 'Weekend_Days', '周末', '', '', '', ''),
(51, 'Staff_ID', '員工ID', '', '', '', ''),
(52, 'Staff_Name', '員工名稱', '', '', '', ''),
(53, 'Staff_Level', '員工等級', '', '', '', '');

-- --------------------------------------------------------

--
-- 資料表結構 `discount`
--

CREATE TABLE `discount` (
  `Enable` tinyint(1) NOT NULL,
  `Discount_ID` bigint(20) NOT NULL,
  `Discount_Name` varchar(255) NOT NULL,
  `Discount_Desc` blob NOT NULL,
  `Trade_Type` varchar(255) NOT NULL,
  `Discount_Type` tinyint(1) NOT NULL,
  `Cal_Method` float NOT NULL,
  `Change_Price` int(11) NOT NULL,
  `Discount_Start_Date` date NOT NULL,
  `Discount_End_Date` date NOT NULL,
  `Fits` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `discount`
--

INSERT INTO `discount` (`Enable`, `Discount_ID`, `Discount_Name`, `Discount_Desc`, `Trade_Type`, `Discount_Type`, `Cal_Method`, `Change_Price`, `Discount_Start_Date`, `Discount_End_Date`, `Fits`) VALUES
(1, 1, '標準房價', 0xe6a899e6ba96e688bfe583b9e5b088e6a188e8aaaae6988e, '標準房價', 0, 10, 0, '2012-01-01', '2038-12-31', 'Auschwitz/七樓/九樓/八樓/六樓/奇妙套房/奇幻套房/奇異套房/酷角客房'),
(1, 2, '現場房價', 0xe78fbee5a0b4e688bfe583b9e5b088e6a188e8aaaae6988e, '現場房價', 0, 10, 0, '2012-01-01', '2038-12-31', 'Auschwitz/七樓/九樓/八樓/六樓/奇妙套房/奇幻套房/奇異套房/酷角客房'),
(1, 4, '測試用專案名稱(一)', 0xe6b8ace8a9a6e794a8e5b088e6a188e8aaaae6988e3c6272202f3e0d0ae688bfe583b9e689933735e68a98, '測試用專案通路(一)', 0, 7.5, 0, '2019-10-14', '2026-12-31', '奇妙套房/奇幻套房/奇異套房/酷角客房'),
(1, 5, '測試用專案通路(二)', 0xe6b8ace8a9a6e794a8e5b088e6a18828e4ba8c293c6272202f3e0d0ae887aae8a882e688bfe583b9e782ba353030e58583, '測試用專案名稱(二)', 1, 0, 500, '2019-10-14', '2024-12-31', '九樓/八樓/六樓/奇妙套房/奇幻套房/奇異套房/酷角客房');

-- --------------------------------------------------------

--
-- 資料表結構 `duty`
--

CREATE TABLE `duty` (
  `Duty_ID` bigint(20) NOT NULL,
  `Last_Duty_ID` bigint(20) NOT NULL,
  `Last_Duty_Datetime` datetime NOT NULL,
  `Duty_Datetime` datetime NOT NULL,
  `Duty_Staff_ID` varchar(20) NOT NULL,
  `Cash_Amount` int(11) NOT NULL,
  `Credit_Card_Amount` int(11) NOT NULL,
  `Transfer_Amount` int(11) NOT NULL,
  `Else_Amount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `duty`
--

INSERT INTO `duty` (`Duty_ID`, `Last_Duty_ID`, `Last_Duty_Datetime`, `Duty_Datetime`, `Duty_Staff_ID`, `Cash_Amount`, `Credit_Card_Amount`, `Transfer_Amount`, `Else_Amount`) VALUES
(1, 0, '0000-00-00 00:00:00', '2020-03-04 13:57:14', '1', 2653, 2000, 900, 10000);

-- --------------------------------------------------------

--
-- 資料表結構 `expense_order`
--

CREATE TABLE `expense_order` (
  `EO_ID` int(11) NOT NULL,
  `Booking_ID` varchar(255) NOT NULL,
  `Extra_Expense_ID` int(11) NOT NULL,
  `Expense_Count` int(11) NOT NULL,
  `Expense_Total_Price` int(11) NOT NULL,
  `Expense_Remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 資料表結構 `extra_expense`
--

CREATE TABLE `extra_expense` (
  `Extra_Expense_ID` int(11) NOT NULL,
  `Extra_Expense_Name` varchar(20) NOT NULL,
  `Extra_Expense_Price` int(11) NOT NULL,
  `Extra_Expense_Enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `extra_expense`
--

INSERT INTO `extra_expense` (`Extra_Expense_ID`, `Extra_Expense_Name`, `Extra_Expense_Price`, `Extra_Expense_Enable`) VALUES
(1, '測試', 500, 1),
(2, '測試2', 1000, 1);

-- --------------------------------------------------------

--
-- 資料表結構 `hotel_info`
--

CREATE TABLE `hotel_info` (
  `Company_Name` varchar(255) NOT NULL,
  `officeid` varchar(20) NOT NULL,
  `posno` varchar(20) NOT NULL,
  `posid` varchar(20) NOT NULL,
  `Timezone_Set` varchar(50) NOT NULL,
  `QRCode_Key` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `hotel_info`
--

INSERT INTO `hotel_info` (`Company_Name`, `officeid`, `posno`, `posid`, `Timezone_Set`, `QRCode_Key`) VALUES
('阿福炒雞香', '50763214', 'THINKXTECH001', '50763214FB2A83BEE', 'Asia/Taipei', '9AE6927804D3178B2449973A2C3DD8AB');

-- --------------------------------------------------------

--
-- 資料表結構 `invoice_list`
--

CREATE TABLE `invoice_list` (
  `Invoice_Aphabetic_Letter` varchar(2) NOT NULL DEFAULT 'AC',
  `Invoice_Start_Month` int(11) NOT NULL,
  `Invoice_End_Month` int(11) NOT NULL,
  `Invoice_Start` varchar(10) NOT NULL,
  `Invoice_End` varchar(10) NOT NULL,
  `Invoice_Type` varchar(2) NOT NULL,
  `Invoice_Counter` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `invoice_list`
--

INSERT INTO `invoice_list` (`Invoice_Aphabetic_Letter`, `Invoice_Start_Month`, `Invoice_End_Month`, `Invoice_Start`, `Invoice_End`, `Invoice_Type`, `Invoice_Counter`) VALUES
('YE', 1, 2, '01150000', '01150249', '07', 6);

-- --------------------------------------------------------

--
-- 資料表結構 `payment`
--

CREATE TABLE `payment` (
  `Code` bigint(20) NOT NULL,
  `Duty_ID` varchar(255) NOT NULL,
  `Booking_ID` varchar(255) NOT NULL,
  `Payment_Type` varchar(255) NOT NULL,
  `Payment_Method_Num` tinyint(1) NOT NULL,
  `Payment_Method_Name` varchar(20) NOT NULL,
  `Amount` int(11) NOT NULL,
  `Payment_Remark` blob NOT NULL,
  `Staff_ID` varchar(20) NOT NULL,
  `Payment_Datetime` datetime NOT NULL,
  `Invoive_Number` varchar(10) NOT NULL,
  `Random_Number` varchar(4) NOT NULL,
  `Upload_XML` blob NOT NULL,
  `BuyerID` varchar(10) NOT NULL,
  `Cancel_Date` date NOT NULL,
  `Cancel_Time` time NOT NULL,
  `Cancel_Reason` varchar(20) NOT NULL,
  `ReturnTaxDocumentNumber` varchar(60) NOT NULL,
  `Invoice_Status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `payment`
--

INSERT INTO `payment` (`Code`, `Duty_ID`, `Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Payment_Method_Name`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`, `Invoive_Number`, `Random_Number`, `Upload_XML`, `BuyerID`, `Cancel_Date`, `Cancel_Time`, `Cancel_Reason`, `ReturnTaxDocumentNumber`, `Invoice_Status`) VALUES
(56, '', 'B7E4317235E001', '4', 3, '', 0, '', '1', '2020-03-23 00:00:00', '', '', '', '', '0000-00-00', '00:00:00', '', '', ''),
(57, '', 'B7E4A1F2162F001', '4', 3, '', 0, '', '1', '2020-10-31 00:00:00', '', '', '', '', '0000-00-00', '00:00:00', '', '', ''),
(58, '', 'B7E4A1F218C002', '4', 3, '', 0, '', '1', '2020-10-31 00:00:00', '', '', '', '', '0000-00-00', '00:00:00', '', '', ''),
(59, '', 'B7E4A1F21A0003', '4', 3, '', 0, '', '1', '2020-10-31 00:00:00', '', '', '', '', '0000-00-00', '00:00:00', '', '', ''),
(60, '', 'B7E4A1F21B3A004', '4', 3, '', 0, '', '1', '2020-10-31 00:00:00', '', '', '', '', '0000-00-00', '00:00:00', '', '', ''),
(61, '', 'B7E4A1F21C2B0006', '', 0, '', 100, '', '3', '2020-10-31 14:28:43', '', '', '', '', '0000-00-00', '00:00:00', '', '', '');

-- --------------------------------------------------------

--
-- 資料表結構 `room_status`
--

CREATE TABLE `room_status` (
  `Room_Type` varchar(20) NOT NULL,
  `Room_Num` varchar(10) NOT NULL,
  `Addable` tinyint(1) NOT NULL,
  `Remark` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `room_status`
--

INSERT INTO `room_status` (`Room_Type`, `Room_Num`, `Addable`, `Remark`) VALUES
('奇妙套房', '101', 0, ''),
('奇妙套房', '102', 0, ''),
('奇妙套房', '103', 0, ''),
('奇妙套房', '104', 0, ''),
('奇妙套房', '105', 0, ''),
('奇幻套房', '201', 0, ''),
('奇幻套房', '202', 0, ''),
('奇幻套房', '203', 0, ''),
('奇幻套房', '204', 0, ''),
('奇幻套房', '205', 0, ''),
('奇異套房', '301', 0, ''),
('奇異套房', '302', 0, ''),
('奇異套房', '303', 0, ''),
('奇異套房', '304', 0, ''),
('奇異套房', '305', 0, ''),
('酷角客房', '401', 0, ''),
('酷角客房', '402', 0, ''),
('酷角客房', '403', 0, ''),
('酷角客房', '404', 0, ''),
('酷角客房', '405', 0, ''),
('六樓', '600', 0, ''),
('六樓', '601', 0, ''),
('六樓', '602', 0, ''),
('六樓', '603', 0, ''),
('六樓', '604', 0, ''),
('六樓', '605', 0, ''),
('八樓', '801', 0, ''),
('八樓', '802', 0, ''),
('八樓', '803', 0, ''),
('八樓', '804', 0, ''),
('八樓', '805', 0, ''),
('八樓', '806', 0, ''),
('八樓', '807', 0, ''),
('九樓', '901', 0, ''),
('九樓', '902', 0, ''),
('九樓', '903', 0, ''),
('九樓', '904', 0, ''),
('九樓', '905', 0, '');

-- --------------------------------------------------------

--
-- 資料表結構 `room_type`
--

CREATE TABLE `room_type` (
  `Room_Type` varchar(20) NOT NULL,
  `Tenant` int(11) NOT NULL,
  `Rest_Price` int(11) NOT NULL DEFAULT '0',
  `Rest_Per_Hour` int(11) NOT NULL DEFAULT '0',
  `WeekDay_Price` int(11) NOT NULL,
  `Weekend_Price` int(11) NOT NULL,
  `WeekDay_Days` varchar(255) NOT NULL,
  `Weekend_Days` varchar(255) NOT NULL,
  `Description` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `room_type`
--

INSERT INTO `room_type` (`Room_Type`, `Tenant`, `Rest_Price`, `Rest_Per_Hour`, `WeekDay_Price`, `Weekend_Price`, `WeekDay_Days`, `Weekend_Days`, `Description`) VALUES
('九樓', 10, 0, 0, 0, 0, '0/1/2/3/4/5/6', '', ''),
('八樓', 1, 0, 0, 0, 0, '0/1/2/3/4/5/6', '', ''),
('六樓', 5, 0, 0, 0, 0, '0/1/2/3/4/5/6', '', ''),
('奇妙套房', 2, 0, 0, 1892, 1500, '1/2/3/4/5', '0/6', 0xe6b8ace8a9a6e794a8e68f8fe8bfb0),
('奇幻套房', 6, 0, 0, 1892, 1500, '1/2/3/4/5', '0/6', 0xe5a587e5b9bbe5a597e688bf0d0a6166676165676567616567616561670d0a6167616768686172686172687268610d0ae998bfe7a68fe9a699e9a699),
('奇異套房', 4, 0, 0, 1892, 1500, '1/2/3/4/5', '0/6', 0xe5a587e795b0e5a597e688bf),
('酷角客房', 2, 0, 0, 1892, 1500, '1/2/3/4/5', '0/6', 0xe985b7e8a792e5aea2e688bf);

-- --------------------------------------------------------

--
-- 資料表結構 `staff`
--

CREATE TABLE `staff` (
  `Staff_ID` varchar(20) NOT NULL,
  `Staff_Account` varchar(50) NOT NULL,
  `Staff_PWD` varchar(30) NOT NULL,
  `Staff_Name` varchar(255) NOT NULL,
  `Staff_Phone` varchar(20) NOT NULL,
  `Department` varchar(20) NOT NULL,
  `Staff_Title` varchar(10) NOT NULL,
  `Staff_Level` tinyint(1) NOT NULL,
  `Enable` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 資料表的匯出資料 `staff`
--

INSERT INTO `staff` (`Staff_ID`, `Staff_Account`, `Staff_PWD`, `Staff_Name`, `Staff_Phone`, `Department`, `Staff_Title`, `Staff_Level`, `Enable`) VALUES
('0', 'POS_MACHINE', 'POS_MACHINE', '自助機台', '***', '***', '***', 0, 1),
('1', 'Engineer.One', '111', '延吉尼爾一號', '0912-345-678', '開發商', '媽的不想工作', 0, 1),
('2', 'Engineer.Two', 'mickey.huang', '延吉尼爾二號', '0912-345-677', '開發商', '', 0, 1),
('3', 'Big.Boss', 'big.boss1', '飯店頭家', '0912-345-111', '老闆', '多來點客製化吧', 1, 1),
('4', 'Arashi.desu', 'T1nktech', '阿嵐', '0912-345-222', '開發商', '老闆', 1, 1),
('5', 'PartTimeGuy', '000', '某個衰鬼', '0912-345-333', 'Part Time', '打零工的', 3, 1),
('cs01', 'cleaning_staff01', 'cs0', '掃地工(一號)', '0809-449-449', '清潔部', '掃地工', 4, 1),
('cs02', 'cleaning_staff02', 'cs0', '掃地工(二號)', '0809-449-449', '清潔部', '掃地工', 4, 1),
('cs03', 'cleaning_staff03', 'cs0', '掃地工(三號)', '0809-449-449', '清潔部', '掃地工', 4, 1),
('cs04', 'cleaning_staff04', 'cs0', '掃地工(四號)', '0809-449-449', '清潔部', '掃地工', 4, 1),
('cs05', 'cleaning_staff05', 'cs0', '掃地工(五號)', '0809-449-449', '關你屁事', '掃地工', 4, 0);

--
-- 已匯出資料表的索引
--

--
-- 資料表索引 `booking_detail`
--
ALTER TABLE `booking_detail`
  ADD PRIMARY KEY (`Code`);

--
-- 資料表索引 `booking_index`
--
ALTER TABLE `booking_index`
  ADD PRIMARY KEY (`Booking_ID`),
  ADD KEY `Customer_ID` (`Customer_ID`),
  ADD KEY `Discount_ID` (`Discount_ID`);

--
-- 資料表索引 `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`Customer_ID`);

--
-- 資料表索引 `dictionary`
--
ALTER TABLE `dictionary`
  ADD PRIMARY KEY (`Code`);

--
-- 資料表索引 `discount`
--
ALTER TABLE `discount`
  ADD PRIMARY KEY (`Discount_ID`),
  ADD UNIQUE KEY `Discount_Name` (`Discount_Name`);

--
-- 資料表索引 `duty`
--
ALTER TABLE `duty`
  ADD PRIMARY KEY (`Duty_ID`);

--
-- 資料表索引 `expense_order`
--
ALTER TABLE `expense_order`
  ADD PRIMARY KEY (`EO_ID`);

--
-- 資料表索引 `extra_expense`
--
ALTER TABLE `extra_expense`
  ADD PRIMARY KEY (`Extra_Expense_ID`);

--
-- 資料表索引 `hotel_info`
--
ALTER TABLE `hotel_info`
  ADD PRIMARY KEY (`officeid`);

--
-- 資料表索引 `invoice_list`
--
ALTER TABLE `invoice_list`
  ADD PRIMARY KEY (`Invoice_Aphabetic_Letter`);

--
-- 資料表索引 `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`Code`);

--
-- 資料表索引 `room_status`
--
ALTER TABLE `room_status`
  ADD PRIMARY KEY (`Room_Num`);

--
-- 資料表索引 `room_type`
--
ALTER TABLE `room_type`
  ADD PRIMARY KEY (`Room_Type`);

--
-- 資料表索引 `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`Staff_ID`),
  ADD UNIQUE KEY `Staff_Account` (`Staff_Account`);

--
-- 在匯出的資料表使用 AUTO_INCREMENT
--

--
-- 使用資料表 AUTO_INCREMENT `booking_detail`
--
ALTER TABLE `booking_detail`
  MODIFY `Code` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=494;

--
-- 使用資料表 AUTO_INCREMENT `customer`
--
ALTER TABLE `customer`
  MODIFY `Customer_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- 使用資料表 AUTO_INCREMENT `dictionary`
--
ALTER TABLE `dictionary`
  MODIFY `Code` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- 使用資料表 AUTO_INCREMENT `discount`
--
ALTER TABLE `discount`
  MODIFY `Discount_ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用資料表 AUTO_INCREMENT `expense_order`
--
ALTER TABLE `expense_order`
  MODIFY `EO_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用資料表 AUTO_INCREMENT `extra_expense`
--
ALTER TABLE `extra_expense`
  MODIFY `Extra_Expense_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用資料表 AUTO_INCREMENT `payment`
--
ALTER TABLE `payment`
  MODIFY `Code` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
