<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    // error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //////////////////Customer info//////////////////
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'show_all':
                    display_daily_schedule($_POST['Date'],$conn);
                break;

                case 'check_empty_room':
                    $sql = "SELECT * FROM `room_type`,`room_status` WHERE `room_status`.`Room_Type`=`room_type`.`Room_Type` AND `room_status`.`Room_Num`='".$_POST['Room_Num']."'";
                    $result = mysqli_query($conn,$sql);
                    $row = $result->fetch_assoc();
                    $row['Description'] = str_replace(chr(13).chr(10), "<br />",$row['Description']);
                    echo json_encode($row,JSON_UNESCAPED_UNICODE);
                break;

                case 'check_booked_room':
                    $info_array = array();
                    $sql = "SELECT * FROM `customer`,`booking_detail`,`booking_index` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_index`.`Customer_ID`=`customer`.`Customer_ID` AND `booking_detail`.`Booking_ID`='".$_POST['Booking_ID']."' AND `booking_detail`.`Room_Num`='".$_POST['Room_Num']."'";
                    $result = mysqli_query($conn,$sql);
                    $info_array=$result->fetch_assoc();
                    $info_array['Total_Paid']=0;
                    $sql = "SELECT * FROM `booking_index`,`payment` WHERE `booking_index`.`Booking_ID`=`payment`.`Booking_ID` AND `booking_index`.`Booking_ID`='".$_POST['Booking_ID']."'";
                    $result = mysqli_query($conn,$sql);
                    while($row = $result->fetch_assoc()){
                        $info_array['Total_Paid']+=$row['Amount'];
                    }
                    $info_array['Index_Remark'] = str_replace(chr(13).chr(10), "<br />",nl2br($info_array['Index_Remark']));
                    // print_r($info_array);die;
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'Room_Status_Change':
                    date_default_timezone_set('Asia/Taipei');
                    $Now_Datetime = date('Y/m/d H:i:s', time());
                    $Room_Status=0;
                    switch($_POST['Room_Status']){
                        case '0':
                            $Room_Status=1;
                            $sql = "UPDATE `booking_detail` SET `Room_Status`='1',`Actual_CIN_Datetime`='".$Now_Datetime."' WHERE `Booking_ID`='".$_POST['Booking_ID']."' AND `Room_Num`='".$_POST['Room_Num']."'";
                        break;

                        case '1':
                        case '4':
                            $Room_Status=2;
                            $sql = "UPDATE `booking_detail` SET `Room_Status`='2',`Actual_COUT_Datetime`='".$Now_Datetime."' WHERE `Booking_ID`='".$_POST['Booking_ID']."' AND `Room_Num`='".$_POST['Room_Num']."'";
                        break;

                        case '7':
                            $Room_Status=8;
                            $sql = "UPDATE `booking_detail` SET `Room_Status`='8' WHERE `Booking_ID`='".$_POST['Booking_ID']."' AND `Room_Num`='".$_POST['Room_Num']."'";
                        break;
                    }
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                    else
                        echo json_encode(array('Room_Status' => $Room_Status),JSON_UNESCAPED_UNICODE);
                break;

                case 'check_cleaning_room':
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y/m/d', time());
                    $info_array = array();
                    $info_array['Next_Period']='無';
                    $info_array['Index_Remark']='';
                    $sql = "SELECT `booking_index`.`CIN_Date`,`booking_index`.`COUT_Date`,`booking_index`.`Index_Remark` FROM `booking_index`,`booking_detail` WHERE `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND ('$Today_Date' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`)) AND `booking_detail`.`Room_Num`='".$_POST['Room_Num']."' AND `booking_detail`.`Booking_ID`!='".$_POST['Booking_ID']."' AND `booking_detail`.`Room_Status`!=8 ORDER BY `booking_index`.`CIN_Date` DESC";
                    $result = mysqli_query($conn,$sql);
                    $row=$result->fetch_assoc();
                    if(mysqli_num_rows($result)){
                        $info_array['Next_Period']=$row['CIN_Date'] . ' ～ ' . $row['COUT_Date'];
                        $info_array['Index_Remark']=str_replace(chr(13).chr(10), "<br />",nl2br($row['Index_Remark']));
                    }
                    $info_array['Booking_ID']=$_POST['Booking_ID'];
                    $info_array['Room_Num']=$_POST['Room_Num'];
                    $sql = "SELECT `Room_Status` FROM `booking_detail` WHERE `Booking_ID`='".$info_array['Booking_ID']."' AND `Room_Num`='".$info_array['Room_Num']."'";
                    $result = mysqli_query($conn,$sql);
                    $row=$result->fetch_assoc();
                    $info_array['Room_Status']=$row['Room_Status'];

                    $info_array['cleaning_staff']='';
                    $sql = "SELECT * FROM `staff` WHERE `Staff_Level`=4 AND `Enable`=1";
                    $result = mysqli_query($conn,$sql);
                    $count=1;
                    while($row = $result->fetch_assoc()){
                        $info_array['cleaning_staff'] .= $row['Staff_ID'] . "_" . $row['Staff_Name'];
                        if($count<mysqli_num_rows($result))
                            $info_array['cleaning_staff'] .= "(*)";
                        $count++;
                    }
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'change_cleaning_status':
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y-m-d H:i:s', time());
                    $Room_Status = 0;

                    if($_POST['Room_Status_Hidden']=='2'){
                        $Room_Status=3;
                        $sql = "UPDATE `booking_detail` SET `Room_Status`=".$Room_Status.",`Cleaning_Staff_ID`='".$_POST['cleaning_staff']."',`Actual_Starteded_Cleaning_Datetime`='".$Today_Date."' WHERE `Booking_ID`='".$_POST['Booking_ID_Hidden']."' AND `Room_Num`='".$_POST['Room_Num_Hidden']."'";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                    }
                    else{
                        $Room_Status=5;
                        $sql = "UPDATE `booking_detail` SET `Room_Status`=".$Room_Status.",`Actual_Finished_Cleaning_Datetime`='".$Today_Date."' WHERE `Booking_ID`='".$_POST['Booking_ID_Hidden']."' AND `Room_Num`='".$_POST['Room_Num_Hidden']."'";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                    }
                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'show_booking_list':
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y/m/d', time());
                    $info_array = array();
                    $sql = " SELECT * FROM `booking_index`,`booking_detail`,`customer` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_index`.`Customer_ID`=`customer`.`Customer_ID` AND (DATE(`booking_index`.`CIN_Date`)<='$Today_Date' AND DATE(`booking_index`.`COUT_Date`)>='$Today_Date') AND `booking_detail`.`Room_Status`=0";
                    $GROUP_BY = " GROUP BY `booking_index`.`Booking_ID`";
                    $Fuzzy_Search = "";
                    if(!empty($_POST['Fuzzy_Search'])){
                        $Fuzzy_Search = " (`customer`.`Customer_Name` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Phone` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Email` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_SSID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Passport` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_index`.`Booking_ID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_detail`.`Room_Type` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_detail`.`Room_Num` LIKE '%".$_POST['Fuzzy_Search']."%') ";
                        $sql .= " AND " . $Fuzzy_Search . $GROUP_BY;
                    }
                    else
                        $sql .= $GROUP_BY;
                    $result = mysqli_query($conn,$sql);
                    $booking_counter=0;
                    while($row=$result->fetch_assoc()){
                        $info_array[$booking_counter]=$row;
                        if($row['Room_Count']>1){
                            $counter = 1;
                            $rooms = '';
                            $sql = "SELECT `Room_Num` FROM `booking_detail` WHERE `Booking_ID`='".$row['Booking_ID']."'";
                            $room_result = mysqli_query($conn,$sql);
                            while($room_row=$room_result->fetch_assoc()){
                                $rooms .= $room_row['Room_Num'];
                                if($counter<intval($row['Room_Count'])){
                                    $rooms .= ', ';
                                }
                                $counter++;
                            }
                            $info_array[$booking_counter]['Room_Num'] = $rooms;
                        }
                        $booking_counter++;
                    }
                    for($i=0;$i<sizeof($info_array);$i++){
                        $info_array[$i]['Paid'] = 0;
                        $sql = "SELECT `Amount` FROM `payment` WHERE `Booking_ID`='".$info_array[$i]['Booking_ID']."'";
                        $result = mysqli_query($conn,$sql);
                        while($row=$result->fetch_assoc())
                            $info_array[$i]['Paid'] += $row['Amount'];
                    }
                    // print_r($info_array);
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'get_rest_room':
                    $room_array = array();
                    $booking_array = array();
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y/m/d', time());
                    $sql = "SELECT * FROM `room_type`,`room_status` WHERE `room_status`.`Room_Type`=`room_type`.`Room_Type`";
                    $result = mysqli_query($conn,$sql);
                    while($row=$result->fetch_assoc()){
                        $room_array[$row['Room_Type']]['Room_Num'][] = $row['Room_Num'];
                        $room_array[$row['Room_Type']]['default_rest_price'] = $row['Rest_Price'];
                        $room_array[$row['Room_Type']]['rest_per_hour_price'] = $row['Rest_Per_Hour'];
                    }
                    $sql = "SELECT `booking_detail`.`Room_Type`,`booking_detail`.`Room_Num` FROM `booking_index`,`booking_detail` WHERE `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND (('$Today_Date' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`)) OR (`booking_detail`.`Actual_Finished_Cleaning_Datetime`='0000-00-00 00:00:00' AND DATE(`booking_index`.`CIN_Date`)<='$Today_Date')) AND `booking_detail`.`Room_Status`!=8 AND `booking_detail`.`Room_Status`!=5 GROUP BY `booking_detail`.`Room_Num`";
                    $result = mysqli_query($conn,$sql);
                    while($row=$result->fetch_assoc()){
                        $booking_array[$row['Room_Type']]['Room_Num'][] = $row['Room_Num'];
                    }
                    foreach($booking_array as $room_type => $num_array){
                        if(array_key_exists($room_type,$room_array)){
                            $room_array[$room_type]['Room_Num'] = array_diff($room_array[$room_type]['Room_Num'],$num_array['Room_Num']);
                            sort($room_array[$room_type]['Room_Num']);
                        }
                    }
                    echo json_encode($room_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'get_staff':
                    $staff_array = array();
                    $sql = "SELECT `Staff_ID`, `Staff_Name` FROM `staff` WHERE (`Staff_Level` BETWEEN 1 AND 3)  AND `Enable`=1";
                    $result = mysqli_query($conn,$sql);
                    $count=0;
                    while($row=$result->fetch_assoc()){
                        $staff_array[$count]['Staff_ID'] = $row['Staff_ID'];
                        $staff_array[$count]['Staff_Name'] = $row['Staff_Name'];
                        $count++;
                    }
                    echo json_encode($staff_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'create_rest_order':
                    date_default_timezone_set('Asia/Taipei');
                    $Booking_Date = date('Y/m/d', time());
                    $sql = "SELECT COUNT(*) FROM `booking_index` WHERE `Booking_Date`='".$Booking_Date."'";
                    $result = mysqli_query($conn,$sql);
                    $row = $result->fetch_assoc();
                    $Booking_ID = strtoupper('B' . dechex(date('Y', time())) . dechex(date('m', time())) . dechex(date('d', time())) . dechex(date('h', time())) . dechex(date('i', time())) . dechex(date('s', time())) . str_pad($row['COUNT(*)']+1, 4, "0", STR_PAD_LEFT));

                    $Booking_Datetime = date('Y/m/d H:i', time()) . ":00";
                    $COUT_H = date('H', time());
                    $COUT_i = date('i', time());
                    $Payment_Datetime = date('Y-m-d H:i:s', time());

                    $room_type = $_POST['room_type_selector'];
                    $room_num = $_POST['room_num_selector'];
                    $rest_hour = $_POST['rest_hour'];
                    $rest_Total_Price = $_POST['rest_Total_Price'];
                    $staff_ID = $_POST['staff_selector'];
                    
                    $sql = "INSERT INTO `booking_index`(`Booking_ID`, `Booking_Date`, `Customer_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `Total_Price`, `Overall_Status`) VALUES ('".$Booking_ID."','".$Booking_Date."','1','".$Booking_Datetime."','".$Booking_Date . " " . ($COUT_H+$rest_hour) . ":" . $COUT_i . ":00" . "','0','".$rest_Total_Price."','4')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                    $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Room_Num`, `Actual_CIN_Datetime`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_type."','".$room_num."','".$Booking_Datetime."','".$rest_Total_Price."','4')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }

                    foreach($_POST['payment_amount'] as $key=>$value){
                        if($value!='0'){
                            $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Type`, `Payment_Method_Num`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$Booking_ID."','1','".$_POST['payment_method'][$key]."','".$value."','".$_POST['Remark'][$key]."','".$staff_ID."','".$Payment_Datetime."')";
                            if(!mysqli_query($conn,$sql)){
                                echo "This SQL: " . $sql . "<br>";
                                die;
                            }
                        }
                    }

                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'get_available_rooms':
                    get_available_rooms($_POST,$conn);
                break;
            }
        }
    }

    function display_daily_schedule($Date,$conn){
        $Room_Status_Text_Array = array('訂','住','待','清','休','空','清','保','已取消');
        $room_array = array();
        date_default_timezone_set('Asia/Taipei');
        $Today_Date = date('Y/m/d', strtotime($Date));
        $sql = "SELECT * FROM `room_status` ORDER BY `Room_Num` ASC";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]=array();
            $sql = "SELECT * FROM `booking_index`,`booking_detail`,`customer` WHERE `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND (('$Today_Date' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`)) OR (`booking_detail`.`Actual_Finished_Cleaning_Datetime`='0000-00-00 00:00:00' AND DATE(`booking_index`.`CIN_Date`)<='$Today_Date')) AND `booking_detail`.`Room_Num`='".$row['Room_Num']."' AND `customer`.`Customer_ID`=`booking_index`.`Customer_ID` AND `booking_detail`.`Room_Status`!=8 AND `booking_detail`.`Room_Status`!=5 ORDER BY `booking_index`.`CIN_Date`";
            // echo $sql . "\n";
            $info_result = mysqli_query($conn,$sql);
            if(mysqli_num_rows($info_result)){
                if(mysqli_num_rows($info_result)>1){
                    $count=0;
                    $value = array();
                    while($info_row=$info_result->fetch_assoc()){
                        $value[] = $info_row;
                    }
                    switch($value[0]['Room_Status']){
                        case '0':
                        case '1':
                        case '4':
                        case '7':
                            $Booking_ID = $value[0]['Booking_ID'];
                            $Customer_Name = $value[0]['Customer_Name'];
                            $CIN_Date = $value[0]['CIN_Date'];
                            $COUT_Date = $value[0]['COUT_Date'];
                            $Room_Status_Text = $Room_Status_Text_Array[$value[0]['Room_Status']] . " " . $Room_Status_Text_Array[$value[1]['Room_Status']];
                            $Room_Status = $value[0]['Room_Status'] . "_" . $value[1]['Room_Status'];
                        break;

                        
                        case '2':
                        case '3':
                            $Booking_ID = $value[0]['Booking_ID'];
                            $Customer_Name = $value[1]['Customer_Name'];
                            $CIN_Date = $value[1]['CIN_Date'];
                            $COUT_Date = $value[1]['COUT_Date'];
                            $Room_Status_Text = $Room_Status_Text_Array[$value[0]['Room_Status']] . " " . $Room_Status_Text_Array[$value[1]['Room_Status']];
                            $Room_Status = $value[0]['Room_Status'] . "_" . $value[1]['Room_Status'];
                        break;

                        
                        case '5':
                        case '8':
                            $Booking_ID = $value[1]['Booking_ID'];
                            $Customer_Name = $value[1]['Customer_Name'];
                            $CIN_Date = $value[1]['CIN_Date'];
                            $COUT_Date = $value[1]['COUT_Date'];
                            $Room_Status_Text = $Room_Status_Text_Array[$value[1]['Room_Status']];
                            $Room_Status = $value[1]['Room_Status'];
                        break;
                    }
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Booking_ID']=$Booking_ID;
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Customer_Name']=$Customer_Name;
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Order_Period']=date("Y/m/d",strtotime($CIN_Date)) . "～" . date("Y/m/d",strtotime($COUT_Date));
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Room_Status_Text']=$Room_Status_Text;
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Room_Status']="case_" . $Room_Status;
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['First_Status']=$value[0]['Room_Status'];
                }
                else{
                    $info_row=$info_result->fetch_assoc();
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Booking_ID']=$info_row['Booking_ID'];
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Customer_Name']=$info_row['Customer_Name'];
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Order_Period']=date("Y/m/d",strtotime($info_row['CIN_Date'])) . "～" . date("Y/m/d",strtotime($info_row['COUT_Date']));
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Room_Status_Text']="　" . $Room_Status_Text_Array[$info_row['Room_Status']];
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['Room_Status']="case_" . $info_row['Room_Status'];
                    $room_array[substr($row['Room_Num'],0,-2)][$row['Room_Num']]['First_Status']=$info_row['Room_Status'];
                }
            }
        }
        echo json_encode($room_array,JSON_UNESCAPED_UNICODE);
    }

    function get_available_rooms($info,$conn){
        // print_r($info);die;
        $room_array = array();
        $sql = "SELECT * FROM `discount` WHERE `Discount_ID`='".$info['Discount_ID']."'";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        $Discount_row = $row;
        $row = explode("/", $row['Fits']);
        $Day_Count = date_diff(date_create(explode(" ", $info['CIN_Date'])[0]),date_create(explode(" ", $info['COUT_Date'])[0]))->format("%a");
        for($i=0 ; $i < sizeof($row) ; $i++){
            $sql = "SELECT * FROM `room_status`,`room_type` WHERE `room_type`.`Room_Type`='".$row[$i]."' AND `room_status`.`Room_Type`='".$row[$i]."'";
            $result = mysqli_query($conn,$sql);
            while($rows = $result->fetch_assoc()){
                $temp_date = date(explode(" ", $info['CIN_Date'])[0]);
                $room_array[$i]['room_type'] = $rows['Room_Type'];
                $room_array[$i]['room_num'][] = $rows['Room_Num'];
                $room_array[$i]['Tenant'] = $rows['Tenant'];
                $Total_Price = 0;
                for($day=0;$day<$Day_Count;$day++){
                    if(($temp_date <= $Discount_row['Discount_End_Date'] && $temp_date >= $Discount_row['Discount_Start_Date'])){
                        switch($Discount_row['Discount_Type']){
                            case '0':
                                if(strpos($rows['WeekDay_Days'],date_format(date_create($temp_date),'w')) !== false)
                                    $Total_Price+=ceil($rows['WeekDay_Price']*($Discount_row['Cal_Method']/10));
                                else
                                    $Total_Price+=ceil($rows['Weekend_Price']*($Discount_row['Cal_Method']/10));
                            break;

                            case '1':
                                $Total_Price+=$Discount_row['Change_Price'];
                            break;
                        }
                    }
                    else{
                        if(strpos($Room_row['WeekDay_Days'],date_format(date_create($temp_date),'w')) !== false)
                            $Total_Price+=$rows['WeekDay_Price'];
                        else
                            $Total_Price+=$rows['Weekend_Price'];
                    }
                    $temp_date = date_format(date_create($temp_date)->modify('+1 day'), 'Y-m-d');                    
                }
                $room_array[$i]['Total_Price'] = $Total_Price;
                
                // explode("/", $rows['WeekDay_Days']);
                // $rows['WeekDay_Price'];
                // $rows['Weekend_Price'];

            }
            $sql = "SELECT `booking_detail`.`Room_Num` FROM `booking_index`,`booking_detail` WHERE `booking_detail`.`Room_Type`='$row[$i]' AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND (('".$info['CIN_Date']."' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR ('".$info['COUT_Date']."' BETWEEN `booking_index`.`CIN_Date` AND `booking_index`.`COUT_Date`) OR (`booking_index`.`CIN_Date` BETWEEN '".$info['CIN_Date']."' AND '".$info['COUT_Date']."') OR (`booking_index`.`COUT_Date` BETWEEN '".$info['CIN_Date']."' AND '".$info['COUT_Date']."')) AND (`booking_detail`.`Room_Status`!=5 AND `booking_detail`.`Room_Status`!=8)";
            $result = mysqli_query($conn,$sql);
            while($rows = $result->fetch_assoc()){
                unset($room_array[$i]['room_num'][array_search($rows['Room_Num'],$room_array[$i]['room_num'])]);
                sort($room_array[$i]['room_num']);
                if(sizeof($room_array[$i]['room_num'])==0){
                    unset($room_array[$i]);
                    break;
                }
            }
        }
        sort($room_array);
        echo json_encode($room_array,JSON_UNESCAPED_UNICODE);
    }
?>