<style>
table {
  border-collapse: collapse;
}
</style>
<script type="text/javascript" src="../functions.js"></script>
<body onload="includeHTML();">
    <div include-html="../hyper.html"></div><br />
</body>
<?php
include_once "../mysql_connect.inc.php";
if (isset($_POST['Booking_Info'])) {
    $sex = array('女','男','其他');
    $room_status = array('未入住','入住中','退房');
    $payment_status = array('未付款','已付款');
    $enter_method = array('臉部辨識','房卡掃描');
    $or_Statement = "`Booking_ID` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Name` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Phone` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Email` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_SSID` ='".$_POST['Booking_Info']."' OR `customer`.`Customer_Passport` = '".$_POST['Booking_Info']."'";
    $and_Statement = ' `booking_detail`.`Customer_ID` = `customer`.`Customer_ID`';
    $sql = "SELECT * FROM `booking_detail`,`customer` WHERE (".$or_Statement.") AND (".$and_Statement." AND `Display` = 1)";
    $result = mysqli_query($conn,$sql);
    echo "<center><form action='' method='POST'><table border='1'>";
    $row = $result->fetch_array();
    echo "<tr><td colspan='4'><center><h1>".$row['Booking_ID']."</h1></center><p align='right'>".$row['Booking_Date']."</p></td></tr>
            <input type='hidden' name='Booking_ID' value='".$row['Booking_ID']."' readonly></td>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Room_Type'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";  
                echo "
                <td><input type='text' value='".$row['Room_Type']."' readonly></td>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Customer_Name'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['Customer_Name']."' readonly></td>
            </tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Room_Num'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['Room_Num']."' readonly></td>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Customer_Phone'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['Customer_Phone']."' readonly></td>
            </tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'CIN_Date'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['CIN_Date']."' readonly></td>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Customer_Email'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['Customer_Email']."' readonly></td>
            </tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'COUT_Date'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['COUT_Date']."' readonly></td>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Customer_Sex'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$sex[$row['Customer_Sex']]."' readonly></td>
            </tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Staying_Date'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['Staying_Date']."' readonly></td>
                <td>";
                if(!empty($row['Customer_SSID'])){
                    $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Customer_SSID'";
                    $results = mysqli_query($conn,$sql2);
                    $rows = $results->fetch_array();
                    echo $rows[2]."：</td>";
                    echo "
                    <td><input type='text' value='".$row['Customer_SSID']."' readonly></td>";
                }else{
                    $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Customer_Passport'";
                    $results = mysqli_query($conn,$sql2);
                    $rows = $results->fetch_array();
                    echo $rows[2]."：</td>";
                    echo "
                    <td><input type='text' value='".$row['Customer_Passport']."' readonly></td>";
                }
            echo "</tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Total_Price'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                echo "
                <td><input type='text' value='".$row['Total_Price']."' readonly></td>";
                echo "<td colspan='2'></td>
            </tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Payment_Status'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>
                    <td><select name='Payment_Status' disabled>";
                    for($i=0;$i<count($payment_status);$i++){
                        echo "<option value='".$i."'";
                        if($i==$row['Payment_Status'])
                            echo " selected";
                        echo ">".$payment_status[$i]."</option>";
                    }
                echo "</td><td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Room_Status'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>
                    <td><select name='Room_Status' disabled>";
                    for($i=0;$i<count($room_status);$i++){
                        echo "<option value='".$i."'";
                        if($i==2)
                            echo " selected";
                        echo ">".$room_status[$i]."</option>";
                    }
                echo "</td>
            </tr>
            <tr>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Enter_Method'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>
                    <td><select name='Enter_Method' id='Enter_Method' onchange='set_Keycard(this.value)' disabled>";
                    for($i=0;$i<count($enter_method);$i++){
                        echo "<option value='".$i."'";
                        if($i==$row['Enter_Method'])
                            echo " selected";
                        echo ">".$enter_method[$i]."</option>";
                    }
                echo "</td>
                <td>";
                $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Keycard_Counter'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo $rows[2]."：</td>";
                $sql2="SELECT `Tenant` FROM `room_type` WHERE `Room_Type`='".$row['Room_Type']."'";
                $results = mysqli_query($conn,$sql2);
                $rows = $results->fetch_array();
                echo "<td><input type='number' name='Keycard_Counter' id='Keycard_Counter' value='".$row['Keycard_Counter']."' min='1' max='".$rows[0]."' readonly>";
                // echo "<input type='button' id='Keycard_Counter_Button' value='修改' onclick='disable_readonly(this.id)'";
                // if($row['Enter_Method']=='0') echo " style='display:none'";
                // echo ">";
                echo "</td>
            </tr>
                
                <tr>
                    <td colspan='4'>";
                    $sql2="SELECT * FROM `dictionary` WHERE `Col_Name` = 'Remark'";
                    $results = mysqli_query($conn,$sql2);
                    $rows = $results->fetch_array();
                    echo $rows[2]."：<br>
                    <textarea rows='20' cols='100' name='Remark' style='resize: none;'>".str_replace("<br />", chr(13).chr(10),$row['Remark'])."</textarea></td>
                </tr>
                <tr>
                    <td colspan='4'  align='right'><center><input type='submit' name='submit' value='確定退房'></form></center><button name='go_back' onclick='go_back()'>取消</button></td>
                </tr>
            </table>
        </center>";
}

if(isset($_POST['submit'])){
    $result = str_replace("<br />", '',nl2br($_POST["Remark"]));
    $sql = "UPDATE `booking_detail` SET `Room_Status` = 2, `Remark`='".$result."' WHERE `Booking_ID`='".$_POST['Booking_ID']."'";
    if(!mysqli_query($conn,$sql)){
        echo "This SQL: " . $sql . "<br>";
        die;
    }
    echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
}
?>
<script>
    function disable_readonly(id){
        document.getElementsByName (id)[0].removeAttribute('readonly');
    }

    function go_back(){
        window.history.back();
    }

    function set_Keycard(value){
        switch (value){
            case '0':
                document.getElementById("Keycard_Counter_Button").style.display = "none";
                document.getElementById("Keycard_Counter").value = 0;
                break;

            case '1':
                document.getElementById("Keycard_Counter_Button").style.display = "inline";
                document.getElementById("Keycard_Counter").value = 1;
                break;
        }
    }
</script>