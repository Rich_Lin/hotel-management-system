<?php
header('Content-Type: application/json; charset=UTF-8');
include_once "../mysql_connect.inc.php";
if ($_SERVER['REQUEST_METHOD'] == "POST") {
//////////////////Customer info//////////////////
    if(isset($_POST)){
        switch($_POST['Feature']){
            case 'display':
                display($_POST,$conn);
            break;

            case 'change_enable':
                $sql = "UPDATE `discount` SET `Enable`='".$_POST['Enable']."' WHERE `Discount_ID`='".$_POST['Discount_ID']."'";
                if(!mysqli_query($conn,$sql)){
                    echo "This SQL: " . $sql . "<br>";
                    die;
                }
                echo json_encode(array('Status' => 'Success'),JSON_UNESCAPED_UNICODE);
            break;
        
        }
    }
}

function display($Info,$conn){
    $flag=0;
    $sql = "SELECT `Discount_ID`, `Discount_Name`, `Discount_Desc`, `Trade_Type`, `Discount_Type`, `Cal_Method`, `Change_Price`, `Discount_Start_Date`, `Discount_End_Date`, `Fits`, `Enable` FROM `discount`";
    $argument = " WHERE";
    $Trade = "";
    $Date = "";
    if(!empty($Info['Trade'])){
        $Trade = " (`Trade_Type`='".$Info['Trade']."') ";
        $flag++;
    }
    if(!empty($Info['Start']) || !empty($Info['End'])){
        if(empty($Info['End']))
            $Date = " (`Discount_End_Date` >= '" . $Info['Start'] . "')";
        else if(empty($Info['Start']))
            $Date = " (`Discount_Start_Date` <= '" . $Info['End'] . "')";
        else{
            if($Info['Start'] > $Info['End'])
                $Date = " (('".$Info['Start']."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`) OR ('".$Info['End']."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`) OR (`Discount_Start_Date` BETWEEN '".$Info['End']."' AND '".$Info['Start']."') OR (`Discount_End_Date` BETWEEN '".$Info['End']."' AND '".$Info['Start']."'))";
            else
                $Date = " (('".$Info['Start']."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`) OR ('".$Info['End']."' BETWEEN `Discount_Start_Date` AND `Discount_End_Date`) OR (`Discount_Start_Date` BETWEEN '".$Info['Start']."' AND '".$Info['End']."') OR (`Discount_End_Date` BETWEEN '".$Info['Start']."' AND '".$Info['End']."'))";
        }
        $flag++;
    }
    switch($flag){
        case 0:
            $sql = $sql;
        break;

        case 1:
            $sql = $sql . $argument . $Trade . $Date;
        break;

        case 2:
            $sql = $sql . $argument . $Trade . "AND" . $Date;
        break;
    }
    $sql .= ' ORDER BY `Discount_ID`';
    // echo "<br>" . $sql . "<br>";
    // echo $Trade . "<br>";
    // echo $Date . "<br>";
    $info_array = array();
    $result = mysqli_query($conn,$sql);
    while($row = $result->fetch_assoc()){
        foreach($row as $key => $value){
            if($value == 'Discount_Desc')
                $value = str_replace(chr(13).chr(10), "<br />",$value);
            $info_array[$row['Discount_ID']][$key] = $value;
        }
    }
    sort($info_array);
    // echo "<pre>";
    // print_r($info_array);
    // echo "</pre>";die;
    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
}
?>