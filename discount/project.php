<?php
    include_once "../mysql_connect.inc.php";
    header('Content-Type: text/html; charset=UTF-8');
    $Fits = '';
    if(isset($_POST['finish'])){
        $Trade_Type = $_POST["Trade_Type"];
        $Discount_Desc = str_replace('', "<br />",nl2br($_POST["Discount_Desc"]));
        for($i=0;$i<sizeof($_POST['Fits']);$i++)
            if($i+1 != sizeof($_POST['Fits']))
                $Fits .= $_POST['Fits'][$i] . "/";
            else
                $Fits .= $_POST['Fits'][$i];

        if($_POST["Trade_Type"] == 'else')
            $Trade_Type = $_POST["new_trade"];

        if(!empty($_POST['Discount_ID']))
            $sql = "UPDATE `discount` SET `Discount_Name`='".$_POST["Discount_Name"]."',`Discount_Desc`='".$Discount_Desc."',`Trade_Type`='".$Trade_Type."',`Discount_Type`='".$_POST["Discount_Type"]."',`Cal_Method`='".$_POST["Cal_Method"]."',`Change_Price`='".$_POST["Change_Price"]."',`Discount_Start_Date`='".$_POST["Discount_Start_Date"]."',`Discount_End_Date`='".$_POST["Discount_End_Date"]."',`Fits`='".$Fits."' WHERE `Discount_ID`='".$_POST["Discount_ID"]."'";
        else
            $sql = "INSERT INTO `discount`(`Discount_Name`, `Discount_Desc`, `Trade_Type`, `Discount_Type`, `Cal_Method`, `Change_Price`, `Discount_Start_Date`, `Discount_End_Date`, `Fits`) VALUES ('".$_POST["Discount_Name"]."','".$Discount_Desc."','".$Trade_Type."','".$_POST["Discount_Type"]."','".$_POST["Cal_Method"]."','".$_POST["Change_Price"]."','".$_POST["Discount_Start_Date"]."','".$_POST["Discount_End_Date"]."','".$Fits."')";
        
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            die;
        }
    }

    $Trade_Array = array();
    $sql = "SELECT DISTINCT `Trade_Type` FROM `discount`";
    $result = mysqli_query($conn,$sql);
    while($row = $result->fetch_assoc()){
        $Trade_Array[] = $row['Trade_Type'];
    }
?>
<head>
    <script type="text/javascript" src="../functions.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
    <script type="text/javascript" src="../lightpick.js"></script>
    <link rel="stylesheet" type="text/css" href="../lightpick.css">

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

    <style>
        th,td{
            font-size: 23px;
            text-align: left;
            overflow:hidden;
            /* white-space: nowrap; */
            text-overflow: ellipsis;
        }
        .default_tr{
            background-color:#6236FF;
            color:WHITE;
        }
        .default_td{
            padding-top: 10px;
            padding-bottom: 10px;
            padding-left: 20px;
        }
        .function_btn{
            border-radius:15px;
        }
        .project_table{
            border:1px solid #979797;
            margin-top: 25px;
            border-radius:15px
        }
        .project_table:hover{
            border:1px solid #DADADA;
            background-color: #DADADA;
        }
        .modified_select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }
        .modified_select{
            /* border: solid 3px #DADADA; */
            -webkit-appearance: none;
            -moz-appearance: none;
            background: url("/hotel_management_system/images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
            background-size: 18.51px 16.03px;
            background-origin: content-box;
            padding-left: 10px;
            padding-right: 10px;
            background-repeat: no-repeat;
            border-radius:15px;
            height:50px;
        }
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
            cursor: pointer;
        }
        .switch input { 
            opacity: 0;
            width: 0;
            height: 0;
        }
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        input:checked + .slider {
            background-color: #2196F3;
        }
        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        .slider.round {
            border-radius: 34px;
        }
        .slider.round:before {
            border-radius: 50%;
        }
        input{
            font-size:20px;
        }
        .date_div{
            display: block;
            float: left;
        }
        @media screen and (max-width: 1500px) {
            .date_div{
                float: none;
            }
        }
    </style>
</head>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div>
    <div class='right'>
        <center>
            <form action='edit_project.php' method='POST'>
                <p id='result'></p>
                <table style='width: 90%' border='0'>
                    <tr>
                        <td style='font-size:50px' colspan='4'>房價與專案</td>
                    </tr>
                    <tr>
                        <td style='width:20%'>
                            <select id='Trade' class='modified_select' style='width:90%;font-size:20px'>
                                <option value=''>選擇通路</option>
                                <?php
                                for($i=0;$i<sizeof($Trade_Array);$i++)
                                    echo "<option value='".$Trade_Array[$i]."'>".$Trade_Array[$i]."</option>";
                                ?>
                            </select>
                        </td>
                        <td style='text-align:right;font-size:20px;width:40%'>
                            <input type='text' style='width:40%;height:36px;border: solid 3px #DADADA;padding-left: 10px;border-radius:15px;' id='Start' value='' placeholder='點擊選擇起始日'>                            
                            <input type='text' style='width:40%;height:36px;border: solid 3px #DADADA;padding-left: 10px;border-radius:15px;' id='End' value='' placeholder='點擊選擇結束日'>
                        </td>
                        <td style='text-align:right'>
                            <input class='function_btn' style='width:130;height:50;background-color:#0091FF;color:WHITE;margin-left:30px' type='button' id='Rest' value='重設'>
                            <input class='function_btn access_level AC_3' style='width:180;height:50;background-color:#F79B00;color:WHITE;font-size:24px;' type='submit' name='new' value='新增專案'>
                        </td>
                    </tr>
                </table>
                <br>
                <table style='width:90%;border-collapse:collapse' id='discount_list'>
                    <tr class='default_tr'>
                        <td style='width: 5%;background-color:WHITE'></td><td style='width: 15%' class='default_td'>通路</td><td style='width: 17.5%' class='default_td'>專案名稱</td><td style='width: 20%' class='default_td'>專案說明</td><td class='default_td'>優惠日期</td><td style='width: 15%' class='default_td'></td>
                    </tr>
                </table>
            </form>
        </center>
    </div>
</body>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> <!-- 引入 jQuery -->
<script>
    var picker = new Lightpick({
        field: document.getElementById('Start'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('End').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Feature: 'display',
                    Trade: $("#Trade").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#discount_list .new").remove();
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Enable = "";
                        if(json_array[i].Enable == '1')
                            Enable = " checked";
                        var Discount_ID = json_array[i].Discount_ID;
                        var Discount_Name = json_array[i].Discount_Name;
                        var Discount_Desc = json_array[i].Discount_Desc;
                        var Trade_Type = json_array[i].Trade_Type;
                        var Discount_Start_Date = json_array[i].Discount_Start_Date;
                        var Discount_End_Date = json_array[i].Discount_End_Date;
                        var row = "<tr class='new'><td style='width: 5%'><label class='switch'><input class='access_level AC_2' type='checkbox' name='Enable' " + Enable + " value='" + Discount_ID + "' onChange='project_enable(this)'><span class='slider round access_level AC_2'></span><input type='checkbox' style='width:26px;height:26px;' disabled></td><td colspan='5'><table class='project_table' border='0' width='100%'><tr height='100px'><td style='width: 15.5%' class='default_td'>"+Trade_Type+"</td><td style='width: 19%' class='default_td'>"+Discount_Name+"</td><td style='width: 21.5%' class='default_td'>"+Discount_Desc+"</td><td class='default_td'>"+Discount_Start_Date+" ～ "+Discount_End_Date+"</td><td style='width: 20%' class='default_td'><button class='function_btn access_level AC_3' name='edit' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:WHITE;color:BLACK'>編輯</button><button class='function_btn access_level AC_2' name='Delete' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:#E02020;color:WHITE'>刪除</button></td></tr></table></td></tr>";
                        $("#discount_list").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    var picker2 = new Lightpick({
        field: document.getElementById('End'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            var tmp;
            if(document.getElementById('Start').value != '' && document.getElementById('Start').value > document.getElementById('End').value){
                tmp = document.getElementById('End').value;
                document.getElementById('End').value = document.getElementById('Start').value;
                document.getElementById('Start').value = tmp;
            }
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Feature: 'display',
                    Trade: $("#Trade").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#discount_list .new").remove();
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Enable = "";
                        if(json_array[i].Enable == '1')
                            Enable = " checked";
                        var Discount_ID = json_array[i].Discount_ID;
                        var Discount_Name = json_array[i].Discount_Name;
                        var Discount_Desc = json_array[i].Discount_Desc;
                        var Trade_Type = json_array[i].Trade_Type;
                        var Discount_Start_Date = json_array[i].Discount_Start_Date;
                        var Discount_End_Date = json_array[i].Discount_End_Date;
                        var row = "<tr class='new'><td style='width: 5%'><label class='switch'><input class='access_level AC_2' type='checkbox' name='Enable' " + Enable + " value='" + Discount_ID + "' onChange='project_enable(this)'><span class='slider round access_level AC_2'></span><input type='checkbox' style='width:26px;height:26px;' disabled></td><td colspan='5'><table class='project_table' border='0' width='100%'><tr height='100px'><td style='width: 15.5%' class='default_td'>"+Trade_Type+"</td><td style='width: 19%' class='default_td'>"+Discount_Name+"</td><td style='width: 21.5%' class='default_td'>"+Discount_Desc+"</td><td class='default_td'>"+Discount_Start_Date+" ～ "+Discount_End_Date+"</td><td style='width: 20%' class='default_td'><button class='function_btn access_level AC_3' name='edit' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:WHITE;color:BLACK;'>編輯</button><button class='function_btn access_level AC_2' name='Delete' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:#E02020;color:WHITE'>刪除</button></td></tr></table></td></tr>";
                        $("#discount_list").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
            // Day count is disabled at line 719
        }
    });

    $(document).ready(function() {

        $("#Trade").change(function() {
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Feature: 'display',
                    Trade: $("#Trade").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#discount_list .new").remove();
                    var json_array = data;
                    for(i=0;i<json_array.length;i++){
                        var Enable = "";
                        if(json_array[i].Enable == '1')
                            Enable = " checked";
                        var Discount_ID = json_array[i].Discount_ID;
                        var Discount_Name = json_array[i].Discount_Name;
                        var Discount_Desc = json_array[i].Discount_Desc;
                        var Trade_Type = json_array[i].Trade_Type;
                        var Discount_Start_Date = json_array[i].Discount_Start_Date;
                        var Discount_End_Date = json_array[i].Discount_End_Date;
                        var row = "<tr class='new'><td style='width: 5%'><label class='switch'><input class='access_level AC_2' type='checkbox' name='Enable' " + Enable + " value='" + Discount_ID + "' onChange='project_enable(this)'><span class='slider round access_level AC_2'></span><input type='checkbox' style='width:26px;height:26px;' disabled></td><td colspan='5'><table class='project_table' border='0' width='100%'><tr height='100px'><td style='width: 15.5%' class='default_td'>"+Trade_Type+"</td><td style='width: 19%' class='default_td'>"+Discount_Name+"</td><td style='width: 21.5%' class='default_td'>"+Discount_Desc+"</td><td class='default_td'>"+Discount_Start_Date+" ～ "+Discount_End_Date+"</td><td style='width: 20%' class='default_td'><button class='function_btn access_level AC_3' name='edit' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:WHITE;color:BLACK;'>編輯</button><button class='function_btn access_level AC_2' name='Delete' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:#E02020;color:WHITE'>刪除</button></td></tr></table></td></tr>";
                        $("#discount_list").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
        });

        $("#Rest").click(function(){
            $("#Trade").prop('selectedIndex',0);
            $("#Start").val('');
            $("#End").val('');
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Feature: 'display',
                    Trade: $("#Trade").val(),
                    Start: $("#Start").val(),
                    End: $("#End").val()
                },
                success: function(data) {
                    $("#discount_list .new").remove();
                    for(i=0;i<data.length;i++){
                        var Enable = "";
                        if(data[i].Enable == '1')
                            Enable = " checked";
                        var Discount_ID = data[i].Discount_ID;
                        var Discount_Name = data[i].Discount_Name;
                        var Discount_Desc = data[i].Discount_Desc;
                        var Trade_Type = data[i].Trade_Type;
                        var Discount_Start_Date = data[i].Discount_Start_Date;
                        var Discount_End_Date = data[i].Discount_End_Date;
                        var row = "<tr class='new'><td style='width: 5%'><label class='switch'><input class='access_level AC_2' type='checkbox' name='Enable' " + Enable + " value='" + Discount_ID + "' onChange='project_enable(this)'><span class='slider round access_level AC_2'></span><input type='checkbox' style='width:26px;height:26px;' disabled></td><td colspan='5'><table class='project_table' border='0' width='100%'><tr height='100px'><td style='width: 15.5%' class='default_td'>"+Trade_Type+"</td><td style='width: 19%' class='default_td'>"+Discount_Name+"</td><td style='width: 21.5%' class='default_td'>"+Discount_Desc+"</td><td class='default_td'>"+Discount_Start_Date+" ～ "+Discount_End_Date+"</td><td style='width: 20%' class='default_td'><button class='function_btn access_level AC_3' name='edit' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:WHITE;color:BLACK'>編輯</button><button class='function_btn access_level AC_2' name='Delete' value='"+Discount_ID+"' style='font-size:26px;margin:10px;width:100;height:50px;background-color:#E02020;color:WHITE'>刪除</button></td></tr></table></td></tr>";
                        $("#discount_list").append(row);
                    }
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
        })

        $.ajax({
            type: "POST",
            url: "sql_search.php",
            dataType: "json",
            data: {
                Feature: 'display',
                Trade: $("#Trade").val(),
                Start: $("#Start").val(),
                End: $("#End").val()
            },
            success: function(data) {
                var json_array = data;
                for(i=0;i<json_array.length;i++){
                    var Enable = "";
                    if(json_array[i].Enable == '1')
                        Enable = " checked";
                    var Discount_ID = json_array[i].Discount_ID;
                    var Discount_Name = json_array[i].Discount_Name;
                    var Discount_Desc = json_array[i].Discount_Desc;
                    var Trade_Type = json_array[i].Trade_Type;
                    var Discount_Start_Date = json_array[i].Discount_Start_Date;
                    var Discount_End_Date = json_array[i].Discount_End_Date;
                    var row = "<tr class='new'><td style='width: 5%'><label class='switch'><input class='access_level AC_2' type='checkbox' name='Enable' " + Enable + " value='" + Discount_ID + "' onChange='project_enable(this)'><span class='slider round access_level AC_2'></span><input type='checkbox' style='width:26px;height:26px;' disabled></td><td colspan='5'><table class='project_table' border='0' width='100%'><tr height='100px'><td style='width: 15.5%' class='default_td'>"+Trade_Type+"</td><td style='width: 19%' class='default_td'>"+Discount_Name+"</td><td style='width: 21.5%' class='default_td'>"+Discount_Desc+"</td><td class='default_td' style='text-align:center'><div class='date_div'>"+Discount_Start_Date+"</div><div class='date_div'> ～ </div><div class='date_div'>"+Discount_End_Date+"</div></td><td class='default_td'><button class='function_btn access_level AC_3' name='edit' value='"+Discount_ID+"' style='font-size:20px;width:100;height:50px;background-color:WHITE;color:BLACK;'>編輯</button><button class='function_btn access_level AC_2' name='Delete' value='"+Discount_ID+"' style='font-size:20px;margin:10px;width:100;height:50px;background-color:#E02020;color:WHITE'>刪除</button></td></tr></table></td></tr>";
                    $("#discount_list").append(row);
                }
            },
            error: function(jqXHR) {
                $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
            }
        })
    });

    function project_enable(target){
        if(target.checked){
            $.ajax({
                    type: "POST",
                    url: "sql_search.php",
                    dataType: "json",
                    data: {
                        Feature: 'change_enable',
                        Discount_ID: target.value,
                        Enable: '1'
                    },
                    success: function(data) {
                    },
                    error: function(jqXHR) {
                        $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                    }
                })
        }
        else{
            $.ajax({
                type: "POST",
                url: "sql_search.php",
                dataType: "json",
                data: {
                    Feature: 'change_enable',
                    Discount_ID: target.value,
                    Enable: '0'
                },
                success: function(data) {
                },
                error: function(jqXHR) {
                    $("#result").html('<font color="#ff0000">發生錯誤：' + jqXHR.responseText + '</font>');
                }
            })
        }
    }

</script>


<!-- 程式由嵐叔獨立製作:D，若有需要歡迎FaceBook查詢：「嵐叔&貓貓的日常」 -->