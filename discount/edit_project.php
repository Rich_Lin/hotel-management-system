<?php
    include_once "../mysql_connect.inc.php";
    error_reporting(0);

    $Discount_ID = '';
    $Trade_Type = '';
    $Discount_Name = '';
    $Discount_Start_Date = '';
    $Discount_End_Date = '';
    $Discount_Desc = '';
    $Discount_Type = '';
    $Cal_Method = '';
    $Change_Price = '';
    $Fits = '';

    $Room_Type = array();

    $sql = "SELECT `Room_Type` FROM `room_type`";
    $result = mysqli_query($conn,$sql);
    while($row = $result->fetch_assoc()){
        $Room_Type[] = $row['Room_Type'];
    }

    if(isset($_POST['edit'])){
        $sql = "SELECT * FROM `discount` WHERE `Discount_ID`='".$_POST['edit']."'";
        $result = mysqli_query($conn,$sql);
        $row = $result->fetch_assoc();
        
        $Discount_ID = $row['Discount_ID'];
        $Trade_Type = $row['Trade_Type'];
        $Discount_Name = $row['Discount_Name'];
        $Discount_Start_Date = $row['Discount_Start_Date'];
        $Discount_End_Date = $row['Discount_End_Date'];
        $Discount_Desc = str_replace("<br />", '',$row['Discount_Desc']);
        $Discount_Type = $row['Discount_Type'];
        $Cal_Method = $row['Cal_Method'];
        $Change_Price = $row['Change_Price'];
        $Fits = explode("/",$row['Fits']);
    }
?>
<head>
    <style>
        * {
            font-family: Microsoft JhengHei;
            font-size: 26px;
        }
        .input_field{
            border-radius:15px;
            font-size: 26px;
            height: 50px;
        }
        input[type=checkbox], input[type=radio] {
            vertical-align: middle;
            position: relative;
            bottom: 1px;
        }
        tr,td{
            padding-top:20px;
        }
        .modified_select:-moz-focusring {
            color: transparent;
            text-shadow: 0 0 0 #000;
        }
        .modified_select{
            /* border: solid 3px #DADADA; */
            -webkit-appearance: none;
            -moz-appearance: none;
            background: url("/hotel_management_system/images/dropdown-arrow-icon.png") no-repeat scroll right center transparent;
            background-size: 18.51px 16.03px;
            background-origin: content-box;
            padding-left: 10px;
            padding-right: 10px;
            background-repeat: no-repeat;
            border-radius:15px;
            height:50px;
        }
        .styled_select {
            display: block;
            width: 288px;
            /* height: 50px; */
            border-radius: 5px;
            border: 1px solid #666;
            overflow: hidden;
            position: relative;
        }
        .arrow {
            background: url('/hotel_management_system/images/dropdown-arrow-icon.png') no-repeat right;
            -moz-background-size:25px 25px;
            -webkit-background-size:25px 25px;
            -o-background-size:25px 25px;
            background-size:25px 25px;
            display: block; 
            width: 40px; 
            height: 40px;
            position: absolute;
            right: 10px;
            top: 0px;
            pointer-events: none;
        }
        .styled_select select {
            padding-right: 40px;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
            border: 0;
            cursor: pointer;
        }
        select {
            /* background: transparent; */
            font-size: 26px;
        }
        select:focus {
            outline: none;			/* kill the orange outline on select but doesn't work*/
            -webkit-appearance: none;
        }
    </style>
    <script type="text/javascript" src="../functions.js"></script>
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
</head>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div> 
    <div class='right'>
        <center><form action='project.php' method='POST'>
            <table border='0' style='border: 1px solid #979797;border-radius:15px;padding:40px;width:90%'>
            <?php
            echo "
                <input type='hidden' name='Discount_ID' value='".$Discount_ID."'>
                <tr><td style='font-size:36px' colspan='2'>新增/編輯專案</td></tr>
                <tr>
                    <td style='text-align:right;vertical-align:top'>通路：</td>
                    <td>
                            <select style='width:288px' name='Trade_Type' class='modified_select' onchange='check_Trade(this.value)' required>
                                <option value=''>選擇通路</option>";
                                
                                    $sql = "SELECT DISTINCT `Trade_Type` FROM `discount`";
                                    $result = mysqli_query($conn,$sql);
                                    while($row = $result->fetch_assoc()){
                                        echo "<option value='".$row['Trade_Type']."'"; if($row['Trade_Type']==$Trade_Type) echo " selected"; echo ">".$row['Trade_Type']."</option>";
                                    }
                                echo "
                                <option value='else'>其它</option>
                            </select>
                            <input class='input_field' style='display:none;margin-top:10px' type='text' name='new_trade' id='new_trade' placeholder='請輸入通路名稱' maxlength='8'>
                    </td>
                </tr>
                <tr>
                    <td style='text-align:right'>專案名稱：</td>
                    <td><input class='input_field' type='text' name='Discount_Name' value='".$Discount_Name."' maxlength='10' required></td>
                </tr>
                <tr>
                    <td style='text-align:right'>專案期間：</td>
                    <td><input class='input_field' type='text' name='Discount_Start_Date' id='Discount_Start_Date' value='".$Discount_Start_Date."' placeholder='YY/MM/DD'> ～ <input class='input_field' type='text' name='Discount_End_Date' id='Discount_End_Date' value='".$Discount_End_Date."' placeholder='YY/MM/DD'></td>
                </tr>
                <tr>
                    <td style='text-align:right;vertical-align:top'>專案內容：</td>
                    <td><textarea rows='7' cols='40' style='resize:none;border-radius:15px;font-size:26px' name='Discount_Desc' rows>".$Discount_Desc."</textarea></td>
                </tr>
                <tr>
                    <td style='text-align:right'>適用房型：</td>
                    <td><label><input type='checkbox' id='check_all' style='zoom:2' onClick='toggle(this)'>全選</label></td>
                </tr>
                <tr>
                    <td></td><td style='padding:0px'>";
                    for($i=0;$i<sizeof($Room_Type);$i++){
                        echo "<label><input type='checkbox' class='Room_Types' style='zoom:2' name='Fits[]' value='".$Room_Type[$i]."'"; if(in_array($Room_Type[$i],$Fits)) echo " checked"; echo ">".$Room_Type[$i]."</label>&nbsp;&nbsp;";
                    }
                echo "</td></tr>
                <tr>
                    <td style='text-align:right;vertical-align:top'>專案價格：</td>
                    <td>
                        <label><input type='radio' style='width:30px;height:30px' name='Discount_Type' value='0'"; if(!$Discount_Type) echo " checked"; echo ">標準房價</label> x <input style='width:60px' class='input_field' type='number' step='0.1' max='10' min='0'  value='".$Cal_Method."' name='Cal_Method'>折
                        <br>
                        <label><input type='radio' style='width:30px;height:30px' name='Discount_Type' value='1'"; if($Discount_Type) echo " checked"; echo ">自訂房價：</label><input class='input_field' type='number' style='width:160px' name='Change_Price' min='0' placeholder='自訂房價' value='".$Change_Price."'>
                    </td>
                </tr>
                <tr>
                    <td colspan='2' style='text-align:center'><input style='width:435px;height:80px;border-radius:15px;background-color:#0091FF;color:WHITE' type='submit' name='finish' value='完成'></td>
                </tr>
            </table>
        </form></center>";
        ?>
    </div>
    <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
</body>
<link rel="stylesheet" type="text/css" href="../lightpick.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script type="text/javascript" src="../lightpick.js"></script>
<script>
    var picker = new Lightpick({
        field: document.getElementById('Discount_Start_Date'),
        secondField: document.getElementById('Discount_End_Date'),
        singleDate: false,
        minDate: moment().startOf('day'),
        repick: true
        //Day count is disabled at line 719
    });

    function check_Trade(value){
        if(value=='else'){
            document.getElementById("new_trade").style.display = "block";
            document.getElementById("new_trade").required  = true;
        }
        else{
            document.getElementById("new_trade").style.display = "none";
            document.getElementById("new_trade").value = "";
            document.getElementById("new_trade").required  = false;
        }
    }

    function toggle(source){
        var checkboxes = document.getElementsByClassName("Room_Types");
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = source.checked;
        }
    }
</script>

<?php
if(isset($_POST['Delete'])){
    $sql = "DELETE FROM `discount` WHERE `Discount_ID` = '".$_POST['Delete']."'";
    if(!mysqli_query($conn,$sql)){
        echo "This SQL: " . $sql . "<br>";
        die;
    }
    echo '<meta http-equiv=REFRESH CONTENT=0;url=project.php>';
}
?>