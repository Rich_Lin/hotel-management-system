<?php
    include_once "../mysql_connect.inc.php";
    // echo "<pre>";
    // print_r($price_Array);
    // echo "</pre>";die;
?>
<head>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

    <style>
        td{
            /* width: 50%; */
            text-align: center;
            overflow:hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
        .function_btn{
            border-radius:15px;
        }
        .info_table{
            border-radius: 15px;
            margin: 15px 0px;
            table-layout: fixed;
            font-size: 23px;
            width: 100%;
        }
        .info_td{
            /* padding-left: 15px; */
            border: 0;
        }
        .rooms{
            overflow: hidden;
            white-space: nowrap;
            text-overflow: ellipsis;
        }
    </style>
</head>
<script type="text/javascript" src="../functions.js"></script>
<body onload="includeHTML();">
    <div class='navbar-div' include-html="../navbar.html"></div>
    <div class='for_hyper left' include-html="../hyper.html"></div> 
    <div class='right'>
        <center><form action='edit_price.php' method='POST'>
            <table border="0" cellpadding="10" style='width:90%'>
                <tr>
                    <td width='50%'><span style='font-size:50px'>房型編輯</span></td><td width='50%' style='text-align:right'><input class='function_btn access_level AC_3' name="new_room" style='width:180px;height:50px;background-color:#F79B00;color:white;font-size:20px' type=submit value='新增房型'></td>
                </tr>
            </table>
            <table style='width:90%;border-collapse:collapse;table-layout:fixed;' border='0'>
                <tr style='background-color:#6236FF;color:white'>
                    <td style='padding:15px;font-size:23px;width:12.5%;min-width:140px'>名稱</td><td style='padding:15px;width:45%;font-size:23px;'>房號</td><td style='padding:15px;width:125px;font-size:23px;'>平日房價</td><td style='padding:15px;width:125px;font-size:23px;'>假日房價</td><td style='width:260px;'> </td>
                </tr>

                <?php
                    $sql = "SELECT `room_type`.`Room_Type`,`room_type`.`WeekDay_Price`,`room_type`.`Weekend_Price`,`room_status`.`Room_Num` FROM `room_type`,`room_status` WHERE `room_status`.`Room_Type`=`room_type`.`Room_Type`";
                    $result = mysqli_query($conn,$sql);
                    $price_Array = array();
                    $room_Array = array();
                    $type = '';
                    while($row = $result->fetch_assoc()){
                        $room_Array[$row['Room_Type']][] = $row['Room_Num'];
                        $price_Array[$row['Room_Type']]['WeekDay_Price'] = $row['WeekDay_Price'];
                        $price_Array[$row['Room_Type']]['Weekend_Price'] = $row['Weekend_Price'];
                    }
                    foreach($room_Array as $key => $rooms_Array){
                        echo "<tr><td colspan='5'><table border='1' class='info_table'><tr><td class='info_td' style='width:15%;min-width:140px'>".$key."</td><td class='info_td' style='padding:15px;width:40%;text-align:left'>";for($i=0;$i<sizeof($rooms_Array);$i++) echo $rooms_Array[$i]." "; echo "</td><td class='info_td' style='text-align:right;padding-right:10px;width:125px'>".$price_Array[$key]['WeekDay_Price']."</td><td class='info_td rooms' style='text-align:right;padding-right:10px;width:125px'>".$price_Array[$key]['Weekend_Price']."</td><td style='text-align:right;width:260px;border:0'><button class='function_btn access_level AC_3' type='submit' name='edit_room' style='width:100px;height:50px;font-size:20px;margin-right:20px;;background-color:WHITE;color:BLACK' value='".$key."'>編輯</button><button type='submit' class='function_btn access_level AC_2' name='delete' style='width:100px;height:50px;font-size:20px;background-color:#E02020;color:WHITE' value='".$key."'>刪除</button></td></tr></table></td></tr>";
                    }
                ?>
            </table></form>
        </center>
    </div>
    <!-- 此程式由嵐叔獨力完成，若有相關需求，歡迎聯絡Facebook「嵐叔＆貓貓的日常」粉絲專頁 -->
</body>

<script>
    function add_num(){
        var rows = document.getElementById('add_room').getElementsByTagName('span').length;
        var table = document.getElementById('add_room');
        var row = table.insertRow(rows);
        var cell = row.insertCell(0);
        cell.innerHTML = "<span>" + paddingLeft( rows+1 , 2) + ". <input type='text' name='Room_Num["+rows+"]' required></span>";
    }
    function paddingLeft(str,lenght){
        if(str.toString().length >= lenght)
            return str;
        else
            return paddingLeft("0" +str,lenght);
    }
    function autogrow(textarea){
        var adjustedHeight=textarea.clientHeight;
        adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
        if (adjustedHeight>textarea.clientHeight){
            textarea.style.height=adjustedHeight+'px';
        }
    }
</script>
