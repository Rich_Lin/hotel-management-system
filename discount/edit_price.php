<?php
    include_once "../mysql_connect.inc.php";

    $Room_Type = '';
    $Tenant = 0;
    $WeekDay_Price = 0;
    $Weekend_Price = 0;
    $WeekDay_array = array();
    $WeekDay_array[0] = '';
    $Weekend_array = array();
    $Weekend_array[0] = '';
    $Room_Num = array();
    $Room_Num[0] = '';
    $Rest_Price = 0;
    $Rest_Per_Hour = 0;
    $Description = '';
    $counter = 0;

    if(isset($_POST['edit_room'])){
        $Room_Type = $_POST['edit_room'];
        $sql = "SELECT * FROM `room_type`,`room_status` WHERE `room_status`.`Room_Type`=`room_type`.`Room_Type` AND `room_type`.`Room_Type`='".$Room_Type."'";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $Tenant = $row['Tenant'];
            $WeekDay_Price = $row['WeekDay_Price'];
            $Weekend_Price = $row['Weekend_Price'];
            $WeekDay_temp = $row['WeekDay_Days'];
            $Weekend_temp = $row['Weekend_Days'];
            $Room_Num[$counter] = $row['Room_Num'];
            $Description = $row['Description'];
            $Rest_Price = $row['Rest_Price'];
            $Rest_Per_Hour = $row['Rest_Per_Hour'];
            $counter++;
        }
        $counter = 0;



        if(!empty($WeekDay_temp) && strpos($WeekDay_temp, '/') !== false){
            $WeekDay_array = explode("/",$WeekDay_temp);
        }else{
            $WeekDay_array[0] = $WeekDay_temp;
        }

        if(!empty($Weekend_temp) && strpos($Weekend_temp, '/') !== false){
            $Weekend_array = explode("/",$Weekend_temp);
        }else{
            $Weekend_array[0] = $Weekend_temp;
        }

        if(empty($Room_Num)){
            $Room_Num[0] = '';
        }
    }
?>

<html>
    <head>
        <script type="text/javascript" src="../functions.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            input[type=text],textarea{
                border-radius:10px;
            }
            input[type=checkbox]{
                zoom:2;
            }
            input[type=checkbox], input[type=radio] {
                vertical-align: middle;
                position: relative;
                /* bottom: 1px; */
            }
            .switch {
                position: relative;
                display: inline-block;
                width: 35px;
                height: 35px;
            }
            .slider {
                position: absolute;
                cursor: pointer;
                top: 0;
                left: 0;
                right: 0;
                bottom: 0;
                background-color: #ccc;
                -webkit-transition: .4s;
                transition: .4s;
            }
            input:checked + .slider {
                background-color: #2196F3;
                color:WHITE
            }
            td{
                padding-top:7.5px
            }
        </style>
    </head>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <?php
                echo "
                <center style='padding-top:150px'>
                    <form method='POST' action=''>
                        <table border='0' style='border: 1px solid #979797;border-radius:15px;width:90%;table-layout:fixed'>
                            <tr><td style='font-size:36px' colspan='2'>房型編輯</td></tr>
                            <tr>
                                <td>
                                    <table style='width:90%;table-layout:fixed'>
                                        <tr>
                                            <td style='text-align:right;font-size:26px;width:40%'>房型名稱：</td>
                                            <td>
                                                <input type='text' style='font-size:26px;width:90%;height:50' name='Room_Type' value='".$Room_Type."'";if($Room_Type != '') echo " readonly"; echo "></td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='text-align:right;font-size:26px;'>入住人數：</td>
                                            <td><input type='number' style='font-size:26px;width:90%;height:50' name='Tenant' value='".$Tenant."'></td>
                                        </tr>
                                        <tr>
                                            <td style='text-align:right;font-size:26px;'>平日房價：</td>
                                            <td><input type='number' style='font-size:26px;width:90%;height:50' name='WeekDay_Price' value='".$WeekDay_Price."'></td>
                                        </tr>
                                        <tr>
                                            <td style='text-align:right;font-size:26px;'>假日房價：</td>
                                            <td><input type='number' style='font-size:26px;width:90%;height:50' name='Weekend_Price' value='".$Weekend_Price."'></td>
                                        </tr>
                                        <tr style='display:none'>
                                            <td></td>
                                            <td>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' style='display: none' name='WeekDay[0]' value='0' onclick='checkDay(this)'"; if(!in_array('0',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>日</span></label>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' name='WeekDay[1]' value='1' onclick='checkDay(this)'"; if(!in_array('1',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>一</span></label>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' name='WeekDay[2]' value='2' onclick='checkDay(this)'"; if(!in_array('2',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>二</span></label>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' name='WeekDay[3]' value='3' onclick='checkDay(this)'"; if(!in_array('3',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>三</span></label>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' name='WeekDay[4]' value='4' onclick='checkDay(this)'"; if(!in_array('4',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>四</span></label>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' name='WeekDay[5]' value='5' onclick='checkDay(this)'"; if(!in_array('5',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>五</span></label>";
                                            echo "<label class='switch'><input type='checkbox' class='WeekDay' name='WeekDay[6]' value='6' onclick='checkDay(this)'"; if(!in_array('6',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>六</span></label>";
                                                echo 
                                            "</td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[0]' value='0' onclick='checkDay(this)'"; if(in_array('0',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>日</span></label>&nbsp;";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[1]' value='1' onclick='checkDay(this)'"; if(in_array('1',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>一</span></label>&nbsp;";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[2]' value='2' onclick='checkDay(this)'"; if(in_array('2',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>二</span></label>&nbsp;";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[3]' value='3' onclick='checkDay(this)'"; if(in_array('3',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>三</span></label>&nbsp;";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[4]' value='4' onclick='checkDay(this)'"; if(in_array('4',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>四</span></label>&nbsp;";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[5]' value='5' onclick='checkDay(this)'"; if(in_array('5',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>五</span></label>&nbsp;";
                                            echo "<label class='switch'><input type='checkbox' class='Weekend' name='Weekend[6]' value='6' onclick='checkDay(this)'"; if(in_array('6',$Weekend_array)) echo "checked"; echo "><span style='text-align:center;vertical-align:bottom;border-radius:8px;padding-top:8px;' class='slider'>六</span></label>";
                                            echo "
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='vertical-align:top;text-align:right;font-size:26px'>休息基本價：</td>
                                            <td><input type='number' style='font-size:26px;width:207px;height:50px;' name='Rest_Price' min='0' value='".$Rest_Price."' required></td>
                                        </tr>
                                        <tr>
                                            <td style='vertical-align:top;text-align:right;font-size:26px'>每小時價格：</td>
                                            <td><input type='number' style='font-size:26px;width:207px;height:50px;' name='Rest_Per_Hour' min='0' value='".$Rest_Per_Hour."' required></td>
                                        </tr>
                                        <tr>
                                            <td style='vertical-align:top;text-align:right;font-size:26px'>房間描述：</td>
                                            <td><textarea style='font-size:26px' rows='4' name='Description' style='resize: none;' onkeyup='autogrow(this);'>".str_replace("<br />", chr(13).chr(10),$Description)."</textarea></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table style='width:90%;table-layout:fixed'>
                                        <tr>
                                            <td style='text-align:right;font-size:26px;width:30%'>客房號碼：</td>
                                            <td style='width:350px'><input type='button' style='font-size:26px;width:90%;height:50px;background-color:WHITE;border-radius:15px' value='新增房號' onclick='add_num()'></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <table border='0' id='add_room' width='100%'>";
                                                    for($counter=0;$counter<sizeof($Room_Num);$counter++){
                                                        echo "<tr><td><input type='text' style='font-size:26px;width:180px;height:50px;' name='Room_Num[".$counter."]' value='".$Room_Num[$counter]."' required><input type='button' class='function_btn' name='delete' style='width:100px;height:50px;font-size:24px;background-color:#E02020;color:WHITE;border-radius:15px;margin-left:10px' onClick='delete_row(this)' value='刪除'></td></tr>";
                                                    }
                                                    echo "
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='text-align:center;' colspan='2'><input type='submit' style='font-size:32px;width:90%;height:80px;background-color:#0091FF;color:WHITE;border-radius:15px' value='確定'></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </form>
                </center>
                ";
            ?>
        </div>
    </body>
</html>

<script>
    function add_num(){
        var rows = document.getElementById('add_room').getElementsByTagName('tr').length;
        var table = document.getElementById('add_room');
        var row = table.insertRow(rows);
        var cell = row.insertCell(0);
        cell.innerHTML = "<input type='text' style='font-size:26px;width:180px;height:50px' name='Room_Num["+rows+"]' required><input type='button' class='function_btn' name='delete' style='width:100px;height:50px;font-size:24px;background-color:#E02020;color:WHITE;border-radius:15px;margin-left:10px' onClick='delete_row(this)' value='刪除'>";
    }
    function delete_row(target){
        if(document.getElementById('add_room').getElementsByTagName('tr').length<=1){
            alert('至少該有一間房間！');
            return;
        }
        target.parentNode.parentNode.parentNode.removeChild(target.parentNode.parentNode);
        // console.log();
    }
    function paddingLeft(str,lenght){
        if(str.toString().length >= lenght)
            return str;
        else
            return paddingLeft("0" +str,lenght);
    }
    function autogrow(textarea){
        var adjustedHeight=textarea.clientHeight;
        adjustedHeight=Math.max(textarea.scrollHeight,adjustedHeight);
        if (adjustedHeight>textarea.clientHeight){
            textarea.style.height=adjustedHeight+'px';
        }
    }
    function checkDay(checkbox){
        for(i=0;i<7;i++){
            if(checkbox.className=='Weekend'){
                if(document.getElementsByClassName("WeekDay")[i].value==checkbox.value){
                    document.getElementsByClassName("WeekDay")[i].checked = !checkbox.checked;
                    console.log(!checkbox.checked);
                    break;
                }
            }
        }
    }
</script>
<?php
    if(isset($_POST['Room_Type'])){
        $Description = str_replace("<br />", '',nl2br($_POST["Description"]));
        $WeekDay = '';
        $Weekend = '';
        sort($_POST['WeekDay']);
        for($i=0;$i<sizeof($_POST['WeekDay']);$i++)
            if(($i+1)!=sizeof($_POST['WeekDay']))
                $WeekDay .= $_POST['WeekDay'][$i] . "/";
            else
                $WeekDay .= $_POST['WeekDay'][$i];

        sort($_POST['Weekend']);
        for($i=0;$i<sizeof($_POST['Weekend']);$i++)
            if(($i+1)!=sizeof($_POST['Weekend']))
                $Weekend .= $_POST['Weekend'][$i] . "/";
            else
                $Weekend .= $_POST['Weekend'][$i];

        $sql = "SELECT `Room_Type` FROM `room_type` WHERE `Room_Type`='".$_POST['Room_Type']."'";
        $result = mysqli_query($conn,$sql);
        if(($result -> num_rows) > 0){
            $sql = "UPDATE `room_type` SET `Tenant`='".$_POST['Tenant']."',`WeekDay_Price`='".$_POST['WeekDay_Price']."',`Weekend_Price`='".$_POST['Weekend_Price']."',`WeekDay_Days`='".$WeekDay."',`Weekend_Days`='".$Weekend."',`Rest_Price`='".$_POST['Rest_Price']."',`Rest_Per_Hour`='".$_POST['Rest_Per_Hour']."',`Description`='".$Description."' WHERE `Room_Type` = '".$_POST['Room_Type']."'";
            // echo $sql . "<br>";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
        }else{
            $sql = "INSERT INTO `room_type`(`Room_Type`, `Tenant`, `WeekDay_Price`, `Weekend_Price`, `WeekDay_Days`, `Weekend_Days`, `Rest_Price`, `Rest_Per_Hour`, `Description`) VALUES ('".$_POST['Room_Type']."','".$_POST['Tenant']."','".$_POST['WeekDay_Price']."','".$_POST['Weekend_Price']."','".$WeekDay."','".$Weekend."','".$_POST['Rest_Price']."','".$_POST['Rest_Per_Hour']."','".$Description."')";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
                die;
            }
        }

        $sql = "DELETE FROM `room_status` WHERE `Room_Type`='".$_POST['Room_Type']."'";
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            // continue;
            die;
        }
        foreach($_POST['Room_Num'] as $value){
            $sql = "INSERT INTO `room_status`(`Room_Type`, `Room_Num`) VALUES ('".$_POST['Room_Type']."','".$value."')";
            if(!mysqli_query($conn,$sql)){
                echo "This SQL: " . $sql . "<br>";
            }
        }
        echo '<meta http-equiv=REFRESH CONTENT=0;url=price.php>';
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    if(isset($_POST['delete'])){
        if($_COOKIE['Staff_Level']>=3){
            echo '<meta http-equiv=REFRESH CONTENT=0;url=price.php>';
            die;
        }
        $sql = "DELETE FROM `room_status` WHERE `Room_Type`='".$_POST['delete']."'";
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            die;
        }
        $sql = "DELETE FROM `room_type` WHERE `Room_Type`='".$_POST['delete']."'";
        if(!mysqli_query($conn,$sql)){
            echo "This SQL: " . $sql . "<br>";
            die;
        }
        echo '<meta http-equiv=REFRESH CONTENT=0;url=price.php>';
    }
?>