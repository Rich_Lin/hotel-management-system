<html>
    <head>
        <style>
            .date_btn{
                width: 55px;
                height:71px;
                border-radius: 7.5px;
                background-color: WHITE;
                border: 1px solid #6D7278;
                font-size: 16px;
                margin: 0px;
            }
            .checking{
                background-color: #6D7278 !important;
                color: WHITE !important;
                /* width: 220px !important; */
            }
            .current_date{
                background-color: #E5E5E5;
            }
            .the_table{
                border-collapse: separate;
            }
            .the_table td{
                border-radius:7.5px;
                height: 68px;
                width: 65px;
                background-color: #C2C2C2;
                text-align: center;
            }
            .pr{
                position: relative;
            }
            .main_function_div{
                /* width: 100%; */
                height: 750px;
                overflow-x: hidden;
                overflow-y: scroll;
    			position: relative;
            }
            .main_function_div::-webkit-scrollbar{
                display: none
            }
            .unset_booking_block{
                background-image: linear-gradient(90deg, #34C6FF, #007EFA);
                width: 233px;
                height: 142px;
                border-radius: 15px;
                margin: 10px 0px;
                color: WHITE;
                font-size: 22px;
                padding-top: 20px;
                padding-left: 20px;
    			position: relative;
            }
            .arranged_booking{
                top: 0;
                left: 0;
                height: 100%;
                border-radius: 7.5px;
                position: absolute;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                /* background-color: #DADADA; */
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                background-color: transparent;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .info_table{
                width: 90%
            }
            .info_table tr,td{
                /* vertical-align: center;
                padding: 5px 0px;
                font-size: 22px; */
                font-size: 22px;
            }
            .info_button_table{
                width: 100%;
                height: 175px;
                background-color: WHITE;
                border: 2.5px solid #979797;
                border-radius:15px;
                margin: 10px;
            }
            .info_button_table:hover{
                /* border: 1px solid #DADADA; */
                background-color: #DADADA;
            }
            #Booked_Room_Dialog td{
                padding: 4px 0px;
            }
        </style>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="daily_review.css">
        <script type="text/javascript" src="../functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<script type="text/javascript" src="redips-drag-min.js"></script>
		<script type="text/javascript" src="script.js"></script>
		<script type="text/javascript" src="../lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../lightpick.css">
    </head>
    <body onload='includeHTML();create_datebar();create_unset_bookinglist();'>
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center>
                <div>
                    <table width='95%'>
                        <tr>
                            <td><input type='text' style='width:300px;height:50px;font-size:22px;text-align:center;' id='Date'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#0091FF;' value='今日' onClick='set_Date()'></td>
                            <td style='text-align:right'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='休息登入' onClick='create_rest()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='訂單入住' onClick='checkin_list()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='新增訂單' onClick="location.href = '../check_in/reservation.php';"></td>
                        </tr>
                    </table>
                </div>
                <div id="redips-drag" style='width:100%'>
                    <center>
                        <table style='table-layout:fixed;display:inline-block;'>
                            <tr>
                                <td width='10%' class="redips-mark"></td>
                                <td class="redips-mark"><center><table style='width:100%;border-collapse: separate;' cellspacing='10'><tr id='datebar_container'></tr></table></center></td>
                                <td class="redips-mark"></td>
                            </tr>
                            <tr>
                                <td style='vertical-align:top;' class="redips-mark">
                                    <div id='roomlist_container' class='main_function_div'>
                                        <table id='roomlist_table' width='100%' class='the_table' style='font-size:28px;' cellspacing='10'>
                                        </table>
                                    </div>
                                </td>
                                <td style='vertical-align:top' class="redips-mark">
                                    <center>
                                        <div id='roomdate_container' class='main_function_div'>
                                            <table id='roomdate_table' class='the_table' style='width:100%' cellspacing='10'>
                                            </table>
                                        </div>
                                    </center>
                                </td>
                                <td style='width:259px;vertical-align:top;text-align:center;' class="redips-mark">
                                    <div style='font-size:36px'>未排房訂單：<span id='unset_bookinglist_counter'></span></div>
                                    <div class='main_function_div'>
                                        <table id='unset_bookinglist_container'></table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
            </center>
        </div>
    </body>
</html>

<!----------Dialog---------->
    <div id='Booking_List_Dialog' name='dialog_section'><br>
        <center>
            <table width='80%'>
                <tr><td style='text-align:center;font-size:36px;'>未入住訂單：<input type='text' id='Fuzzy_Search' style='text-align:center;font-size:36px;font-family:Microsoft JhengHei;' placeholder='關鍵字搜尋'></td></tr>
            </table>
            <form action='../booking_list/change_info.php' method='POST'>
                <table id='Booking_List_Table' width='90%' style='table-layout: fixed;'>
                </table>
            </form>
        </center>
    </div>

    <div id="Rest_Dialog" name='dialog_section'><br>
        <form id='rest_form' name='rest_form' action='service.php' method='POST'>
            <input type='hidden' name='Feature' value='create_rest_order'>
            <div id='reservation'>
                <center>
                    <table>
                        <tr><td style='font-size:57px;text-align:center;'>
                            休息登入
                        </td></tr>

                        <tr><td style='font-size:26px;text-align:center;'>
                            <select id='room_type_selector' name='room_type_selector' class='modified_select' style='width:541.4px;height:87.2px' onchange='select_room(this.value);' required>
                                <option value=''>請選擇房型</option>
                            </select>
                        </td></tr>

                        <tr><td style='font-size:26px;text-align:center;'>
                            <select id='room_num_selector' name='room_num_selector' class='modified_select' style='width:541.4px;height:87.2px' required>
                                <option value=''>請選擇房號</option>
                            </select>
                        </td></tr>
                        
                        <tr><td style='font-size:26px;'>
                            <div style='width:49%;text-align:left;display:inline-block'>
                                時數：<input type='number' name='rest_hour' style='height:50px;width:80px;font-size:26px;' id='rest_hour' min='1' value='1' onkeyup='set_rest_price(this.value);' onChange='set_rest_price(this.value);' required>
                            </div>
                            <div style='width:49%;text-align:right;display:inline-block'>
                                總額：$<span id='rest_Total_Price_text'></span>
                            </div>
                        </td></tr>
                        
                        <tr><td style='font-size:26px;text-align:center;height:50px;'>
                            <span id='warning_message'></span>
                        </td></tr>
                        
                        <tr><td style='font-size:26px;text-align:right;'>
                            <button type='button' class='function_btn' style='background-color:#0091FF' id='to_payment' onClick='page_to_payment()'>收款</button>
                        </td></tr>
                    </table>
                </center>
            </div>
            <div id='payment' style='display:none;'>
                <center>
                    <table cellspacing='10'>
                        <tr><td style='font-size:32px;'>
                            總額：<span id='Rest_Total_Price'></span>
                            <input type='hidden' id='rest_Total_Price' name='rest_Total_Price'>
                        </td></tr>
                        <tr><td style='font-size:26px;'>
                            收款人員：
                            <select id='staff_selector' class='modified_select' style='width:250px;height:50px;' name='staff_selector' required>
                                <option value=''>請選擇人員</option>
                            </select>
                        </td></tr>
                        <tr><td style='font-size:26px;'>
                            <center>
                                <table border='1' width='100%' cellspacing='10' style='border-radius:15px;'>
                                    <tr>
                                        <td style='font-size:26px;text-align:right;border:0'>收款方式：</td>
                                        <td style='font-size:26px;border:0'>
                                            <select class='modified_select' name='payment_method[]' style='width:250px;height:50px;' required>
                                                <option value=''>請選擇付款方式</option>
                                                <option value='0'>現金</option>
                                                <option value='1'>信用卡</option>
                                                <option value='2'>轉帳</option>
                                                <option value='3'>其它</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style='font-size:26px;text-align:right;border:0'>收款金額：</td>
                                        <td style='font-size:26px;border:0'><input type='number' style='width:250px;height:50px;' name='payment_amount[]' min='0' required></td>
                                    </tr>
                                    <tr>
                                        <td style='font-size:26px;text-align:right;vertical-align:top;border:0'>備註：</td>
                                        <td style='font-size:26px;border:0'><textarea name='Remark[]' rows='3'></textarea></td>
                                    </tr>
                                </table>
                            </center>
                        </td></tr>
                        <tr><td id='new_payment_tr' style='display:none' style='font-size:26px;'>
                            
                        </td></tr>
                        <tr><td style='font-size:32px;text-align:center;'>
                            <button type='button' id='new_btn' style='width:520px;height:66px;color:#0091FF' class='function_btn' onClick='display_another_payment()'>+ 增加</button>
                        </td></tr>
                        <tr><td style='font-size:32px;text-align:center;'>
                            <button style='width:520px;height:66px;background-color:#0091FF' class='function_btn'>完成</button>
                        </td></tr>
                    </table>
                </center>
            </div>
        </form>
    </div>

    <div id="Booked_Room_Dialog" name='dialog_section'><br>
        <center>
            <table id='Index_Table' width='90%'>
                <tr><td rowspan='2'><center><img src='../images/LWithoutN.png' style='width:80px;height:80px;'></center></td><td style='font-size:32px'>訂單編號：<span id='Dialog_Booking_ID' style='font-size:26px;color:#0091FF'></td></tr>
                <tr><td style='font-size:32px'>訂房日期：<span id='Dialog_Booking_Date' style='font-size:26px'></span></td></tr>
            </table>
            <br>
            <table id='Customer_Table' width='90%'>
                <tr><td style='font-size:26px'>旅客姓名：<span id='Customer_Name' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>旅客性別：<span id='Customer_Sex' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>旅客國籍：<span id='Customer_Nationality' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>入住日期：<span id='CIN_Date' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>退房日期：<span id='COUT_Date' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>連絡電話：<span id='Customer_Phone' style='font-size:26px'></span></td></tr>
                <tr><td style='font-size:26px'>E-Mail：<span id='Customer_Email' style='font-size:26px'></span></td></tr>
            </table>
            <br>
            <table id='Detail_Table' width='90%'>
                <tr>
                    <td style='font-size:26px'>房型：<span id='Room_Type' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>總價：<span id='Total_Price' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>房號：<span id='Room_Num' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>已收金額：<span id='Total_Paid' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>入住人數：<span id='People_Count' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>代收金額：<span id='Unpaid' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td style='font-size:26px'>入住天數：<span id='Day_Count' style='font-size:26px'></span></td>
                    <td style='font-size:26px'>房卡數量：<span id='Keycard_Counter' style='font-size:26px'></span></td>
                </tr>
                <tr>
                    <td colspan='2' style='font-size:26px'>備註：<span id='Index_Remark' style='font-size:26px'></span></td>
                </tr>
            </table>
            <br>
            <button type="button" style='width:130px;height:50px;background-color:#0091FF;font-size:24px;margin:0px 5px;' class='function_btn' onClick="close_dialog()">確定</button>
        </center>
    </div>
<!-------------------------->

<script>
    var gender = ['女','男','不明'];
    var Weekday = ['日','一','二','三','四','五','六'];
    var Room_Array = [];
    var Booking_List = [];
    var minDate;

    window.onerror = function(errorMsg, url, lineNumber){
        // location.reload();
    }

    $(document).ready(function() {
        var ignoreScrollEvents = false;
        function syncScroll(element1, element2) {
            element1.scroll(function (e) {
                var ignore = ignoreScrollEvents
                ignoreScrollEvents = false
                if (ignore) return

                ignoreScrollEvents = true
                element2.scrollTop(element1.scrollTop())
            })
        }
        syncScroll($("#roomlist_container"), $("#roomdate_container"));
        syncScroll($("#roomdate_container"), $("#roomlist_container"));


        $("#Rest_Dialog").dialog({
            height: 600,
            width: 700,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
        
        $("#Booking_List_Dialog").dialog({
            height: 854,
            width: 970,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#rest_form").submit(function(){
            event.preventDefault();
            $.ajax({
                type: "POST",
                url: "service.php",
                data: $("#rest_form").serialize(),
                success: function(data) {
                    create_datebar();
                    create_unset_bookinglist();
                    close_dialog();
                },
                error: function(jqXHR) {
                    console.log("error: " + jqXHR.responseText);
                }
            })
        });
        
        $("#Booked_Room_Dialog").dialog({
            height: 876,
            width: 669,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
    });

    var picker = new Lightpick({
        field: document.getElementById('Date'),
        startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            create_datebar();
            // Day count is disabled at line 719
        }
    });

    function set_Date(){
        $("#Date").val(moment().startOf('day').format('YYYY/MM/DD'));
        create_datebar();
    }

    function create_datebar(){
        var Today_Date = moment().startOf('day');
        var Start_Date = $("#Date").val();
        minDate =  moment(new Date(Start_Date)).subtract(2, 'day').format('YYYY/MM/DD');
        var row='';
        for(i=0;i<15;i++){
            row += "<td class='redips-mark";
            if(i>9)
                row += " over_size";
            row += "'><center><button type='button' id='" + moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD') + "' class='date_btn";
            if(moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD')==moment(new Date(Today_Date)).format('YYYY/MM/DD')){
                row += " current_date";
            }
            if(i==2)
                row += " checking' onClick='set_Checking_Date(this.id)'>" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "<br>(" + Weekday[moment(new Date(minDate)).add(i, 'day').day()] + ")</button></center></td>";
            else
                row += "' onClick='set_Checking_Date(this.id)'>" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "<br>(" + Weekday[moment(new Date(minDate)).add(i, 'day').day()] + ")</button></center></td>";
        }
        $("#datebar_container").html(row);
        create_roomlist();
    }

    function set_Checking_Date(target_value){
        $("#Date").val(target_value);
        create_datebar();
    }

    function set_Month(value){
        var Current_Date = $("#Date").val();
        switch(value){
            case -1:
                $("#Date").val(moment(new Date(Current_Date)).subtract(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;

            case 1:
                $("#Date").val(moment(new Date(Current_Date)).add(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;
        }
    }

    function create_roomlist(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_roomlist",
            },
            success: function(data) {
                Room_Array = data;
                var room_list = '';
                for(i=0;i<Room_Array.length;i++){
                    room_list += "<tr><td class='redips-mark'>" + Room_Array[i]['Room_Num'] + "</td></tr>";
                }
                $("#roomlist_table").html(room_list);
                create_calendar_table();
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        });
    }

    function create_calendar_table(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_booked_list",
                minDate: minDate,
            },
            success: function(data) {
                Room_Calendar = data;
                var Room_Calendar_list = '';
                for(i=0;i<Room_Array.length;i++){
                    Room_Calendar_list += "<tr>";
                    for(day_count=0;day_count<15;day_count++){
                        var flag = true;
                        for(x=0;x<Room_Calendar.length;x++){
                            if(Room_Calendar[x].Room_Num==Room_Array[i]['Room_Num'] && day_count==Room_Calendar[x].Start_From){
                                if(Room_Calendar[x].Room_Status == 'case_0'){
                                    var set_width = (Room_Calendar[x].Day_Count*71) + (Room_Calendar[x].Day_Count-1)*10;
                                    Room_Calendar_list += "<td class='"+Room_Array[i]['Room_Num']+" "+Room_Array[i]['Room_Type']+" pr";
                                    if(day_count>9)
                                        Room_Calendar_list += " over_size";
                                    Room_Calendar_list += "'><div id='"+Room_Calendar[x].Code+"' title='"+Room_Calendar[x].Booking_ID+"' class='"+Room_Calendar[x].Room_Status+" "+Room_Array[i]['Room_Type']+" redips-drag arranged_booking' style='width:"+set_width+"px;' onClick='open_dialog(this.id);'></div></td>";
                                }
                                else{
                                    Room_Calendar_list += "<td id='"+Room_Calendar[x].Code+"' class='"+Room_Calendar[x].Room_Status+" "+Room_Array[i]['Room_Num']+" "+Room_Array[i]['Room_Type']+" redips-mark";
                                    if(day_count>9)
                                        Room_Calendar_list += " over_size";
                                    Room_Calendar_list += "' colspan='"+Room_Calendar[x].Day_Count+"' onClick='open_dialog(this.id);'></td>";
                                    day_count+=Room_Calendar[x].Day_Count-1;
                                    // console.log(Room_Calendar[x]);
                                }
                                flag = false;
                            }
                        }
                        if(flag){
                            Room_Calendar_list += "<td class='"+Room_Array[i]['Room_Num']+" "+Room_Array[i]['Room_Type'];
                            if(day_count>9)
                                Room_Calendar_list += " over_size";
                            Room_Calendar_list += "'></td>";
                        }
                    }
                    Room_Calendar_list += "</tr>";
                }
                $("#roomdate_table").html(Room_Calendar_list);
                redips.init();
                setTimeout('create_calendar_table()', 60000);
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function create_unset_bookinglist(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_no_roomlist",
            },
            success: function(data) {
                $("#unset_bookinglist_counter").html(data.length);
                var unset_bookinglist = '';
                var bookinglist = data;
                for(i=0;i<bookinglist.length;i++){
                    bookinglist[i].CIN_Date = bookinglist[i].CIN_Date.split(" ")[0];
                    bookinglist[i].COUT_Date = bookinglist[i].COUT_Date.split(" ")[0];
                    unset_bookinglist += "<tr><td><div class='unset_booking_block redips-drag "+bookinglist[i].Room_Type+"' id='"+bookinglist[i].Code+"'><div>旅客："+bookinglist[i].Customer_Name+"<br>房型："+bookinglist[i].Room_Type+"<br>入住："+bookinglist[i].CIN_Date+"<br>退房："+bookinglist[i].COUT_Date+"</div></div></td></tr>";
                }
                $("#unset_bookinglist_container").html(unset_bookinglist);
    			redips.init();
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        });
        setTimeout('create_unset_bookinglist()', 60000);
    }

    function set_limit_cells(class_Name){
        var x = document.getElementById("roomdate_table").getElementsByTagName("td");
        for(i=0;i<x.length;i++){
            if(class_Name != ""){
                if(!x[i].classList.contains(class_Name))
                    x[i].classList.add("redips-mark");
            }
            else{
                if(x[i].colSpan == 1)
                    x[i].classList.remove("redips-mark");
            }
        }
    }

    function arrange_booking(obj){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "arrange_booking",
                BDetail_Code: obj.id,
                Arranged_Room: obj.parentNode.classList[0]
            },
            success: function(data) {
                switch(data.Success){
                    case 0:
                        alert("房型錯誤無法排房！");
                        create_unset_bookinglist();
                        create_calendar_table();
                    break;

                    case 1:
                        alert("此時間段已安排房間！");
                        create_unset_bookinglist();
                        create_calendar_table();
                    break;
                }
                
                $("#Date").val(moment(new Date(data.Date.split(" ")[0])).format('YYYY/MM/DD'));
                create_datebar();
                create_unset_bookinglist();
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        });
    }

    function select_room(room_type){
        var room_num_option = "<option value=''>請選擇房號</option>";
        for(i=0;i<room_json[room_type]['Room_Num'].length;i++){
            room_num_option += "<option value='"+room_json[room_type]['Room_Num'][i]+"'>"+room_json[room_type]['Room_Num'][i]+"</option>";
        }
        $("#rest_Total_Price_text").html(parseInt(room_json[room_type]['default_rest_price'],10)+parseInt(($("#rest_hour").val()-1)*room_json[room_type]['rest_per_hour_price'],10));
        $("#room_num_selector").html(room_num_option);
        $("#Rest_Total_Price").html($("#rest_Total_Price_text").html());
        $("#rest_Total_Price").val($("#rest_Total_Price_text").html());
    }

    function set_rest_price(target_value){
        if(target_value>=8){
            $("#warning_message").html('<font color="#ff0000">超過休息時數上限，請新增過夜訂單</font>');
            document.getElementById('to_payment').disabled = true;
            $("#rest_Total_Price_text").html('XXX');
            $("#Rest_Total_Price").html($("#rest_Total_Price_text").html());
            $("#rest_Total_Price").val('0');
        }
        else{
            $("#warning_message").html('');
            document.getElementById('to_payment').disabled = false;
            $("#rest_Total_Price_text").html(parseInt(room_json[$("#room_type_selector").val()]['default_rest_price'],10)+parseInt((target_value-1)*room_json[$("#room_type_selector").val()]['rest_per_hour_price'],10));
            $("#Rest_Total_Price").html($("#rest_Total_Price_text").html());
            $("#rest_Total_Price").val($("#rest_Total_Price_text").html());
        }
    }

    function page_to_payment(){
        if($("#room_type_selector").val()=='' || $("#room_num_selector").val()==''){
            alert("尚有部分未填寫完成");
            return;
        }
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_staff",
            },
            success: function(data) {
                var new_decodedCookie = decodeURIComponent(document.cookie).split(';');
                var ID = new_decodedCookie[0].split('=')[1];
                staff_json = data;
                var staff_option = "<option value=''>請選擇人員</option>";
                for(i=0;i<staff_json.length;i++){
                    staff_option += "<option value='"+staff_json[i].Staff_ID+"'";
                    if(ID == staff_json[i].Staff_ID)
                        staff_option += " selected";
                    staff_option += ">"+staff_json[i].Staff_Name+"</option>";
                }
                $("#staff_selector").html(staff_option);
                document.getElementById('reservation').style.display = 'none';
                document.getElementById('payment').style.display = 'block';
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function checkin_list(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "show_booking_list",
                Fuzzy_Search: $("#Fuzzy_Search").val()
            },
            success: function(data) {
                $("#Booking_List_Table").html('');
                var json_data = data;
                var row = "";
                for(i=0;i<json_data.length;i++){
                    row += "<tr><td><button type='submit' class='info_button_table' value='"+json_data[i]['Booking_ID']+"_info' name='Action'><center><table class='info_table'><tr><td width='50%'>訂單編號：<span style='color:#0091FF;'>"+json_data[i]['Booking_ID']+"</span></td><td>入住日期："+json_data[i]['CIN_Date'].replace(/-/g, "/")+"</td></tr><tr><td>房號：<span style='color:#0091FF;'>"+json_data[i]['Room_Num']+"</span></td><td>退房日期："+json_data[i]['COUT_Date']+"</td></tr><tr><td>房型："+json_data[i]['Room_Type']+"</td><td>旅客姓名："+json_data[i]['Customer_Name']+"</td></tr><tr><td>代收金額："+(json_data[i]['Total_Price']-json_data[i]['Paid'])+"</td><td>連絡電話："+json_data[i]['Customer_Phone']+"</td></tr></table></center></button></td></tr>";
                }
                $("#Booking_List_Table").html(row);
                $( "#Booking_List_Dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function close_dialog(){
        var Dialog_Section = document.getElementsByName('dialog_section');
        for(i=0;i<Dialog_Section.length;i++)
            $('#'+Dialog_Section[i].id).dialog( "close" );
        $("#cleaning_staff").val('');
        $("#Fuzzy_Search").val('');
        $("#room_type_selector").val('');
        $("#room_num_selector").val('');
        $("#rest_hour").val('1');
        $("#rest_Total_Price").val('');
        $("#staff_selector").val('');
        // $("#").val('');
        $("#rest_Total_Price_text").html('');
        $("#warning_message").html('');
        // $("#").html('');

        document.getElementById('reservation').style.display = 'block';
        document.getElementById('payment').style.display = 'none';

        var x = document.getElementsByName('payment_method[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';
        var x = document.getElementsByName('payment_amount[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';
        var x = document.getElementsByName('Remark[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';

        var x = document.getElementsByClassName('new_payment');
        for(i=0;i<x.length;i++)
            x[i].required = false;
        document.getElementById('new_payment_tr').style.display = "none";
        document.getElementById('new_payment_tr').innerHTML = "";
        document.getElementById('new_btn').style.display = "block";
    }

    function create_rest(){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "get_rest_room",
            },
            success: function(data) {
                room_json = data;
                var room_type_option = "<option value=''>請選擇房型</option>";
                for(var room_type in room_json){
                    room_type_option += "<option value='"+room_type+"'>"+room_type+"</option>";
                }
                $("#room_type_selector").html(room_type_option);
                $("#Rest_Dialog").dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function open_dialog(The_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "check_booked_room",
                Code: The_ID
            },
            success: function(data) {
                $("#Booking_ID_Hidden").val(data['Booking_ID']);
                $("#Room_Status_Hidden").val(data['Room_Status']);
                $("#Room_Num_Hidden").val(data['Room_Num']);

                $("#Dialog_Booking_ID").html(data['Booking_ID']);
                $("#Dialog_Booking_Date").html(data['Booking_Date'].replace(/-/g, "/"));
                $("#Customer_Name").html(data['Customer_Name']);
                $("#Customer_Sex").html(gender[data['Customer_Sex']]);
                $("#Customer_Nationality").html(data['Customer_Nationality']);
                $("#Customer_Phone").html(data['Customer_Phone']);
                $("#Customer_Email").html(data['Customer_Email']);
                $("#CIN_Date").html(data['CIN_Date'].replace(/-/g, "/"));
                $("#COUT_Date").html(data['COUT_Date'].replace(/-/g, "/"));
                $("#Room_Type").html(data['Room_Type']);
                $("#Room_Num").html(data['Room_Num']);
                $("#People_Count").html(data['People_Count']);
                $("#Day_Count").html(data['Day_Count']);
                $("#Keycard_Counter").html(data['Keycard_Counter']);
                $("#Index_Remark").html(data['Index_Remark']);
                $("#Total_Price").html(data['Total_Price']);
                $("#Total_Paid").html(data['Total_Paid']);
                $("#Unpaid").html(data['Total_Price']-data['Total_Paid']);

                $( "#Booked_Room_Dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function display_another_payment(){
        var new_payment = "<center><table border='1' width='100%' cellspacing='10' style='border-radius:15px;'><tr><td style='font-size:26px;text-align:right;border:0'>收款方式：</td><td style='font-size:26px;border:0'><select class='modified_select new_payment' name='payment_method[]' style='width:250px;height:50px;'><option value=''>請選擇付款方式</option><option value='0'>現金</option><option value='1'>信用卡</option><option value='2'>轉帳</option><option value='3'>其它</option></select></td></tr><tr><td style='font-size:26px;text-align:right;border:0'>收款金額：</td><td style='font-size:26px;border:0'><input type='number' class='new_payment' style='width:250px;height:50px;' id='new_payment_amount' name='payment_amount[]' min='0'></td></tr><tr><td style='font-size:26px;text-align:right;vertical-align:top;border:0'>備註：</td><td style='font-size:26px;border:0'><textarea name='Remark[]' rows='3'></textarea></td></tr></table></center>";
        document.getElementById('new_payment_tr').innerHTML = new_payment;
        document.getElementById('new_btn').style.display = "none";
    }

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });
</script>