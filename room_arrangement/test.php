<html>
    <head>
        <style>
            .date_btn{
                width: 55px;
                height:71px;
                border-radius: 7.5px;
                background-color: WHITE;
                border: 1px solid #6D7278;
                font-size: 16px;
                margin: 0px;
            }
            .checking{
                background-color: #6D7278 !important;
                color: WHITE !important;
                /* width: 220px !important; */
            }
            .current_date{
                background-color: #E5E5E5;
            }
            .the_table{
                border-collapse: separate;
            }
            .the_table td{
                border-radius:7.5px;
                height: 68px;
                width: 65px;
                background-color: #C2C2C2;
                text-align: center;
            }
            .pr{
                position: relative;
            }
            .main_function_div{
                /* width: 100%; */
                height: 750px;
                overflow-x: hidden;
                overflow-y: scroll;
    			position: relative;
            }
            .main_function_div::-webkit-scrollbar{
                display: none
            }
            .unset_booking_block{
                background-image: linear-gradient(90deg, #34C6FF, #007EFA);
                width: 233px;
                height: 142px;
                border-radius: 15px;
                margin: 10px 0px;
                color: WHITE;
                font-size: 22px;
                padding-top: 20px;
                padding-left: 20px;
    			position: relative;
            }
            .arranged_booking{
                top: 0;
                left: 0;
                height: 100%;
                border-radius: 7.5px;
                position: absolute;
            }
            .ui-widget.ui-widget-content{
                border-radius: 20px;
                border-width: 20px;
                /* background-color: #DADADA; */
                border: 1px solid #DADADA;
            }
            .ui-widget-content {
                border-radius: 20px;
                border-width: 20px;
            }
            .ui-widget-overlay{
                background-color: transparent;
            }
            .ui-dialog-titlebar{
                display: none
            }
            .info_table{
                width: 90%
            }
            .info_table tr,td{
                /* vertical-align: center;
                padding: 5px 0px;
                font-size: 22px; */
                font-size: 22px;
            }
            .info_button_table{
                width: 100%;
                height: 175px;
                background-color: WHITE;
                border: 2.5px solid #979797;
                border-radius:15px;
                margin: 10px;
            }
            .info_button_table:hover{
                /* border: 1px solid #DADADA; */
                background-color: #DADADA;
            }
            #Booked_Room_Dialog td{
                padding: 4px 0px;
            }
        </style>
        <meta charset="UTF-8" />
        <link rel="stylesheet" type="text/css" href="daily_review.css">
        <script type="text/javascript" src="../functions.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

		<script type="text/javascript" src="redips-drag-min.js"></script>
		<script type="text/javascript" src="script.js"></script>
		<script type="text/javascript" src="../lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../lightpick.css">
    </head>
    <body onload='includeHTML();create_datebar();create_unset_bookinglist();'>
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center>
                <div>
                    <table width='95%'>
                        <tr>
                            <td><input type='text' style='width:300px;height:50px;font-size:22px;text-align:center;' id='Date'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#0091FF;' value='今日' onClick='set_Date()'></td>
                            <td style='text-align:right'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='休息登入' onClick='create_rest()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='訂單入住' onClick='checkin_list()'><input type='button' class='function_btn' style='width:130px;height:50px;background-color:#F79B00;' value='新增訂單' onClick="location.href = '../check_in/reservation.php';"></td>
                        </tr>
                    </table>
                </div>
                <div id="redips-drag" style='width:100%'>
                    <center>
                        <table style='table-layout:fixed;display:inline-block;'>
                            <tr>
                                <td width='10%' class="redips-mark"></td>
                                <td class="redips-mark"><center><table style='width:100%;border-collapse: separate;' cellspacing='10'><tr id='datebar_container'></tr></table></center></td>
                            </tr>
                            <tr>
                                <td style='vertical-align:top;' class="redips-mark">
                                    <div id='roomlist_container' class='main_function_div'>
                                        <table id='roomlist_table' width='100%' class='the_table' style='font-size:28px;' cellspacing='10'>
                                        </table>
                                    </div>
                                </td>
                                <td style='vertical-align:top' class="redips-mark">
                                    <center>
                                        <div id='roomdate_container' class='main_function_div'>
                                            <table id='roomdate_table' class='the_table' style='width:100%' cellspacing='10'>
                                            </table>
                                        </div>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </center>
                </div>
            </center>
        </div>
    </body>
</html>

<!----------Dialog---------->
    <div id='Booking_List_Dialog' name='dialog_section'><br>
        <center>
            <table width='80%'>
                <tr><td style='text-align:center;font-size:36px;'>未入住訂單：<input type='text' id='Fuzzy_Search' style='text-align:center;font-size:36px;font-family:Microsoft JhengHei;' placeholder='關鍵字搜尋'></td></tr>
            </table>
            <form action='../booking_list/change_info.php' method='POST'>
                <table id='Booking_List_Table' width='90%' style='table-layout: fixed;'>
                </table>
            </form>
        </center>
    </div>
<!-------------------------->

<script>
    var gender = ['女','男','不明'];
    var Weekday = ['日','一','二','三','四','五','六'];
    var Room_Array = [];
    var Booking_List = [];
    var minDate;

    window.onerror = function(errorMsg, url, lineNumber){
        // location.reload();
    }

    $(document).ready(function() {
        var ignoreScrollEvents = false;
        function syncScroll(element1, element2) {
            element1.scroll(function (e) {
                var ignore = ignoreScrollEvents
                ignoreScrollEvents = false
                if (ignore) return

                ignoreScrollEvents = true
                element2.scrollTop(element1.scrollTop())
            })
        }
        syncScroll($("#roomlist_container"), $("#roomdate_container"));
        syncScroll($("#roomdate_container"), $("#roomlist_container"));
        
        $("#Booking_List_Dialog").dialog({
            height: 854,
            width: 970,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
    });

    var picker = new Lightpick({
        field: document.getElementById('Date'),
        startDate: moment().startOf('day'),
        singleDate: true,
        repick: true,
        onSelect: function(date){
            create_datebar();
            // Day count is disabled at line 719
        }
    });

    function set_Date(){
        $("#Date").val(moment().startOf('day').format('YYYY/MM/DD'));
        create_datebar();
    }

    function create_datebar(){
        var Today_Date = moment().startOf('day');
        var Start_Date = $("#Date").val();
        minDate =  moment(new Date(Start_Date)).subtract(2, 'day').format('YYYY/MM/DD');
        var row='';
        for(i=0;i<15;i++){
            row += "<td class='redips-mark";
            if(i>9)
                row += " over_size";
            row += "'><center><button type='button' id='" + moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD') + "' class='date_btn";
            if(moment(new Date(minDate)).add(i, 'day').format('YYYY/MM/DD')==moment(new Date(Today_Date)).format('YYYY/MM/DD')){
                row += " current_date";
            }
            if(i==2)
                row += " checking' onClick='set_Checking_Date(this.id)'>" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "<br>(" + Weekday[moment(new Date(minDate)).add(i, 'day').day()] + ")</button></center></td>";
            else
                row += "' onClick='set_Checking_Date(this.id)'>" + moment(new Date(minDate)).add(i, 'day').format('MM/DD') + "<br>(" + Weekday[moment(new Date(minDate)).add(i, 'day').day()] + ")</button></center></td>";
        }
        $("#datebar_container").html(row);
        create_roomlist();
    }

    function set_Checking_Date(target_value){
        $("#Date").val(target_value);
        create_datebar();
    }

    function set_Month(value){
        var Current_Date = $("#Date").val();
        switch(value){
            case -1:
                $("#Date").val(moment(new Date(Current_Date)).subtract(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;

            case 1:
                $("#Date").val(moment(new Date(Current_Date)).add(1, 'month').format('YYYY/MM/DD'));
                create_datebar();
            break;
        }
    }

    function create_roomlist(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_roomlist",
            },
            success: function(data) {
                Room_Array = data;
                var room_list = '';
                for(i=0;i<Room_Array.length;i++){
                    room_list += "<tr><td class='redips-mark'>" + Room_Array[i]['Room_Num'] + "</td></tr>";
                }
                $("#roomlist_table").html(room_list);
                create_calendar_table();
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        });
    }

    function create_calendar_table(){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: "get_booked_list",
                minDate: minDate,
            },
            success: function(data) {
                Room_Calendar = data;
                var Room_Calendar_list = '';
                for(i=0;i<Room_Array.length;i++){
                    Room_Calendar_list += "<tr>";
                    for(day_count=0;day_count<15;day_count++){
                        var flag = true;
                        for(x=0;x<Room_Calendar.length;x++){
                            if(Room_Calendar[x].Room_Num==Room_Array[i]['Room_Num'] && day_count==Room_Calendar[x].Start_From){
                                if(Room_Calendar[x].Room_Status == 'case_0'){
                                    var set_width = (Room_Calendar[x].Day_Count*71) + (Room_Calendar[x].Day_Count-1)*10;
                                    Room_Calendar_list += "<td class='"+Room_Array[i]['Room_Num']+" "+Room_Array[i]['Room_Type']+" pr";
                                    if(day_count>9)
                                        Room_Calendar_list += " over_size";
                                    Room_Calendar_list += "'><div id='"+Room_Calendar[x].Code+"' title='"+Room_Calendar[x].Booking_ID+"' class='"+Room_Calendar[x].Room_Status+" "+Room_Array[i]['Room_Type']+" redips-drag arranged_booking' style='width:"+set_width+"px;' onClick='open_dialog(this.id);'></div></td>";
                                }
                                else{
                                    Room_Calendar_list += "<td id='"+Room_Calendar[x].Code+"' class='"+Room_Calendar[x].Room_Status+" "+Room_Array[i]['Room_Num']+" "+Room_Array[i]['Room_Type']+" redips-mark";
                                    if(day_count>9)
                                        Room_Calendar_list += " over_size";
                                    Room_Calendar_list += "' colspan='"+Room_Calendar[x].Day_Count+"' onClick='open_dialog(this.id);'></td>";
                                    day_count+=Room_Calendar[x].Day_Count-1;
                                    // console.log(Room_Calendar[x]);
                                }
                                flag = false;
                            }
                        }
                        if(flag){
                            Room_Calendar_list += "<td class='"+Room_Array[i]['Room_Num']+" "+Room_Array[i]['Room_Type'];
                            if(day_count>9)
                                Room_Calendar_list += " over_size";
                            Room_Calendar_list += "'></td>";
                        }
                    }
                    Room_Calendar_list += "</tr>";
                }
                $("#roomdate_table").html(Room_Calendar_list);
                redips.init();
                setTimeout('create_calendar_table()', 60000);
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    function close_dialog(){
        var Dialog_Section = document.getElementsByName('dialog_section');
        for(i=0;i<Dialog_Section.length;i++)
            $('#'+Dialog_Section[i].id).dialog( "close" );
        $("#cleaning_staff").val('');
        $("#Fuzzy_Search").val('');
        $("#room_type_selector").val('');
        $("#room_num_selector").val('');
        $("#rest_hour").val('1');
        $("#rest_Total_Price").val('');
        $("#staff_selector").val('');
        // $("#").val('');
        $("#rest_Total_Price_text").html('');
        $("#warning_message").html('');
        // $("#").html('');

        document.getElementById('reservation').style.display = 'block';
        document.getElementById('payment').style.display = 'none';

        var x = document.getElementsByName('payment_method[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';
        var x = document.getElementsByName('payment_amount[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';
        var x = document.getElementsByName('Remark[]');
        for(i=0;i<x.length;i++)
            x[i].value = '';

        var x = document.getElementsByClassName('new_payment');
        for(i=0;i<x.length;i++)
            x[i].required = false;
        document.getElementById('new_payment_tr').style.display = "none";
        document.getElementById('new_payment_tr').innerHTML = "";
        document.getElementById('new_btn').style.display = "block";
    }

    function open_dialog(The_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            dataType: "json",
            data: {
                Feature: "check_booked_room",
                Code: The_ID
            },
            success: function(data) {
                $("#Booking_ID_Hidden").val(data['Booking_ID']);
                $("#Room_Status_Hidden").val(data['Room_Status']);
                $("#Room_Num_Hidden").val(data['Room_Num']);

                $("#Dialog_Booking_ID").html(data['Booking_ID']);
                $("#Dialog_Booking_Date").html(data['Booking_Date'].replace(/-/g, "/"));
                $("#Customer_Name").html(data['Customer_Name']);
                $("#Customer_Sex").html(gender[data['Customer_Sex']]);
                $("#Customer_Nationality").html(data['Customer_Nationality']);
                $("#Customer_Phone").html(data['Customer_Phone']);
                $("#Customer_Email").html(data['Customer_Email']);
                $("#CIN_Date").html(data['CIN_Date'].replace(/-/g, "/"));
                $("#COUT_Date").html(data['COUT_Date'].replace(/-/g, "/"));
                $("#Room_Type").html(data['Room_Type']);
                $("#Room_Num").html(data['Room_Num']);
                $("#People_Count").html(data['People_Count']);
                $("#Day_Count").html(data['Day_Count']);
                $("#Keycard_Counter").html(data['Keycard_Counter']);
                $("#Index_Remark").html(data['Index_Remark']);
                $("#Total_Price").html(data['Total_Price']);
                $("#Total_Paid").html(data['Total_Paid']);
                $("#Unpaid").html(data['Total_Price']-data['Total_Paid']);

                $( "#Booked_Room_Dialog" ).dialog( "open" );
            },
            error: function(jqXHR) {
                console.log(jqXHR.responseText);
            }
        })
    }

    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });
</script>