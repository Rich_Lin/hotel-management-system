"use strict";

var redips = {};

redips.configuration = function() {
    redips.left = 'unset_bookinglist_container';
    redips.right = 'roomdate_table';
    redips.size = { w: 0, h: 0 };
};

redips.init = function() {
    var rd = REDIPS.drag;
    var previous_parent;
    redips.configuration();
    redips.hideTables();
    redips.singleContent();
    redips.setEvents();
    // document.getElementById(redips.left).style.position = 'fixed';
    rd.init();

    rd.event.moved = function(cloned) {
        previous_parent = rd.obj.parentNode.parentNode;
        if (rd.obj.classList.contains("unset_booking_block")) {
            set_limit_cells(rd.obj.classList[2]);
            console.log("2: " + rd.obj.classList[2]);
            rd.obj.style.height = '162px';
        } else {
            set_limit_cells(rd.obj.classList[1]);
            console.log("1: " + rd.obj.classList[1]);
            rd.obj.style.height = '68px';
        }
        var scid;
        if (cloned === false) {
            scid = redips.findContainer(rd.obj);
        }
        if (scid === redips.left || cloned === true) {
            rd.dropMode = 'single';
        } else {
            rd.dropMode = 'switch';
        }
        if (rd.obj.classList.contains("redips-drag")) {
            if (rd.obj.parentNode.classList.contains("pr"))
                rd.obj.parentNode.classList.remove("pr");
            rd.obj.style.opacity = "0.1";
        }
    };
    rd.event.droppedBefore = function(targetCell) {
        var id = rd.obj.id,
            tcid,
            scid;
        tcid = redips.findContainer(targetCell);
        scid = redips.findContainer(rd.td.source);
        if (scid === redips.left && tcid === redips.right) {
            // rd.ajaxCall(redips.content_url + '?id=' + id, redips.handler, {div: rd.obj});
            rd.obj.style.width = redips.width;
            rd.obj.style.height = '';
        } else if (scid === redips.right && tcid === redips.left) {
            rd.obj.innerHTML = id;
            rd.obj.style.width = redips.size.w;
            rd.obj.style.height = redips.size.h;
        }
    };
    rd.event.dropped = function(targetCell) {
        set_limit_cells("");
        var flag = true;
        var tcid = redips.findContainer(targetCell);
        if (tcid === redips.left) {
            if (rd.obj.className.indexOf('clnd') !== -1) {
                rd.obj.parentNode.removeChild(rd.obj);
            } else {
                // REDIPS.event.add(rd.obj, 'mouseover', redips.showTooltip);
            }
            rd.obj.classList.remove("arranged_booking");
            rd.obj.classList.remove("case_0");
            rd.obj.classList.add("unset_booking_block");
            rd.obj.style.height = '142px';
            flag = false;
        }
        rd.obj.style.opacity = "";
        if (!rd.obj.parentNode.classList.contains("pr"))
            rd.obj.parentNode.classList.add("pr");
        if (rd.obj.classList.contains("unset_booking_block") && flag == true) {
            rd.obj.classList.remove("unset_booking_block");
            rd.obj.classList.add("arranged_booking");
            rd.obj.classList.add("case_0");
        }
        arrange_booking(rd.obj);
    };
    rd.event.cloned = function() {
        if (rd.obj.className.indexOf('clnd') === -1) {
            rd.obj.className += ' clnd';
        }
        rd.hover.colorTd = redips.hover2;
    };
    rd.error.ajax = function(xhr, obj) {
        obj.div.innerHTML = 'Error: [' + xhr.status + '] ' + xhr.statusText;
        return false;
    };

};

redips.handler = function(xhr, obj) {
    obj.div.innerHTML = xhr.responseText;
};


redips.setEvents = function() {
    var regex_drag = /\bdrag\b/i,
        div, i;
    div = document.getElementById('redips-drag').getElementsByTagName('div');
    for (i = 0; i < div.length; i++) {
        if (regex_drag.test(div[i].className)) {
            // REDIPS.event.add(div[i], 'mouseover', redips.showTooltip);
            // REDIPS.event.add(div[i], 'mouseout', redips.hideTooltip);
        }
    }
    redips.size.w = div[0].style.width;
    redips.size.h = div[0].style.height;
};


redips.hideTables = function() {
    var div, i;
    div = document.getElementById(redips.right).getElementsByTagName('div');
    for (i = 1; i < div.length; i++) {
        // div[i].style.display = 'none';
    }
};


redips.singleContent = function() {
    var cell, i;
    cell = document.getElementById('unset_bookinglist_container').getElementsByTagName('td');
    for (i = 0; i < cell.length; i++) {
        cell[i].className = 'redips-single';
    }
};


redips.findContainer = function(c) {
    try {
        while (c && c.id !== redips.left && c.id !== redips.right) {
            c = c.parentNode;
        }
        return c.id;
    } catch {
        create_unset_bookinglist();
        create_calendar_table();
    }
};


if (window.addEventListener) {
    window.addEventListener('load', redips.init, false);
} else if (window.attachEvent) {
    window.attachEvent('onload', redips.init);
}