<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    // error_reporting(0);
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        //////////////////Customer info//////////////////
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'get_roomlist':
                    get_roomlist($conn);
                break;

                case 'get_booked_list':
                    get_booked_list($_POST['minDate'],$conn);
                break;

                case 'get_no_roomlist':
                    get_no_roomlist($conn);
                break;

                case 'arrange_booking':
                    $sql = "SELECT `booking_detail`.`Room_Type`,`booking_index`.`CIN_Date`,`booking_index`.`COUT_Date` FROM `booking_index`,`booking_detail` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_detail`.`Code`='".$_POST['BDetail_Code']."'";
                    $result = mysqli_query($conn,$sql);
                    $row = $result->fetch_assoc();
                    $Date = $row['CIN_Date'];
                    $Room_Type = $row['Room_Type'];

                    $sql = "SELECT `Room_Type` FROM `room_status` WHERE `Room_Num`='".$_POST['Arranged_Room']."' AND `Room_Type`='".$Room_Type."'";
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result)==0){
                        echo json_encode(array('Success' => 0, 'Date' => $Date),JSON_UNESCAPED_UNICODE);
                        die;
                    }
                    $sql = "SELECT * FROM `booking_index`,`booking_detail` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_detail`.`Room_Num`='".$_POST['Arranged_Room']."' AND ((DATE(`booking_index`.`CIN_Date`) BETWEEN '".explode(" ",$row['CIN_Date'])[0]."' AND '".explode(" ",$row['COUT_Date'])[0]."' ) OR (DATE(`booking_index`.`COUT_Date`) BETWEEN '".explode(" ",$row['CIN_Date'])[0]."' AND '".explode(" ",$row['COUT_Date'])[0]."' ) OR ('".explode(" ",$row['CIN_Date'])[0]."' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`)) OR ('".explode(" ",$row['COUT_Date'])[0]."' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`))) AND (`booking_detail`.`Room_Status` BETWEEN 0 AND 4 OR `booking_detail`.`Room_Status`=7) AND `booking_detail`.`Code`!='".$_POST['BDetail_Code']."' ORDER BY `booking_detail`.`Room_Num`, `booking_index`.`CIN_Date`";
                    $result = mysqli_query($conn,$sql);
                    if(mysqli_num_rows($result)>0)
                        echo json_encode(array('Success' => 1, 'Date' => $Date),JSON_UNESCAPED_UNICODE);
                    else{
                        $sql = "UPDATE `booking_detail` SET `Room_Num`='".$_POST['Arranged_Room']."' WHERE `Code`='".$_POST['BDetail_Code']."'";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                        echo json_encode(array('Success' => 2, 'Date' => $Date),JSON_UNESCAPED_UNICODE);
                    }
                break;

                case 'show_booking_list':
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y/m/d', time());
                    $info_array = array();
                    $sql = " SELECT * FROM `booking_index`,`booking_detail`,`customer` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_index`.`Customer_ID`=`customer`.`Customer_ID` AND (DATE(`booking_index`.`CIN_Date`)<='$Today_Date' AND DATE(`booking_index`.`COUT_Date`)>='$Today_Date') AND `booking_detail`.`Room_Status`=0";
                    $GROUP_BY = " GROUP BY `booking_index`.`Booking_ID`";
                    $Fuzzy_Search = "";
                    if(!empty($_POST['Fuzzy_Search'])){
                        $Fuzzy_Search = " (`customer`.`Customer_Name` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Phone` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Email` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_SSID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `customer`.`Customer_Passport` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_index`.`Booking_ID` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_detail`.`Room_Type` LIKE '%".$_POST['Fuzzy_Search']."%' OR `booking_detail`.`Room_Num` LIKE '%".$_POST['Fuzzy_Search']."%') ";
                        $sql .= " AND " . $Fuzzy_Search . $GROUP_BY;
                    }
                    else
                        $sql .= $GROUP_BY;
                    $result = mysqli_query($conn,$sql);
                    $booking_counter=0;
                    while($row=$result->fetch_assoc()){
                        $info_array[$booking_counter]=$row;
                        // echo $sql;
                        // print_r($row);
                        // die;
                        // if($row['Room_Count']>1){
                        //     $counter = 1;
                        //     $rooms = '';
                        //     $sql = "SELECT `Room_Num` FROM `booking_detail` WHERE `Booking_ID`='".$row['Booking_ID']."'";
                        //     $room_result = mysqli_query($conn,$sql);
                        //     while($room_row=$room_result->fetch_assoc()){
                        //         $rooms .= $room_row['Room_Num'];
                        //         if($counter<intval($row['Room_Count'])){
                        //             $rooms .= ', ';
                        //         }
                        //         $counter++;
                        //     }
                        //     $info_array[$booking_counter]['Room_Num'] = $rooms;
                        // }
                        $booking_counter++;
                    }
                    for($i=0;$i<sizeof($info_array);$i++){
                        $info_array[$i]['Paid'] = 0;
                        $sql = "SELECT `Amount` FROM `payment` WHERE `Booking_ID`='".$info_array[$i]['Booking_ID']."'";
                        $result = mysqli_query($conn,$sql);
                        while($row=$result->fetch_assoc())
                            $info_array[$i]['Paid'] += $row['Amount'];
                    }
                    // print_r($info_array);
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'get_rest_room':
                    $room_array = array();
                    $booking_array = array();
                    date_default_timezone_set('Asia/Taipei');
                    $Today_Date = date('Y/m/d', time());
                    $sql = "SELECT * FROM `room_type`,`room_status` WHERE `room_status`.`Room_Type`=`room_type`.`Room_Type`";
                    $result = mysqli_query($conn,$sql);
                    while($row=$result->fetch_assoc()){
                        $room_array[$row['Room_Type']]['Room_Num'][] = $row['Room_Num'];
                        $room_array[$row['Room_Type']]['default_rest_price'] = $row['Rest_Price'];
                        $room_array[$row['Room_Type']]['rest_per_hour_price'] = $row['Rest_Per_Hour'];
                    }
                    $sql = "SELECT `booking_detail`.`Room_Type`,`booking_detail`.`Room_Num` FROM `booking_index`,`booking_detail` WHERE `booking_detail`.`Booking_ID`=`booking_index`.`Booking_ID` AND (('$Today_Date' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`)) OR (`booking_detail`.`Actual_Finished_Cleaning_Datetime`='0000-00-00 00:00:00' AND DATE(`booking_index`.`CIN_Date`)<='$Today_Date')) AND `booking_detail`.`Room_Status`!=8 AND `booking_detail`.`Room_Status`!=5 GROUP BY `booking_detail`.`Room_Num`";
                    $result = mysqli_query($conn,$sql);
                    while($row=$result->fetch_assoc()){
                        $booking_array[$row['Room_Type']]['Room_Num'][] = $row['Room_Num'];
                    }
                    foreach($booking_array as $room_type => $num_array){
                        $room_array[$room_type]['Room_Num'] = array_diff($room_array[$room_type]['Room_Num'],$num_array['Room_Num']);
                        sort($room_array[$room_type]['Room_Num']);
                    }
                    echo json_encode($room_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'create_rest_order':
                    date_default_timezone_set('Asia/Taipei');
                    $Booking_Date = date('Y/m/d', time());
                    $sql = "SELECT COUNT(*) FROM `booking_index` WHERE `Booking_Date`='".$Booking_Date."'";
                    $result = mysqli_query($conn,$sql);
                    $row = $result->fetch_assoc();
                    $Booking_ID = strtoupper('B' . dechex(date('Y', time())) . dechex(date('m', time())) . dechex(date('d', time())) . dechex(date('h', time())) . dechex(date('i', time())) . dechex(date('s', time())) . str_pad($row['COUNT(*)']+1, 4, "0", STR_PAD_LEFT));

                    $Booking_Datetime = date('Y/m/d H:i', time()) . ":00";
                    $COUT_H = date('H', time());
                    $COUT_i = date('i', time());
                    $Payment_Datetime = date('Y-m-d H:i:s', time());

                    $room_type = $_POST['room_type_selector'];
                    $room_num = $_POST['room_num_selector'];
                    $rest_hour = $_POST['rest_hour'];
                    $rest_Total_Price = $_POST['rest_Total_Price'];
                    $staff_ID = $_POST['staff_selector'];
                    
                    $sql = "INSERT INTO `booking_index`(`Booking_ID`, `Booking_Date`, `Customer_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `Total_Price`, `Overall_Status`) VALUES ('".$Booking_ID."','".$Booking_Date."','1','".$Booking_Datetime."','".$Booking_Date . " " . ($COUT_H+$rest_hour) . ":" . $COUT_i . ":00" . "','0','".$rest_Total_Price."','4')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                    $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Room_Num`, `Actual_CIN_Datetime`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_type."','".$room_num."','".$Booking_Datetime."','".$rest_Total_Price."','4')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }

                    foreach($_POST['payment_amount'] as $key=>$value){
                        if($value!='0'){
                            $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Method_Num`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$Booking_ID."','".$_POST['payment_method'][$key]."','".$value."','".$_POST['Remark'][$key]."','".$staff_ID."','".$Payment_Datetime."')";
                            if(!mysqli_query($conn,$sql)){
                                echo "This SQL: " . $sql . "<br>";
                                die;
                            }
                        }
                    }

                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;

                case 'get_staff':
                    $staff_array = array();
                    $sql = "SELECT `Staff_ID`, `Staff_Name` FROM `staff` WHERE (`Staff_Level` BETWEEN 1 AND 3)  AND `Enable`=1";
                    $result = mysqli_query($conn,$sql);
                    $count=0;
                    while($row=$result->fetch_assoc()){
                        $staff_array[$count]['Staff_ID'] = $row['Staff_ID'];
                        $staff_array[$count]['Staff_Name'] = $row['Staff_Name'];
                        $count++;
                    }
                    echo json_encode($staff_array,JSON_UNESCAPED_UNICODE);
                break;

                case 'create_rest_order':
                    date_default_timezone_set('Asia/Taipei');
                    $Booking_Date = date('Y/m/d', time());
                    $sql = "SELECT COUNT(*) FROM `booking_index` WHERE `Booking_Date`='".$Booking_Date."'";
                    $result = mysqli_query($conn,$sql);
                    $row = $result->fetch_assoc();
                    $Booking_ID = strtoupper('B' . dechex(date('Y', time())) . dechex(date('m', time())) . dechex(date('d', time())) . dechex(date('h', time())) . dechex(date('i', time())) . dechex(date('s', time())) . str_pad($row['COUNT(*)']+1, 4, "0", STR_PAD_LEFT));

                    $Booking_Datetime = date('Y/m/d H:i', time()) . ":00";
                    $COUT_H = date('H', time());
                    $COUT_i = date('i', time());
                    $Payment_Datetime = date('Y-m-d H:i:s', time());

                    $room_type = $_POST['room_type_selector'];
                    $room_num = $_POST['room_num_selector'];
                    $rest_hour = $_POST['rest_hour'];
                    $rest_Total_Price = $_POST['rest_Total_Price'];
                    $staff_ID = $_POST['staff_selector'];
                    
                    $sql = "INSERT INTO `booking_index`(`Booking_ID`, `Booking_Date`, `Customer_ID`, `CIN_Date`, `COUT_Date`, `Day_Count`, `Total_Price`, `Overall_Status`) VALUES ('".$Booking_ID."','".$Booking_Date."','1','".$Booking_Datetime."','".$Booking_Date . " " . ($COUT_H+$rest_hour) . ":" . $COUT_i . ":00" . "','0','".$rest_Total_Price."','4')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }
                    $sql = "INSERT INTO `booking_detail`(`Booking_ID`, `Room_Type`, `Room_Num`, `Actual_CIN_Datetime`, `Price`, `Room_Status`) VALUES ('".$Booking_ID."','".$room_type."','".$room_num."','".$Booking_Datetime."','".$rest_Total_Price."','4')";
                    if(!mysqli_query($conn,$sql)){
                        echo "This SQL: " . $sql . "<br>";
                        die;
                    }

                    foreach($_POST['payment_amount'] as $key=>$value){
                        if($value!='0'){
                            $sql = "INSERT INTO `payment`(`Booking_ID`, `Payment_Method_Num`, `Amount`, `Payment_Remark`, `Staff_ID`, `Payment_Datetime`) VALUES ('".$Booking_ID."','".$_POST['payment_method'][$key]."','".$value."','".$_POST['Remark'][$key]."','".$staff_ID."','".$Payment_Datetime."')";
                            if(!mysqli_query($conn,$sql)){
                                echo "This SQL: " . $sql . "<br>";
                                die;
                            }
                        }
                    }

                    echo json_encode(array('Success' => true),JSON_UNESCAPED_UNICODE);
                break;
                
                case 'check_booked_room':
                    $info_array = array();
                    $sql = "SELECT * FROM `customer`,`booking_detail`,`booking_index` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_index`.`Customer_ID`=`customer`.`Customer_ID` AND `booking_detail`.`Code`='".$_POST['Code']."'";
                    $result = mysqli_query($conn,$sql);
                    $info_array=$result->fetch_assoc();
                    $info_array['Total_Paid']=0;
                    $sql = "SELECT * FROM `booking_index`,`payment`,`booking_detail` WHERE `booking_index`.`Booking_ID`=`payment`.`Booking_ID` AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_detail`.`Code`='".$_POST['Code']."'";
                    $result = mysqli_query($conn,$sql);
                    while($row = $result->fetch_assoc()){
                        $info_array['Total_Paid']+=$row['Amount'];
                    }
                    $info_array['Index_Remark'] = str_replace(chr(13).chr(10), "<br />",nl2br($info_array['Index_Remark']));
                    // print_r($info_array);die;
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;
            }
        }
    }

    function get_roomlist($conn){
        $room_array = array();
        $sql = "SELECT * FROM `room_status` ORDER BY `Room_Num`";
        $result = mysqli_query($conn,$sql);
        for($i=0;$row = $result->fetch_assoc();$i++){
            $room_array[$i]['Room_Num'] = $row['Room_Num'];
            $room_array[$i]['Room_Type'] = $row['Room_Type'];
        }
        echo json_encode($room_array,JSON_UNESCAPED_UNICODE);
    }

    function get_booked_list($minDate,$conn){
        $booked_array = array();
        $maxDate = date('Y/m/d', strtotime($minDate. ' + 14 days'));
        $sql = "SELECT `booking_detail`.`Code`,`booking_detail`.`Booking_ID`,`booking_detail`.`Room_Type`,`booking_detail`.`Room_Num`,`booking_detail`.`Room_Status`,DATE(`booking_index`.`CIN_Date`),DATE(`booking_index`.`COUT_Date`) FROM `booking_index`,`booking_detail` WHERE `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `booking_detail`.`Room_Num`!='' AND ((DATE(`booking_index`.`CIN_Date`) BETWEEN '".$minDate."' AND '".$maxDate."' ) OR (DATE(`booking_index`.`COUT_Date`) BETWEEN '".$minDate."' AND '".$maxDate."' ) OR ('".$minDate."' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`)) OR ('".$maxDate."' BETWEEN DATE(`booking_index`.`CIN_Date`) AND DATE(`booking_index`.`COUT_Date`))) AND (`booking_detail`.`Room_Status` BETWEEN 0 AND 4 OR `booking_detail`.`Room_Status`=7) ORDER BY `booking_detail`.`Room_Num`, `booking_index`.`CIN_Date`";
        $result = mysqli_query($conn,$sql);
        $count = 0;
        while($row = $result->fetch_assoc()){
            if($count>0){
                if($booked_array[$count-1]['Room_Num'] == $row['Room_Num'] && $booked_array[$count-1]['COUT_Date'] == $row['DATE(`booking_index`.`CIN_Date`)']){
                    $count--;
                    $booked_array[$count]['Code'] = $row['Code'];
                    $booked_array[$count]['Booking_ID'] .= "_" . $row['Booking_ID'];
                    $booked_array[$count]['Room_Status'] .= "_" . $row['Room_Status'];
                    $booked_array[$count]['COUT_Date'] = $row['DATE(`booking_index`.`COUT_Date`)'];
                    if(date_create($booked_array[$count]['CIN_Date']) < date_create($minDate))
                        $booked_array[$count]['CIN_Date'] = $minDate;
                    if(date_create($booked_array[$count]['COUT_Date']) > date_create($maxDate))
                        $booked_array[$count]['COUT_Date'] = $maxDate;
                    $booked_array[$count]['Day_Count'] = date_diff(date_create($booked_array[$count]['CIN_Date']),date_create($booked_array[$count]['COUT_Date']))->format("%a") + 1;
                    $tmp = date_diff(date_create($booked_array[$count]['CIN_Date']),date_create($minDate))->format("%a");
                    if($tmp < 0)
                        $tmp = 0;
                    $booked_array[$count]['Start_From'] = $tmp;
                }
                else{
                    $booked_array[$count]['Code'] = $row['Code'];
                    $booked_array[$count]['Booking_ID'] = $row['Booking_ID'];
                    $booked_array[$count]['Room_Type'] = $row['Room_Type'];
                    $booked_array[$count]['Room_Num'] = $row['Room_Num'];
                    $booked_array[$count]['Room_Status'] = "case_" . $row['Room_Status'];
                    $booked_array[$count]['CIN_Date'] = $row['DATE(`booking_index`.`CIN_Date`)'];
                    $booked_array[$count]['COUT_Date'] = $row['DATE(`booking_index`.`COUT_Date`)'];
                    if(date_create($booked_array[$count]['CIN_Date']) < date_create($minDate))
                        $booked_array[$count]['CIN_Date'] = $minDate;
                    if(date_create($booked_array[$count]['COUT_Date']) > date_create($maxDate))
                        $booked_array[$count]['COUT_Date'] = $maxDate;
                    $booked_array[$count]['Day_Count'] = date_diff(date_create($booked_array[$count]['CIN_Date']),date_create($booked_array[$count]['COUT_Date']))->format("%a") + 1;
                    $tmp = date_diff(date_create($booked_array[$count]['CIN_Date']),date_create($minDate))->format("%a");
                    if($tmp < 0)
                        $tmp = 0;
                    $booked_array[$count]['Start_From'] = $tmp;
                }
            }
            else{
                $booked_array[$count]['Code'] = $row['Code'];
                $booked_array[$count]['Booking_ID'] = $row['Booking_ID'];
                $booked_array[$count]['Room_Type'] = $row['Room_Type'];
                $booked_array[$count]['Room_Num'] = $row['Room_Num'];
                $booked_array[$count]['Room_Status'] = "case_" . $row['Room_Status'];
                $booked_array[$count]['CIN_Date'] = $row['DATE(`booking_index`.`CIN_Date`)'];
                $booked_array[$count]['COUT_Date'] = $row['DATE(`booking_index`.`COUT_Date`)'];
                if(date_create($booked_array[$count]['CIN_Date']) < date_create($minDate))
                    $booked_array[$count]['CIN_Date'] = $minDate;
                if(date_create($booked_array[$count]['COUT_Date']) > date_create($maxDate))
                    $booked_array[$count]['COUT_Date'] = $maxDate;
                $booked_array[$count]['Day_Count'] = date_diff(date_create($booked_array[$count]['CIN_Date']),date_create($booked_array[$count]['COUT_Date']))->format("%a") + 1;
                $tmp = date_diff(date_create($booked_array[$count]['CIN_Date']),date_create($minDate))->format("%a");
                if($tmp < 0)
                    $tmp = 0;
                $booked_array[$count]['Start_From'] = $tmp;
            }
            $count++;
        }
        echo json_encode($booked_array,JSON_UNESCAPED_UNICODE);
    }

    function get_no_roomlist($conn){
        $booked_array = array();
        $count = 0;
        $sql = "SELECT `booking_detail`.`Code`,`customer`.`Customer_Name`,`booking_detail`.`Room_Type`,`booking_index`.`CIN_Date`,`booking_index`.`COUT_Date` FROM `booking_index`,`booking_detail`,`customer` WHERE `customer`.`Customer_ID`=`booking_index`.`Customer_ID` AND `booking_detail`.`Room_Num`='' AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND (`booking_detail`.`Room_Status`=0 OR `booking_detail`.`Room_Status`=7) ORDER BY `booking_index`.`CIN_Date`, `booking_detail`.`Code`";
        $result = mysqli_query($conn,$sql);
        while($row = $result->fetch_assoc()){
            $booked_array[$count]['Code'] = $row['Code'];
            $booked_array[$count]['Customer_Name'] = $row['Customer_Name'];
            $booked_array[$count]['Room_Type'] = $row['Room_Type'];
            $booked_array[$count]['CIN_Date'] = date("Y/m/d",strtotime($row['CIN_Date']));
            $booked_array[$count]['COUT_Date'] = date("Y/m/d",strtotime($row['COUT_Date']));
            $count++;
        }
        echo json_encode($booked_array,JSON_UNESCAPED_UNICODE);
    }
?>