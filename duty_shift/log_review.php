<?php
    include_once "../mysql_connect.inc.php";
    if($_COOKIE['Staff_Level']>1)
        echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
?>

<html>
    <head>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="../functions.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="../lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../lightpick.css">

        <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">

        <style>
            .ui-widget.ui-widget-content{
                /* border-radius: 20px; */
                /* border-width: 20px; */
            }
            .ui-widget-content {
                /* border-radius: 20px; */
                /* border-width: 20px; */
            }
            .ui-dialog-titlebar{
                /* display: none */
            }
            .payment_table{
                border: 1px solid #979797;
                border-collapse: collapse;
                width: 90%;
                font-size: 26px;
                margin-top: 30px;
                margin-bottom: 50px;
                table-layout: fixed;
                overflow: hidden;
            }
            .payment_table tr:first-child td{
                background-color: #6236FF;
                color: WHITE;
                text-align: center;
            }
            .payment_table tr:nth-child(odd){
                background: #CCC
            }
            .payment_table tr{
                border: 1px solid #979797;
                height: 74px;
            }
            .payment_table td{
                border: 1px solid #979797;
                text-align: center;
                font-size: 22px;
                padding: 0px 10px;
                word-break: break-word;
            }
        </style>
        
    </head>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center>
                <div>
                    <table style='width: 90%' border='0'>
                        <tr>
                            <td style='text-align:center;width:250px;'>
                                <input type='text' style='width:240px;height:50px;font-size:20px' id='Fuzzy_Search' placeholder='關鍵字搜尋'>
                            </td>
                            <td style='text-align:center;font-size:20px'>
                                <input type='text' id='Start' value='' style='width:270px;height:50px;font-size:20px;display:inline-block' placeholder='點擊以選擇起始時間'>
                                <!-- <span class=''> ～ </span> -->
                                <input type='text' id='End' value='' style='width:270px;height:50px;font-size:20px;display:inline-block' placeholder='點擊以選擇結束時間'>
                            </td>
                            <td style='text-align:center;font-size:20px'>
                                <button class='function_btn' id='Reset' style='width:110px;height:50px;font-size:20px;background-color:#0091FF'>重設</button>
                            </td>
                        </tr>
                    </table>
                </div>
                <div>
                    <table class='payment_table' border='1'>
                        <tr>
                            <td>交班時間</td>
                            <td>交班人員</td>
                            <td>交班總金額</td>
                            <td>現金總額</td>
                            <td>信用卡總額</td>
                            <td>轉帳總額</td>
                            <td>其他總額</td>
                            <td></td>
                        </tr>
                        <?php
                            $sql = "SELECT * FROM `duty`,`staff` WHERE `duty`.`Duty_Staff_ID`=`staff`.`Staff_ID`";
                            $result = mysqli_query($conn,$sql);
                            while($row = $result->fetch_assoc()){
                                $total = $row['Cash_Amount']+$row['Credit_Card_Amount']+$row['Transfer_Amount']+$row['Else_Amount'];
                                echo "<tr>";
                                    echo "<td>".str_replace("-", "/",nl2br($row['Duty_Datetime']))."</td>";
                                    echo "<td>".$row['Staff_Name']."</td>";
                                    echo "<td>".$total."</td>";
                                    echo "<td>".$row['Cash_Amount']."</td>";
                                    echo "<td>".$row['Credit_Card_Amount']."</td>";
                                    echo "<td>".$row['Transfer_Amount']."</td>";
                                    echo "<td>".$row['Else_Amount']."</td>";
                                    echo "<td><button type='submit' class='function_btn' style='width:90%;height:50px;background-color:#0091FF;font-size:22px;' value='".$row['Duty_ID']."' name='Duty_ID' onClick='open_dialog(this.value)'>詳細內容</button></td>";
                                echo "</tr>";
                            }
                        ?>
                    </table>
                </div>
            </center>
        </div>
    </body>
</html>

<!-- Dialog -->
    <div id="Detail_Dialog" name='dialog_section'>
        <center>
            <div id="container" style='width:100%'></div>
        </center>
    </div>
<!------------>

<script>
    $(document).ready(function() {
        $("#Detail_Dialog").dialog({
            height: "auto",
            width: "100%",
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });
    });

    function openTab(evt, ActionName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].classList.remove("activated");
            // tablinks[i].className = tablinks[i].className.replace(" activated", "");
        }
        document.getElementById(ActionName).style.display = "block";
        // evt.currentTarget.className += " activated";
        evt.currentTarget.classList.add("activated");
    }

    function goback(){
        window.history.back();
    }

    function open_dialog(Duty_ID){
        $.ajax({
            type: "POST",
            url: "service.php",
            data: {
                Feature: 'Get_Duty_List',
                Duty_ID: Duty_ID,
            },
            success: function(data) {
                $("#container").html('');
                var Payment_Type_array = ['訂金','現場收款','月結簽帳','帳務調整','退款','其它','預先授權'];
                var Payment_Method_Num_array = ['現金','信用卡','轉帳','其它'];
                var json_array = data;
                var row = '';
                table = "<table id='details' class='payment_table' style='width:100%'><tr><td width='17.5%'>訂單編號</td><td width='15%'>入住時間</td><td>房號</td><td width='9%'>收款人員</td><td width='15%'>付款時間</td><td width='9%'>付款類型</td><td width='9%'>付款方式</td><td width='7.5%' style='background-color:#F79B00'>收款</td><td>備註</td></tr>";
                $("#container").append(table);
                for(i=0;i<json_array.length;i++){
                    var Booking_ID = json_array[i].Booking_ID;
                    var Staying_Dates = json_array[i].Date_Range.replace(/-/g, "/").replace(/ /g, "<br>");
                    var Rooms = json_array[i].Rooms;
                    var Staff_ID = json_array[i].Staff_Name;
                    var Paid = json_array[i].Amount;
                    var Payment_Datetime = json_array[i].Payment_Datetime.replace(/-/g, "/").replace(/ /g, "<br>");
                    var Remark = json_array[i].Payment_Remark;
                    var date = json_array[i].Payment_Datetime;
                    var Payment_Type = Payment_Type_array[json_array[i].Payment_Type];
                    var Payment_Method_Num = Payment_Method_Num_array[json_array[i].Payment_Method_Num];
                    date = date.split(" ");
                    date = date[0];
                    row = "<tr><td>"+Booking_ID+"</td><td>"+Staying_Dates+"</td><td>"+Rooms+"</td><td>"+Staff_ID+"</td><td>"+Payment_Datetime+"</td><td>"+Payment_Type+"</td><td>"+Payment_Method_Num+"</td><td style='text-align:right'>"+Paid+"</td><td>"+Remark+"</td></tr>"
                    $("#details").append(row);
                }
                $("#Detail_Dialog").dialog( "open" );
            },
            error: function(jqXHR) {
                console.log("error: " + jqXHR.responseText);
            }
        })
    }
    
    $("body").on("click",".ui-widget-overlay",function() {
        close_dialog();
    });

    function close_dialog(){
        $("#container").html('');
        $('#Detail_Dialog').dialog( "close" );
    }
</script>