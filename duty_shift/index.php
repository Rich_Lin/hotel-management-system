<?php
    include_once "../mysql_connect.inc.php";

    error_reporting(0);

    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST['Action']) && $_POST['Action'] == 'submit')
            echo '<meta http-equiv=REFRESH CONTENT=0;url=index.php>';
    }

    $Payment_Array = array();
    $Total_Payment_Array = array();
    $Room_Array = array();
    $BID_Array = array();
    $sql = "SELECT `payment`.`Code`,`payment`.`Booking_ID`,`staff`.`Staff_Name`,`payment`.`Payment_Method_Num`,`payment`.`Amount`,`payment`.`Payment_Datetime`,`payment`.`Payment_Remark` FROM `payment`,`booking_index`,`staff` WHERE `booking_index`.`Booking_ID`=`payment`.`Booking_ID` AND `payment`.`Staff_ID`=`staff`.`Staff_ID` AND `payment`.`Duty_ID`='' ORDER BY `payment`.`Payment_Method_Num` DESC";
    $result = mysqli_query($conn,$sql);
    while($row = $result -> fetch_assoc()){
        $Payment_Array[$row['Payment_Method_Num']][] = $row;
        $Total_Payment_Array[$row['Payment_Method_Num']] += $row['Amount'];
        if(!in_array($row['Booking_ID'],$BID_Array))
            $BID_Array[]=$row['Booking_ID'];
    }

    for($i=0;$i<sizeof($BID_Array);$i++){
        $sql = "SELECT `Room_Num` FROM `booking_detail` WHERE `Booking_ID`='".$BID_Array[$i]."'";
        $result = mysqli_query($conn,$sql);
        while($row=$result->fetch_assoc()){
            $Room_Array[$BID_Array[$i]][] = $row['Room_Num'];
        }
    }
?>

<html>
    <head>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
        <script type="text/javascript" src="../functions.js"></script>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="../lightpick.js"></script>
        <link rel="stylesheet" type="text/css" href="../lightpick.css">

        <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script> -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
        
        <style>
            .tabcontent {
                width: 85%;
                display: none;
                /* border: 1px solid #ccc; */
            }
            .tablinks{
                width: 80% !important;
                height: 160px !important;
                font-size: 40px !important;
                color: WHITE !important;
                background-color: #0091FF;
                border-radius:15px !important;
            }
            .activated{
                border: 10px solid #32C5FF;
            }
            .payment_table{
                border: 1px solid #979797;
                border-collapse: collapse;
                width: 100%;
                font-size: 26px;
                margin-top: 30px;
                margin-bottom: 50px;
                table-layout: fixed;
                overflow: hidden;
            }
            .payment_table tr:first-child td{
                background-color: #6236FF;
                color: WHITE;
                text-align: center;
            }
            .payment_table tr:nth-child(odd){
                background: #CCC
            }
            .payment_table tr{
                border: 1px solid #979797;
                height: 74px;
            }
            .payment_table td{
                border: 1px solid #979797;
                text-align: center;
                font-size: 22px;
                padding: 0px 10px;
            }
            .footer_div{
                margin-bottom: 70px;
                bottom:0;
            }
        </style>
    </head>
    <body onload="includeHTML();">
        <div class='navbar-div' include-html="../navbar.html"></div>
        <div class='for_hyper left' include-html="../hyper.html"></div>
        <div class='right'>
            <center>
                <form id='myform' action='service.php'>
                    <input type='hidden' name='Feature' value='shift'>
                    <input type='hidden' name='Cash_Amount' value='<?php echo $Total_Payment_Array[0]; ?>'>
                    <input type='hidden' name='Credit_Card_Amount' value='<?php echo $Total_Payment_Array[1]; ?>'>
                    <input type='hidden' name='Transfer_Amount' value='<?php echo $Total_Payment_Array[2]; ?>'>
                    <input type='hidden' name='Else_Amount' value='<?php echo $Total_Payment_Array[3]; ?>'>
                    <table style='width:90%;'>
                        <tr>
                            <td style='text-align:center;width:20%'><button class='function_btn tablinks activated' type='button' onclick='openTab(event, "cash_tab")'>現金<br><span id='cash_amount'><?php echo $Total_Payment_Array[0]; ?></span></button></td>
                            <td style='text-align:center;width:20%'><button class='function_btn tablinks' type='button' onclick='openTab(event, "credit_card_tab")'>信用卡<br><span id='credit_card_amount'><?php echo $Total_Payment_Array[1]; ?></span></button></td>
                            <td style='text-align:center;width:20%'><button class='function_btn tablinks' type='button' onclick='openTab(event, "transfer_tab")'>轉帳<br><span id='transfer_amount'><?php echo $Total_Payment_Array[2]; ?></span></button></td>
                            <td style='text-align:center;width:20%'><button class='function_btn tablinks' type='button' onclick='openTab(event, "else_tab")'>其它<br><span id='else_amount'><?php echo $Total_Payment_Array[3]; ?></span></button></td>
                            <?php if($_COOKIE['Staff_Level']<=1) echo "<td style='text-align:center'><button class='function_btn tablinks' type='button' style='background-color:#F79B00;' onclick='location.href = \"log_review.php\";'>交班明細</button></td>"?>
                        </tr>
                    </table>
                    <div class='tabcontent' id='cash_tab' style="display: block;">
                        <table class='payment_table' id='cash_table'>
                            <tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>
                            <?php
                                for($i=0;$i<sizeof($Payment_Array[0]);$i++){
                                    echo "<tr>
                                            <td><input type='hidden' name='code[]' value='".$Payment_Array[0][$i]['Code']."'>".$Payment_Array[0][$i]['Booking_ID']."</td>
                                            <td>"; 
                                                for($j=0;$j<=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1;$j++){
                                                    echo $Room_Array[$Payment_Array[0][$i]['Booking_ID']][$j]; 
                                                    if($j!=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1)
                                                        echo ", ";
                                                }
                                            echo "</td>
                                            <td>".$Payment_Array[0][$i]['Staff_Name']."</td>
                                            <td style='text-align:right'>".$Payment_Array[0][$i]['Amount']."</td>
                                            <td>".$Payment_Array[0][$i]['Payment_Datetime']."</td>
                                            <td style='text-align:left'>".str_replace(chr(13).chr(10), "<br />",nl2br($Payment_Array[0][$i]['Payment_Remark']))."</td>
                                        </tr>";
                                }
                            ?>
                        </table>
                    </div>
                    <div class='tabcontent' id='credit_card_tab'>
                        <table class='payment_table' id='credit_card_table'>
                            <tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>
                            <?php
                                for($i=0;$i<sizeof($Payment_Array[1]);$i++){
                                    echo "<tr>
                                            <td><input type='hidden' name='code[]' value='".$Payment_Array[1][$i]['Code']."'>".$Payment_Array[1][$i]['Booking_ID']."</td>
                                            <td>"; 
                                                for($j=0;$j<=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1;$j++){
                                                    echo $Room_Array[$Payment_Array[0][$i]['Booking_ID']][$j]; 
                                                    if($j!=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1)
                                                        echo ", ";
                                                }
                                            echo "</td>
                                            <td>".$Payment_Array[1][$i]['Staff_Name']."</td>
                                            <td style='text-align:right'>".$Payment_Array[1][$i]['Amount']."</td>
                                            <td>".$Payment_Array[1][$i]['Payment_Datetime']."</td>
                                            <td style='text-align:left'>".str_replace(chr(13).chr(10), "<br />",nl2br($Payment_Array[1][$i]['Payment_Remark']))."</td>
                                        </tr>";
                                }
                            ?>
                        </table>
                    </div>
                    <div class='tabcontent' id='transfer_tab'>
                        <table class='payment_table' id='transfer_table'>
                            <tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>
                            <?php
                                for($i=0;$i<sizeof($Payment_Array[2]);$i++){
                                    echo "<tr>
                                            <td><input type='hidden' name='code[]' value='".$Payment_Array[2][$i]['Code']."'>".$Payment_Array[2][$i]['Booking_ID']."</td>
                                            <td>"; 
                                                for($j=0;$j<=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1;$j++){
                                                    echo $Room_Array[$Payment_Array[0][$i]['Booking_ID']][$j]; 
                                                    if($j!=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1)
                                                        echo ", ";
                                                }
                                            echo "</td>
                                            <td>".$Payment_Array[2][$i]['Staff_Name']."</td>
                                            <td style='text-align:right'>".$Payment_Array[2][$i]['Amount']."</td>
                                            <td>".$Payment_Array[2][$i]['Payment_Datetime']."</td>
                                            <td style='text-align:left'>".str_replace(chr(13).chr(10), "<br />",nl2br($Payment_Array[2][$i]['Payment_Remark']))."</td>
                                        </tr>";
                                }
                            ?>
                        </table>
                    </div>
                    <div class='tabcontent' id='else_tab'>
                        <table class='payment_table' id='else_table'>
                            <tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>
                            <?php
                                for($i=0;$i<sizeof($Payment_Array[3]);$i++){
                                    echo "<tr>
                                            <td><input type='hidden' name='code[]' value='".$Payment_Array[3][$i]['Code']."'>".$Payment_Array[3][$i]['Booking_ID']."</td>
                                            <td>"; 
                                                for($j=0;$j<=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1;$j++){
                                                    echo $Room_Array[$Payment_Array[0][$i]['Booking_ID']][$j]; 
                                                    if($j!=sizeof($Room_Array[$Payment_Array[0][$i]['Booking_ID']])-1)
                                                        echo ", ";
                                                }
                                            echo "</td>
                                            <td>".$Payment_Array[3][$i]['Staff_Name']."</td>
                                            <td style='text-align:right'>".$Payment_Array[3][$i]['Amount']."</td>
                                            <td>".$Payment_Array[3][$i]['Payment_Datetime']."</td>
                                            <td style='text-align:left'>".str_replace(chr(13).chr(10), "<br />",nl2br($Payment_Array[3][$i]['Payment_Remark']))."</td>
                                        </tr>";
                                }
                            ?>
                        </table>
                    </div>
                    <div class='footer_div'>
                        <table style='width:100%' border='0'>
                            <tr>
                                <td style='width:33%'></td>
                                <td style='width:33%'>
                                    <button type='submit' name='Action'value='submit' class='function_btn' style='background-color:#F79B00;width:650px;height:110px;font-size:48px'>交班</button>
                                </td>
                                <td style='width:33%;font-size:26px;'>
                                    <?php
                                        $sql = 'SELECT * FROM `duty` WHERE 1 ORDER BY `Duty_Datetime` DESC LIMIT 1';
                                        $result = mysqli_query($conn,$sql);
                                        $row = $result -> fetch_assoc();
                                        echo "<input type='hidden' name='Last_Duty_ID' value='".$row['Duty_ID']."'><input type='hidden' name='Last_Duty_Datetime' value='".$row['Duty_Datetime']."'>上次交班時間：<span id='Last_Duty_Datetime'>" . $row['Duty_Datetime'] . "</span>";
                                    ?>
                                </td>
                            </tr>
                    </div>
                </form>
            </center>
        </div>
    </body>

</html>

<script>
    $(document).ready(function() {
        
        $("#dialog").dialog({
            height: 862,
            width: 970,
            autoOpen: false,
            resizable: false,
            draggable: false,
            modal: true
            // dialogClass: 'ui-dialog-osx',
        });

        $("#myform").submit(function(){
            event.preventDefault();
            var shift_confirm = confirm("確定要交班嗎？");
            if(shift_confirm == true){
                $.ajax({
                    type: "POST",
                    url: "service.php",
                    data: $("#myform").serialize(),
                    success: function(data) {
                        $("#cash_amount").html('');
                        $("#credit_card_amount").html('');
                        $("#transfer_amount").html('');
                        $("#else_amount").html('');
                        
                        $("#cash_table").children().remove();
                        $("#cash_table").html("<tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>");
                        $("#credit_card_table").children().remove();
                        $("#credit_card_table").html("<tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>");
                        $("#transfer_table").children().remove();
                        $("#transfer_table").html("<tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>");
                        $("#else_table").children().remove();
                        $("#else_table").html("<tr><td style='width:20%'>訂單編號</td><td style='width:10%'>房號</td><td style='width:12.5%'>收款人員</td><td style='background-color:#F79B00;width:12.5%'>收款金額</td><td style='width:20%'>收款時間</td><td style='width:25%'>備註</td></tr>");
                        $("#Last_Duty_Datetime").html(data.Last_Duty_Datetime);
                    },
                    error: function(jqXHR) {
                        console.log("error: " + jqXHR.responseText);
                    }
                })
            }
        })
    });

    function openTab(evt, ActionName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].classList.remove("activated");
            // tablinks[i].className = tablinks[i].className.replace(" activated", "");
        }
        document.getElementById(ActionName).style.display = "block";
        // evt.currentTarget.className += " activated";
        evt.currentTarget.classList.add("activated");
    }

    function goback(){
        window.history.back();
    }
</script>