<?php
    header('Content-Type: application/json; charset=UTF-8');
    include_once "../mysql_connect.inc.php";
    if ($_SERVER['REQUEST_METHOD'] == "POST") {
        if(isset($_POST)){
            switch($_POST['Feature']){
                case 'shift':
                    if(isset($_COOKIE['Staff_ID'])){
                        date_default_timezone_set('Asia/Taipei');
                        $Duty_Datetime = date('Y/m/d H:i:s', time());
                        $sql = 'SELECT COUNT(*) FROM `duty` WHERE 1';
                        $result = mysqli_query($conn,$sql);
                        $row = $result -> fetch_assoc();
                        $Duty_ID=$row['COUNT(*)']+1;
                        for($i=0;$i<sizeof($_POST['code']);$i++){
                            $sql = "UPDATE `payment` SET `Duty_ID`='".$Duty_ID."' WHERE `Code`='".$_POST['code'][$i]."'";
                            if(!mysqli_query($conn,$sql)){
                                echo "This SQL: " . $sql . "<br>";
                                die;
                            }
                        }
                        $sql = "INSERT INTO `duty`(`Duty_ID`, `Last_Duty_ID`, `Last_Duty_Datetime`, `Duty_Datetime`, `Duty_Staff_ID`, `Cash_Amount`, `Credit_Card_Amount`, `Transfer_Amount`, `Else_Amount`) VALUES ('".$Duty_ID."','".$_POST['Last_Duty_ID']."','".$_POST['Last_Duty_Datetime']."','".$Duty_Datetime."','".$_COOKIE['Staff_ID']."','".$_POST['Cash_Amount']."','".$_POST['Credit_Card_Amount']."','".$_POST['Transfer_Amount']."','".$_POST['Else_Amount']."')";
                        if(!mysqli_query($conn,$sql)){
                            echo "This SQL: " . $sql . "<br>";
                            die;
                        }
                        echo json_encode(array('Last_Duty_Datetime' => $Duty_Datetime),JSON_UNESCAPED_UNICODE);
                    }
                break;

                case 'Get_Duty_List':
                    $info_array = array();
                    $sql = "SELECT * FROM `payment` WHERE `Duty_ID`='".$_POST['Duty_ID']."'";
                    $result = mysqli_query($conn,$sql);
                    while($row = $result -> fetch_assoc()){
                        $info_array[$row['Code']] = $row;
                    }
                    foreach($info_array as $Code => $value){
                        $room_string = '';
                        $Date_Range = '';
                        $sql = "SELECT `booking_index`.`CIN_Date`,`booking_index`.`COUT_Date`,`booking_detail`.`Room_Num`,`staff`.`Staff_Name` FROM `booking_index`,`booking_detail`,`staff`,`payment` WHERE `payment`.`Code`='".$Code."' AND `booking_index`.`Booking_ID`=`payment`.`Booking_ID` AND `booking_index`.`Booking_ID`=`booking_detail`.`Booking_ID` AND `payment`.`Staff_ID`=`staff`.`Staff_ID`";
                        $result = mysqli_query($conn,$sql);
                        while($row = $result -> fetch_assoc()){
                            if($room_string=='')
                                $room_string .= $row['Room_Num'];
                            else
                                $room_string .= ", " . $row['Room_Num'];

                            if($Date_Range=='')
                                $Date_Range = $row['CIN_Date'] . " ~ " . $row['COUT_Date'];

                            $info_array[$Code]['Staff_Name'] = $row['Staff_Name'];

                        }
                        $info_array[$Code]['Date_Range'] = $Date_Range;
                        $info_array[$Code]['Rooms'] = $room_string;
                    }
                    sort($info_array);
                    echo json_encode($info_array,JSON_UNESCAPED_UNICODE);
                break;
            }
        }
    }
?>